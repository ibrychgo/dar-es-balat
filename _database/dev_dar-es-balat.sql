/*
 Navicat MySQL Data Transfer

 Source Server         : My mysql
 Source Server Version : 50525
 Source Host           : localhost
 Source Database       : dev_dar-es-balat

 Target Server Version : 50525
 File Encoding         : utf-8

 Date: 12/20/2013 18:35:43 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `exp_accessories`
-- ----------------------------
DROP TABLE IF EXISTS `exp_accessories`;
CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(255) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_accessories`
-- ----------------------------
INSERT INTO `exp_accessories` VALUES ('1', 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'), ('2', 'Mx_cloner_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|sites|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.1.3');

-- ----------------------------
--  Table structure for `exp_actions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_actions`;
CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  `csrf_exempt` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_actions`
-- ----------------------------
INSERT INTO `exp_actions` VALUES ('1', 'Comment', 'insert_new_comment', '0'), ('2', 'Comment_mcp', 'delete_comment_notification', '0'), ('3', 'Comment', 'comment_subscribe', '0'), ('4', 'Comment', 'edit_comment', '0'), ('5', 'Email', 'send_email', '0'), ('6', 'Metaweblog_api', 'incoming', '0'), ('7', 'Search', 'do_search', '0'), ('8', 'Channel', 'submit_entry', '0'), ('9', 'Channel', 'filemanager_endpoint', '0'), ('10', 'Channel', 'smiley_pop', '0'), ('11', 'Channel', 'combo_loader', '0'), ('12', 'Member', 'registration_form', '0'), ('13', 'Member', 'register_member', '0'), ('14', 'Member', 'activate_member', '0'), ('15', 'Member', 'member_login', '0'), ('16', 'Member', 'member_logout', '0'), ('17', 'Member', 'send_reset_token', '0'), ('18', 'Member', 'process_reset_password', '0'), ('19', 'Member', 'send_member_email', '0'), ('20', 'Member', 'update_un_pw', '0'), ('21', 'Member', 'member_search', '0'), ('22', 'Member', 'member_delete', '0'), ('23', 'Rte', 'get_js', '0'), ('24', 'Playa_mcp', 'filter_entries', '0'), ('25', 'Channel_videos', 'channel_videos_router', '0'), ('26', 'Tagger', 'tagger_router', '0');

-- ----------------------------
--  Table structure for `exp_captcha`
-- ----------------------------
DROP TABLE IF EXISTS `exp_captcha`;
CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_categories`
-- ----------------------------
DROP TABLE IF EXISTS `exp_categories`;
CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_categories`
-- ----------------------------
INSERT INTO `exp_categories` VALUES ('1', '1', '1', '0', 'Featured', 'featured', '', '', '1'), ('3', '1', '2', '0', 'Topic A', 'topic-a', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.', '{filedir_1}NatGeo01.jpg', '1'), ('4', '1', '2', '0', 'Topic B', 'topic-b', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.', '{filedir_1}NatGeo04.jpg', '2'), ('5', '1', '2', '0', 'Topic C', 'topic-c', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.', '{filedir_1}Aerial04.jpg', '3'), ('6', '1', '3', '0', 'How the Universe Works', 'how-the-universe-works', '', '', '3'), ('7', '1', '3', '0', 'Religion and Morality', 'religion-and-morality', '', '', '4'), ('8', '1', '3', '0', 'Creationism vs Science', 'creationism-vs-science', '', '', '1'), ('9', '1', '3', '0', 'Universal Scales', 'universal-scales', '', '', '5'), ('10', '1', '3', '0', 'Evolution', 'evolution', '', '', '2'), ('11', '1', '4', '0', 'Ideas - Design Patterns', 'ideas-design-patterns', '', '', '1'), ('12', '1', '4', '0', 'Ideas - Tech', 'ideas-tech', '', '', '2');

-- ----------------------------
--  Table structure for `exp_category_field_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_category_field_data`;
CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` varchar(40) DEFAULT 'none',
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_category_field_data`
-- ----------------------------
INSERT INTO `exp_category_field_data` VALUES ('1', '1', '1', null, 'none'), ('3', '1', '2', 'NatGeo01.jpg', 'none'), ('4', '1', '2', 'NatGeo04.jpg', 'none'), ('5', '1', '2', 'Aerial04.jpg', 'none'), ('6', '1', '3', null, 'none'), ('7', '1', '3', null, 'none'), ('8', '1', '3', null, 'none'), ('9', '1', '3', null, 'none'), ('10', '1', '3', null, 'none'), ('11', '1', '4', null, 'none'), ('12', '1', '4', null, 'none');

-- ----------------------------
--  Table structure for `exp_category_fields`
-- ----------------------------
DROP TABLE IF EXISTS `exp_category_fields`;
CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_category_fields`
-- ----------------------------
INSERT INTO `exp_category_fields` VALUES ('1', '1', '2', 'category-image-filename', 'Category Image filename (from selected image above', 'text', '', '128', '6', 'none', 'n', 'ltr', 'y', '2');

-- ----------------------------
--  Table structure for `exp_category_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_category_groups`;
CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_category_groups`
-- ----------------------------
INSERT INTO `exp_category_groups` VALUES ('1', '1', 'Utility', 'a', '2', 'all', '', ''), ('2', '1', 'Posts', 'c', '0', 'all', '', ''), ('3', '1', 'Public Resources', 'a', '0', 'all', '', ''), ('4', '1', 'Private Resources', 'a', '0', 'all', '', '');

-- ----------------------------
--  Table structure for `exp_category_posts`
-- ----------------------------
DROP TABLE IF EXISTS `exp_category_posts`;
CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_category_posts`
-- ----------------------------
INSERT INTO `exp_category_posts` VALUES ('5', '3'), ('6', '1'), ('6', '4'), ('7', '5'), ('8', '1'), ('8', '3'), ('9', '1'), ('9', '4'), ('19', '1'), ('19', '3'), ('19', '5'), ('21', '1'), ('21', '4'), ('21', '5'), ('24', '1'), ('24', '3'), ('45', '3'), ('45', '5'), ('47', '4'), ('48', '3'), ('49', '5'), ('63', '4');

-- ----------------------------
--  Table structure for `exp_channel_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_data`;
CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` text,
  `field_ft_16` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_21` text,
  `field_ft_21` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_27` text,
  `field_ft_27` tinytext,
  `field_id_28` text,
  `field_ft_28` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_31` text,
  `field_ft_31` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_data`
-- ----------------------------
INSERT INTO `exp_channel_data` VALUES ('45', '1', '6', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '{filedir_1}NatGeo02.jpg', 'none', '', null, '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, 'With content - large', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('5', '1', '2', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '{filedir_1}Aerial04.jpg', 'none', '', 'none', '', null, '', null, 'Test Blog Entry 1 Meta Title', 'none', 'Test Blog Entry 1 Meta Description', 'none', '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'No', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('6', '1', '2', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '{filedir_1}images.jpg', 'none', '', 'none', '', null, '', null, 'Test Blog Entry Meta Title 2', 'none', 'Test Blog Entry Meta Description 2', 'none', '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'No', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('7', '1', '2', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '{filedir_1}images.jpg', 'none', '', 'none', '', null, '', null, 'Test Blog Entry Meta Title 3', 'none', 'Test Blog Entry Meta Description 3', 'none', '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('8', '1', '2', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '{filedir_1}NatGeo04.jpg', 'none', '', 'none', '', null, '', null, 'Test Blog Entry Meta Title 4', 'none', 'Test Blog Entry Meta Description 4', 'none', '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'With content - medium', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('9', '1', '2', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '{filedir_1}images.jpg', 'none', '', 'none', '', null, '', null, 'Test Blog Entry Meta Title 5', 'none', 'Test Blog Entry Meta Description 5', 'none', '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('10', '1', '5', '', null, '', null, '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('11', '1', '5', '', null, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', null, '', null, '', 'none', '', null, '', null, 'About Meta Title', null, 'About Meta Description', 'none', '', null, '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'No', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('12', '1', '5', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '', 'none', '', 'none', '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('57', '1', '4', '', null, '', null, '', null, 'Article Archives', 'none', '', 'xhtml', 'widgets/archive-list', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'blog', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('19', '1', '1', 'ChannelVideos', 'xhtml', '', 'xhtml', '{filedir_1}NatGeo02.jpg', 'none', '', 'none', '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', 'none', '', 'none', 'No', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('21', '1', '1', 'ChannelVideos', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n  \n\n  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 'xhtml', '{filedir_1}Aerial04.jpg', 'none', '', 'none', '', null, '', null, 'Eric Hovind OFFICIALLY accepts atheism is true', 'none', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', 'none', '', 'none', 'No', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('62', '1', '5', '', 'xhtml', '', 'xhtml', '', 'none', '', null, '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, 'No', 'none', '', 'xhtml', '', null, '', null, '', null, '', 'none', '', 'none', '', 'xhtml'), ('24', '1', '6', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '{filedir_1}NatGeo11.jpg', 'none', '', 'none', '', null, '', null, 'Gallery Meta Title', 'none', 'Gallery Meta Description', 'none', '', null, '', 'none', '', null, '', null, '', null, '1', 'none', '', 'none', '', 'none', 'With content - medium', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('60', '1', '4', '', null, '', null, '', null, 'Video Topics', 'none', '', 'xhtml', 'widgets/topic-list', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'videos', 'none', '', 'none', '2', 'none', '', 'none', '', 'none', '', 'xhtml'), ('31', '1', '5', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\n  \n\n At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', 'xhtml', '', 'none', '', 'none', '', null, '', null, 'Privacy Meta Title', 'none', 'Privacy Meta Description', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('32', '1', '5', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\n  \n\n At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', 'xhtml', '', 'none', '', 'none', '', null, '', null, 'Terms metatitle', 'none', 'Terms metadescription', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('41', '1', '5', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\n  \n\n At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', 'xhtml', '', 'none', '', 'none', '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, 'No', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('42', '1', '4', '', null, '', null, '', null, '', 'none', '', 'xhtml', 'widgets/carousel', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', 'carousel', 'none', '', null, '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('43', '1', '4', '', null, '', null, '', null, 'About this site', 'none', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;\n\n <a href=\"/about\">Learn more</a>', 'xhtml', '0', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'blog', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('46', '1', '4', '', null, '', null, '', null, 'Get in touch!', 'none', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.\n\n<a class=\"button\" href=\"/about\">More</a>', 'xhtml', '0', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', 'panel', 'none', '', null, '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('47', '1', '6', '', 'xhtml', '', 'xhtml', '{filedir_1}NatGeo02.jpg', 'none', '', null, '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, 'With content - large', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('48', '1', '6', '', 'xhtml', '', 'xhtml', '{filedir_1}NatGeo11.jpg', 'none', '', null, '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, 'With content - large', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('49', '1', '6', '', 'xhtml', '', 'xhtml', '{filedir_1}Aerial04.jpg', 'none', '', null, '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, 'With content - large', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('50', '1', '4', '', null, '', null, '', null, 'Featured Videos', 'none', '', 'xhtml', 'widgets/entry-list', 'none', '', null, '', null, '', 'none', '', null, '1', 'none', '3', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'videos', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('52', '1', '5', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'xhtml', '', 'none', '', null, '', null, '', null, 'Contact meta title', 'none', 'Contact meta description', 'none', '', null, '[43] [about-this-site] About this site\n[50] [configurable-entry-list] Configurable Entry List', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, 'No', 'none', '', 'xhtml', '', null, '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('53', '1', '4', '', null, '', null, '', null, 'Recent Posts', 'none', '', 'xhtml', 'widgets/entry-list', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '5', 'none', 'No', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'blog', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('54', '1', '4', '', null, '', null, '', null, 'Newest Photos', 'none', '', 'xhtml', 'widgets/entry-list', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '4', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'portfolio', 'none', 'small', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('58', '1', '4', '', null, '', null, '', null, 'Archives', 'none', '', 'xhtml', 'widgets/archive-list', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'videos', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('56', '1', '4', '', null, '', null, '', null, 'Article Topics', 'none', '', 'xhtml', 'widgets/topic-list', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'blog', 'none', '', 'none', '2', 'none', '', 'none', '', 'none', '', 'xhtml'), ('59', '1', '4', '', null, '', null, '', null, 'Photo Archives', 'none', '', 'xhtml', 'widgets/archive-list', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'portfolio', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml'), ('63', '1', '2', '', 'xhtml', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\n  \n\n  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat\n\n  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\n\n  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat..', 'xhtml', '{filedir_1}Aerial04.jpg', 'none', '', null, '', null, '', null, '', 'none', '', 'none', '', null, '', 'none', '', null, '', null, '', null, '', 'none', '', null, '', null, 'With content - large', 'none', '', 'xhtml', '', null, '', null, '', null, '', 'none', '', 'none', '', 'xhtml'), ('55', '1', '4', '', null, '', null, '', null, 'Topics', 'none', '', 'xhtml', 'widgets/topic-list', 'none', '', null, '', null, '', 'none', '', null, '', 'none', '', 'none', 'Yes', 'none', '', null, '', 'none', '', 'none', '', null, '', null, 'portfolio', 'none', '', 'none', '2', 'none', '', 'none', '', 'none', '', 'xhtml');

-- ----------------------------
--  Table structure for `exp_channel_entries_autosave`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_entries_autosave`;
CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_entries_autosave`
-- ----------------------------
INSERT INTO `exp_channel_entries_autosave` VALUES ('7', '0', '1', '2', '1', null, '127.0.0.1', 'autosave_1383362313', 'autosave_1383362313', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1383362220', '2013', '11', '01', '0', '0', '20131102031833', '0', '0', 'a:27:{s:8:\"entry_id\";i:7;s:10:\"channel_id\";s:1:\"2\";s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:4368:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p>\n\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat..</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"field_id_1\";s:0:\"\";s:10:\"field_id_3\";s:0:\"\";s:11:\"field_id_24\";s:0:\"\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_id_6\";s:0:\"\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_id_21\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_28\";s:0:\"\";s:17:\"original_entry_id\";i:0;s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";}'), ('6', '0', '1', '4', '1', null, '127.0.0.1', 'autosave_1383361793', 'autosave_1383361793', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383361680', '2013', '11', '01', '0', '0', '20131102030953', '0', '0', 'a:27:{s:8:\"entry_id\";i:6;s:10:\"channel_id\";s:1:\"4\";s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_4\";s:18:\"my favorite videos\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_id_6\";s:20:\"widgets/archive-list\";s:11:\"field_id_25\";s:6:\"videos\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_id_16\";s:1:\"3\";s:11:\"field_id_15\";s:1:\"3\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:2:\"sm\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_10\";s:0:\"\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_id_2\";s:0:\"\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_id_24\";s:0:\"\";s:17:\"original_entry_id\";i:0;s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";}');

-- ----------------------------
--  Table structure for `exp_channel_fields`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_fields`;
CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_fields`
-- ----------------------------
INSERT INTO `exp_channel_fields` VALUES ('1', '1', '1', 'cf-post_videos', 'Videos', '', 'channel_videos', '', '0', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '8', 'any', 'YTo4OntzOjExOiJjdl9zZXJ2aWNlcyI7YToyOntpOjA7czo3OiJ5b3V0dWJlIjtpOjE7czo1OiJ2aW1lbyI7fXM6OToiY3ZfbGF5b3V0IjtzOjU6InRpbGVzIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('2', '1', '1', 'cf-post_body', 'Body', '', 'rte', '', '0', '0', '0', '10', '128', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '5', 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'), ('3', '1', '1', 'cf-post_featured-image', 'Featured Image', '', 'file', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '3', 'any', 'YToxMDp7czoxODoiZmllbGRfY29udGVudF90eXBlIjtzOjU6ImltYWdlIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjE6IjEiO3M6MTM6InNob3dfZXhpc3RpbmciO3M6MToieSI7czoxMjoibnVtX2V4aXN0aW5nIjtzOjA6IiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'), ('4', '1', '2', 'cf-widget_title', 'Display title', '', 'text', '', '0', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'none', 'n', '1', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('5', '1', '2', 'cf-widget_body', 'Body', '', 'rte', '', '0', '0', '0', '10', '0', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '3', 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'), ('6', '1', '2', 'cf-widget_template', 'Use Dynamic Widget instead', '', 'template_snippet_select', '', '0', '0', '0', '0', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '4', 'any', 'YTo3OntzOjIzOiJ0ZW1wbGF0ZV9zbmlwcGV0X3NlbGVjdCI7YToyOntzOjE1OiJmaWVsZF90ZW1wbGF0ZXMiO2E6NDp7czo4OiJzaG93X2FsbCI7YjowO3M6MTA6InNob3dfZ3JvdXAiO2E6MTp7aTowO3M6Nzoid2lkZ2V0cyI7fXM6MTM6InNob3dfc2VsZWN0ZWQiO2I6MDtzOjk6InRlbXBsYXRlcyI7YjowO31zOjE0OiJmaWVsZF9zbmlwcGV0cyI7YTozOntzOjg6InNob3dfYWxsIjtiOjA7czoxMzoic2hvd19zZWxlY3RlZCI7YjowO3M6ODoic25pcHBldHMiO2I6MDt9fXM6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('8', '1', '1', 'cf-post_metatitle', 'Meta Title', '', 'text', '', '0', '0', '0', '0', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '1', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('9', '1', '1', 'cf-post_metadescription', 'Meta Description', '', 'textarea', '', '0', '0', '0', '6', '0', 'n', 'ltr', 'n', 'n', 'none', 'n', '2', 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('10', '1', '2', 'cf-widget_feature-image', 'Feature Image', '', 'file', '', '0', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'none', 'n', '2', 'any', 'YToxMDp7czoxODoiZmllbGRfY29udGVudF90eXBlIjtzOjU6ImltYWdlIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjE6IjEiO3M6MTM6InNob3dfZXhpc3RpbmciO3M6MToieSI7czoxMjoibnVtX2V4aXN0aW5nIjtzOjA6IiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'), ('11', '1', '1', 'cf-post_sidebar', 'Sidebar Widgets', '', 'playa', '', '0', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'none', 'n', '6', 'any', 'YToxNDp7czo1OiJtdWx0aSI7czoxOiJ5IjtzOjc6ImV4cGlyZWQiO3M6MToibiI7czo2OiJmdXR1cmUiO3M6MToieSI7czo4OiJlZGl0YWJsZSI7czoxOiJuIjtzOjg6ImNoYW5uZWxzIjthOjE6e2k6MDtzOjE6IjQiO31zOjg6InN0YXR1c2VzIjthOjE6e2k6MDtzOjQ6Im9wZW4iO31zOjc6Im9yZGVyYnkiO3M6NToidGl0bGUiO3M6NDoic29ydCI7czozOiJBU0MiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('23', '1', '1', 'cf-post_featured-type', 'Display featured image on post page?', '', 'select', 'No\r\nAs banner\r\nWith content - large\r\nWith content - medium\r\nWith content - small\r\nWith content - thumbnail\r\n', 'n', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'none', 'n', '4', 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('24', '1', '1', 'cf-post_menu', 'Add to Taxonomy Menu', '', 'taxonomy', '', '0', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '9', 'any', 'YTo3OntzOjE2OiJ0YXhvbm9teV9vcHRpb25zIjthOjM6e3M6ODoiY2hhbm5lbHMiO2E6NDp7aToxO3M6MDoiIjtpOjI7czowOiIiO2k6NTtzOjE6IjEiO2k6NjtzOjA6IiI7fXM6MTQ6InNob3dfdGVtcGxhdGVzIjthOjQ6e2k6MTtzOjE6IjAiO2k6MjtzOjE6IjAiO2k6NTtzOjE6IjEiO2k6NjtzOjE6IjAiO31zOjE2OiJkZWZhdWx0X3RlbXBsYXRlIjthOjQ6e2k6MTtzOjA6IiI7aToyO3M6MDoiIjtpOjU7czoyOiIxMCI7aTo2O3M6MDoiIjt9fXM6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('15', '1', '2', 'cf-widget_filter', 'Apply filter', 'Enter IDs for the topics you want to display, separated by a pipe|: <br>(Featured = 1, Topic A = 3, Topic B = 4, Topic C = 5)', 'text', 'Featured|1\r\nTopic A| 3', '0', '0', '0', '0', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '5', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('16', '1', '2', 'cf-widget_limit', 'Limit Results', '', 'text', '', '0', '0', '0', '0', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '6', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('20', '1', '1', 'cf-post_gallery', 'Image Gallery', '', 'matrix', '', '0', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'none', 'n', '7', 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO2k6MjtpOjE7aTozO2k6MjtpOjQ7fX0='), ('21', '1', '2', 'cf-widget_featured-post', 'Featured Post', 'Some dynamic widgets need to know which post you want them to feature.', 'playa', '', '0', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'none', 'n', '8', 'any', 'YToxMzp7czo1OiJtdWx0aSI7czoxOiJuIjtzOjc6ImV4cGlyZWQiO3M6MToibiI7czo2OiJmdXR1cmUiO3M6MToieSI7czo4OiJlZGl0YWJsZSI7czoxOiJuIjtzOjg6ImNoYW5uZWxzIjthOjE6e2k6MDtzOjE6IjIiO31zOjc6Im9yZGVyYnkiO3M6NToidGl0bGUiO3M6NDoic29ydCI7czozOiJBU0MiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('18', '1', '2', 'cf-widget_include-images', 'Include featured images?', '', 'select', 'Yes\r\nNo', 'n', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '7', 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('22', '1', '2', 'cf-widget_class', 'Widget Custom Class', '', 'text', '', '0', '0', '0', '0', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '9', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('25', '1', '2', 'cf-widget_channel', 'Channel', '', 'select', 'blog\r\nportfolio\r\nvideos', 'n', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'none', 'n', '10', 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('27', '1', '2', 'cf-widget_image-size', 'Image Size', '', 'text', '', '0', '0', '0', '0', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '11', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('28', '1', '2', 'cf-widget_cat-group', 'Category Group ', '(For topic/category listings)<br> Use \"2\" for default category group.', 'text', '', '0', '0', '0', '0', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '12', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('29', '1', '1', 'cf-post_source', 'Source', '', 'text', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'y', 'n', 'none', 'n', '10', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('30', '1', '1', 'cf-post_citation', 'Citation', '', 'text', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'y', 'n', 'none', 'n', '11', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('31', '1', '1', 'cf-post_tags', 'Tags', '', 'tagger', '', '0', '0', '0', '0', '0', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '12', 'any', 'YTo3OntzOjY6InRhZ2dlciI7YTozOntzOjE0OiJzaG93X21vc3RfdXNlZCI7czozOiJ5ZXMiO3M6MTI6InNpbmdsZV9maWVsZCI7czoyOiJubyI7czoxNzoiYXV0b19hc3NpZ25fZ3JvdXAiO3M6MToiMCI7fXM6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');

-- ----------------------------
--  Table structure for `exp_channel_form_settings`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_form_settings`;
CREATE TABLE `exp_channel_form_settings` (
  `channel_form_settings_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `default_status` varchar(50) NOT NULL DEFAULT 'open',
  `require_captcha` char(1) NOT NULL DEFAULT 'n',
  `allow_guest_posts` char(1) NOT NULL DEFAULT 'n',
  `default_author` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_form_settings_id`),
  KEY `site_id` (`site_id`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_channel_member_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_member_groups`;
CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_channel_titles`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_titles`;
CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_titles`
-- ----------------------------
INSERT INTO `exp_channel_titles` VALUES ('47', '1', '6', '1', null, '127.0.0.1', 'Photo', 'photo', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1383259980', '2013', '10', '31', '0', '0', '20131031225702', '0', '0'), ('48', '1', '6', '1', null, '127.0.0.1', 'Photo', 'photo1', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1383259980', '2013', '10', '31', '0', '0', '20131031225709', '0', '0'), ('45', '1', '6', '1', null, '127.0.0.1', 'Another test portfolio', 'another-test-portfolio', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1383089760', '2013', '10', '29', '0', '0', '20131029234044', '0', '0'), ('19', '1', '1', '1', null, '127.0.0.1', 'Another Test Video', 'another-test-video', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1382827800', '2013', '10', '26', '0', '0', '20131028212841', '0', '0'), ('5', '1', '2', '1', null, '127.0.0.1', 'Test Blog Entry 1', 'test-blog-entry-1', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1367433120', '2013', '05', '01', '0', '0', '20131031025047', '0', '0'), ('6', '1', '2', '1', null, '127.0.0.1', 'Test Blog Entry 2', 'test-blog-entry-2', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1371493920', '2013', '06', '17', '0', '0', '20131028212412', '0', '0'), ('7', '1', '2', '1', null, '127.0.0.1', 'Test Blog Entry 3', 'test-blog-entry-3', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1376850720', '2013', '08', '18', '0', '0', '20131026200501', '1383097755', '2'), ('8', '1', '2', '1', null, '127.0.0.1', 'Test Blog Entry 4', 'test-blog-entry-4', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1382725860', '2013', '10', '25', '0', '0', '20131029224738', '1383271633', '3'), ('9', '1', '2', '1', null, '127.0.0.1', 'Test Blog Entry 5', 'test-blog-entry-5', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1385148720', '2013', '11', '22', '0', '0', '20131026200450', '0', '0'), ('10', '1', '5', '1', null, '127.0.0.1', 'Test proxy Page', 'test-proxy-page', 'open', 'y', '0', '0', '0', '0', 'y', 'n', '1382736900', '2013', '10', '25', '0', '0', '20131025213601', '0', '0'), ('11', '1', '5', '1', null, '127.0.0.1', 'Our Story', 'about', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1382740320', '2013', '10', '25', '0', '0', '20131029235205', '0', '0'), ('46', '1', '4', '1', null, '127.0.0.1', 'Get in touch now!', 'get-in-touch-now', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383255780', '2013', '10', '31', '0', '0', '20131031214446', '0', '0'), ('12', '1', '5', '1', null, '127.0.0.1', 'About Me', 'about-me', 'open', 'y', '0', '0', '0', '0', 'y', 'n', '1382741460', '2013', '10', '25', '0', '0', '20131025225613', '0', '0'), ('57', '1', '4', '1', null, '127.0.0.1', 'Blog Archives', 'blog-archives1', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383338040', '2013', '11', '01', '0', '0', '20131101203456', '0', '0'), ('58', '1', '4', '1', null, '127.0.0.1', 'Video Archives', 'video-archives', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383338160', '2013', '11', '01', '0', '0', '20131101203642', '0', '0'), ('49', '1', '6', '1', null, '127.0.0.1', 'Photo', 'photo2', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1383260040', '2013', '10', '31', '0', '0', '20131031225635', '1383273044', '3'), ('21', '1', '1', '1', null, '127.0.0.1', 'First Featured Video', 'first-featured-video', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1382828160', '2013', '10', '26', '0', '0', '20131028212915', '1382829802', '1'), ('56', '1', '4', '1', null, '127.0.0.1', 'Blog Topics', 'blog-topics', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383337860', '2013', '11', '01', '0', '0', '20131101203138', '0', '0'), ('24', '1', '6', '1', null, '127.0.0.1', 'Test Gallery', 'test-gallery', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1382891880', '2013', '10', '27', '0', '0', '20131028212400', '0', '0'), ('62', '1', '5', '1', null, '127.0.0.1', 'Nested 1', 'nested-1', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383343740', '2013', '11', '01', '0', '0', '20131101221020', '0', '0'), ('60', '1', '4', '1', null, '127.0.0.1', 'Video Topics', 'video-topics', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383338220', '2013', '11', '01', '0', '0', '20131101203811', '0', '0'), ('63', '1', '2', '1', null, '127.0.0.1', 'Yet another blog entry', 'yet-another-blog-entry', 'Public', 'y', '0', '0', '0', '0', 'y', 'n', '1383362220', '2013', '11', '01', '0', '0', '20131102032009', '0', '0'), ('59', '1', '4', '1', null, '127.0.0.1', 'Portfolio Archives', 'portfolio-archives', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383338160', '2013', '11', '01', '0', '0', '20131101203706', '0', '0'), ('43', '1', '4', '1', null, '127.0.0.1', 'About this site', 'about-this-site', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383076080', '2013', '10', '29', '0', '0', '20131102031357', '0', '0'), ('31', '1', '5', '1', null, '127.0.0.1', 'Privacy Policy', 'privacy-policy', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1382921880', '2013', '10', '27', '0', '0', '20131028010012', '0', '0'), ('32', '1', '5', '1', null, '127.0.0.1', 'Terms & Conditions', 'terms-conditions', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1382921940', '2013', '10', '27', '0', '0', '20131028005951', '0', '0'), ('41', '1', '5', '1', null, '127.0.0.1', 'Frequently Asked Questions', 'frequently-asked-questions', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383070200', '2013', '10', '29', '0', '0', '20131029181058', '0', '0'), ('42', '1', '4', '1', null, '127.0.0.1', 'Home Carousel', 'home-carousel', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383075780', '2013', '10', '29', '0', '0', '20131029194741', '0', '0'), ('50', '1', '4', '1', null, '127.0.0.1', 'Widget - Featured Videos (3)', 'featured-videos1', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383276780', '2013', '10', '31', '0', '0', '20131101200315', '0', '0'), ('55', '1', '4', '1', null, '127.0.0.1', 'Portfolio Topics', 'portfolio-topics1', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383337440', '2013', '11', '01', '0', '0', '20131101203024', '0', '0'), ('52', '1', '5', '1', null, '127.0.0.1', 'Contact Us', 'contact', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383333120', '2013', '11', '01', '0', '0', '20131101194158', '0', '0'), ('53', '1', '4', '1', null, '127.0.0.1', 'Widget - Recent Blog Posts (5)', 'widget-recent-blog-posts', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383336420', '2013', '11', '01', '0', '0', '20131101201108', '0', '0'), ('54', '1', '4', '1', null, '127.0.0.1', 'Widget - Recent Portfolio (4)', 'widget-recent-portfolio-4', 'open', 'y', '0', '0', '0', '0', 'n', 'n', '1383336480', '2013', '11', '01', '0', '0', '20131101202111', '0', '0');

-- ----------------------------
--  Table structure for `exp_channel_videos`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_videos`;
CREATE TABLE `exp_channel_videos` (
  `video_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` tinyint(3) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(10) unsigned DEFAULT '0',
  `field_id` mediumint(8) unsigned DEFAULT '1',
  `service` varchar(250) DEFAULT '',
  `service_video_id` varchar(250) DEFAULT '',
  `hash_id` varchar(250) DEFAULT '',
  `video_title` varchar(250) DEFAULT '',
  `video_desc` varchar(250) DEFAULT '',
  `video_username` varchar(250) DEFAULT '',
  `video_author` varchar(250) DEFAULT '',
  `video_author_id` int(10) unsigned DEFAULT '0',
  `video_date` varchar(250) DEFAULT '',
  `video_views` varchar(250) DEFAULT '',
  `video_duration` varchar(250) DEFAULT '',
  `video_url` varchar(250) DEFAULT '',
  `video_img_url` varchar(250) DEFAULT '',
  `video_order` smallint(5) unsigned DEFAULT '1',
  `video_cover` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`video_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_videos`
-- ----------------------------
INSERT INTO `exp_channel_videos` VALUES ('3', '1', '19', '1', '1', 'youtube', '4WA4qtemcUs', '', 'PZ Myers accuses Shermer of rape', 'Freezepage of the original article.\nhttp://www.freezepage.com/1376039041HQQMDPRCAF\n\nPZ Myers then added part where he says the story was given to him by Carrie Poppy.  That is, someone gave the story to Carrie, who then gave it to PZ myers, who then ', 'Thunderf00t', 'Thunderf00t', '0', '1376844125', '86088', '1161', 'http://www.youtube.com/embed/4WA4qtemcUs', 'http://i.ytimg.com/vi/4WA4qtemcUs/default.jpg', '0', '0'), ('4', '1', '21', '1', '1', 'youtube', 'A9BfsHsVGNg', '', 'Eric Hovind OFFICIALLY accepts atheism is true and accurate', 'For those who don\'t get the title, Eric Hovind put up his copy of the video under the title of \'OFFICIAL\'.  What makes it official? Nothing, its simply Eric trying to score a little attention, and yeah, sure under the circumstances I have nothing aga', 'Thunderf00t', 'Thunderf00t', '0', '1333629617', '338246', '3416', 'http://www.youtube.com/embed/A9BfsHsVGNg', 'http://i.ytimg.com/vi/A9BfsHsVGNg/default.jpg', '0', '0');

-- ----------------------------
--  Table structure for `exp_channels`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channels`;
CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(255) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channels`
-- ----------------------------
INSERT INTO `exp_channels` VALUES ('1', '1', 'videos', 'Videos', 'http://idaho/videos/', '', 'en', '2', '1', '1382828160', '1382829802', '1|2', '1', 'Public', '1', '1', '', 'y', 'y', null, 'all', 'y', 'n', 'n', '', '/videos/post/', 'y', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', '0', '/videos/post/', 'y', '', 'y', '10', '', '', '0'), ('2', '1', 'blog', 'Blog', 'http://idaho/blog/post', '', 'en', '4', '5', '1382725860', '1383271633', '1|2', '1', 'Public', '1', '2', '', 'y', 'y', null, 'all', 'y', 'n', 'n', '', '/blog/post/', 'y', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', '0', '/blog/post/', 'y', '', 'y', '10', '', '', '0'), ('4', '1', 'widgets', 'Widgets', 'http://idaho//', '', 'en', '12', '0', '1383338220', '0', '1|2', '1', 'open', '2', '4', '', 'n', 'y', null, 'all', 'y', 'n', 'n', '', '', 'n', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', '0', '', 'n', '', 'y', '10', '', '', '0'), ('5', '1', 'pages', 'Pages', 'http://idaho//', '', 'en', '8', '0', '1383343740', '0', '', '1', 'open', '1', '2', '', 'n', 'y', null, 'all', 'y', 'n', 'n', '', '', 'n', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', '0', '', 'y', '', 'y', '10', '', '', '0'), ('6', '1', 'portfolio', 'Portfolio', 'http://idaho/', '', 'en', '2', '3', '1383089760', '1383273044', '1|2', '1', 'Public', '1', '2', '', 'y', 'y', null, 'all', 'y', 'n', 'n', '', '/portfolio/', 'y', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', '0', '/portfolio/', 'y', '', 'y', '10', '', '', '0'), ('7', '1', 'resources', 'Resources', 'http://idaho//', null, 'en', '0', '0', '0', '0', '4|3', '1', 'open', '1', null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, '0', null, 'y', null, 'n', '10', '', '', '0');

-- ----------------------------
--  Table structure for `exp_comment_subscriptions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_comment_subscriptions`;
CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_comment_subscriptions`
-- ----------------------------
INSERT INTO `exp_comment_subscriptions` VALUES ('1', '8', '1', '', '1382806211', 'n', '1ULOflCFF'), ('2', '7', '1', '', '1382817060', 'n', '1HPwvteyA'), ('3', '21', '1', '', '1382829802', 'n', '1UC7J5wWz'), ('4', '49', '1', '', '1383272840', 'n', '1QjjYiZnr');

-- ----------------------------
--  Table structure for `exp_comments`
-- ----------------------------
DROP TABLE IF EXISTS `exp_comments`;
CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`),
  KEY `comment_date_idx` (`comment_date`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_comments`
-- ----------------------------
INSERT INTO `exp_comments` VALUES ('1', '1', '8', '2', '1', 'o', 'Bryan Lewis', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1382806211', null, 'This is a test comment.'), ('2', '1', '8', '2', '1', 'o', 'Bryan Lewis', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1382806330', null, 'This is another comment\n'), ('3', '1', '7', '2', '1', 'o', 'Bryan Lewis', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1382817060', null, 'Comments working?'), ('4', '1', '21', '1', '1', 'o', 'Bryan Lewis', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1382829802', null, 'Testing Video Commenting\n'), ('5', '1', '7', '2', '1', 'o', 'Bryan Lewis', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1383097755', null, 'Testing comments\n'), ('6', '1', '8', '2', '0', 'o', 'JOhn Smit', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1383271633', null, 'Testing.'), ('7', '1', '49', '6', '0', 'o', 'John Smith', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1383271946', null, 'Testing portfolio comments.'), ('8', '1', '49', '6', '1', 'o', 'Bryan Lewis', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1383272840', null, 'Testing comment again.'), ('9', '1', '49', '6', '1', 'o', 'Bryan Lewis', 'bryan.lewis@gmail.com', '', '', '127.0.0.1', '1383273044', null, 'Another comment');

-- ----------------------------
--  Table structure for `exp_content_types`
-- ----------------------------
DROP TABLE IF EXISTS `exp_content_types`;
CREATE TABLE `exp_content_types` (
  `content_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_type_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_content_types`
-- ----------------------------
INSERT INTO `exp_content_types` VALUES ('1', 'grid'), ('2', 'channel');

-- ----------------------------
--  Table structure for `exp_cp_log`
-- ----------------------------
DROP TABLE IF EXISTS `exp_cp_log`;
CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_cp_log`
-- ----------------------------
INSERT INTO `exp_cp_log` VALUES ('1', '1', '1', 'bryan', '127.0.0.1', '1382651457', 'Logged in'), ('2', '1', '1', 'bryan', '127.0.0.1', '1382651902', 'Logged in'), ('3', '1', '1', 'bryan', '127.0.0.1', '1382654853', 'Logged in'), ('4', '1', '1', 'bryan', '127.0.0.1', '1382658143', 'Logged in'), ('5', '1', '1', 'bryan', '127.0.0.1', '1382659540', 'Channel Created:&nbsp;&nbsp;Videos'), ('6', '1', '1', 'bryan', '127.0.0.1', '1382659724', 'Logged in'), ('7', '1', '1', 'bryan', '127.0.0.1', '1382662248', 'Logged out'), ('8', '1', '1', 'bryan', '127.0.0.1', '1382714308', 'Logged in'), ('9', '1', '1', 'bryan', '127.0.0.1', '1382721778', 'Category Group Created:&nbsp;&nbsp;Flags'), ('10', '1', '1', 'bryan', '127.0.0.1', '1382721862', 'Channel Created:&nbsp;&nbsp;blog'), ('11', '1', '1', 'bryan', '127.0.0.1', '1382722069', 'Channel Created:&nbsp;&nbsp;Widget Areas'), ('12', '1', '1', 'bryan', '127.0.0.1', '1382722080', 'Channel Created:&nbsp;&nbsp;Widgets'), ('13', '1', '1', 'bryan', '127.0.0.1', '1382736730', 'Channel Created:&nbsp;&nbsp;Page'), ('14', '1', '1', 'bryan', '127.0.0.1', '1382744791', 'Logged out'), ('15', '1', '1', 'bryan', '127.0.0.1', '1382744840', 'Logged in'), ('16', '1', '1', 'bryan', '127.0.0.1', '1382749091', 'Logged out'), ('17', '1', '1', 'bryan', '127.0.0.1', '1382799250', 'Logged in'), ('18', '1', '1', 'bryan', '127.0.0.1', '1382816034', 'Logged out'), ('19', '1', '1', 'bryan', '127.0.0.1', '1382816613', 'Logged in'), ('20', '1', '1', 'bryan', '127.0.0.1', '1382817804', 'Category Group Created:&nbsp;&nbsp;Posts'), ('21', '1', '1', 'bryan', '127.0.0.1', '1382823357', 'Channel Deleted:&nbsp;&nbsp;Widget Groups'), ('22', '1', '1', 'bryan', '127.0.0.1', '1382833267', 'Logged out'), ('23', '1', '1', 'bryan', '127.0.0.1', '1382883481', 'Logged in'), ('24', '1', '1', 'bryan', 'fe80::bae8:56ff:fe02:7854', '1382883569', 'Logged in'), ('25', '1', '1', 'bryan', '127.0.0.1', '1382883866', 'Logged in'), ('26', '1', '1', 'bryan', '127.0.0.1', '1382890633', 'Logged in'), ('27', '1', '1', 'bryan', '127.0.0.1', '1382891809', 'Channel Created:&nbsp;&nbsp;Galleries'), ('28', '1', '1', 'bryan', '127.0.0.1', '1382896299', 'Logged in'), ('29', '1', '1', 'bryan', '127.0.0.1', '1382898988', 'Logged in'), ('30', '1', '1', 'bryan', '127.0.0.1', '1382898998', 'Logged in'), ('31', '1', '1', 'bryan', '127.0.0.1', '1382911216', 'Logged in'), ('32', '1', '1', 'bryan', '127.0.0.1', '1382918478', 'Logged out'), ('33', '1', '1', 'bryan', '127.0.0.1', '1382921850', 'Logged in'), ('34', '1', '1', 'bryan', '127.0.0.1', '1382925475', 'Logged out'), ('35', '1', '1', 'bryan', '127.0.0.1', '1382930654', 'Logged in'), ('36', '1', '1', 'bryan', '127.0.0.1', '1382936634', 'Logged out'), ('37', '1', '1', 'bryan', '127.0.0.1', '1382981891', 'Logged in'), ('38', '1', '1', 'bryan', '127.0.0.1', '1382984995', 'Logged out'), ('39', '1', '1', 'bryan', '127.0.0.1', '1382995329', 'Logged in'), ('40', '1', '1', 'bryan', '127.0.0.1', '1383062666', 'Logged in'), ('41', '1', '1', 'bryan', '127.0.0.1', '1383073315', 'Logged out'), ('42', '1', '1', 'bryan', '127.0.0.1', '1383075724', 'Logged in'), ('43', '1', '1', 'bryan', '127.0.0.1', '1383083758', 'Logged in'), ('44', '1', '1', 'bryan', '127.0.0.1', '1383090017', 'Logged in'), ('45', '1', '1', 'bryan', '127.0.0.1', '1383091445', 'Logged in'), ('46', '1', '1', 'bryan', '127.0.0.1', '1383092855', 'Logged out'), ('47', '1', '1', 'bryan', '127.0.0.1', '1383093581', 'Logged out'), ('48', '1', '1', 'bryan', '127.0.0.1', '1383097709', 'Logged in'), ('49', '1', '1', 'bryan', '127.0.0.1', '1383098163', 'Logged in'), ('50', '1', '1', 'bryan', '127.0.0.1', '1383173284', 'Logged in'), ('51', '1', '1', 'bryan', '127.0.0.1', '1383177627', 'Logged in'), ('52', '1', '1', 'bryan', '127.0.0.1', '1383180374', 'Logged in'), ('53', '1', '1', 'bryan', '127.0.0.1', '1383180427', 'Site Updated&nbsp;&nbsp;Pree-built'), ('54', '1', '1', 'bryan', '127.0.0.1', '1383180454', 'Site Updated&nbsp;&nbsp;Pree-built'), ('55', '1', '1', 'bryan', '127.0.0.1', '1383180521', 'Site Updated&nbsp;&nbsp;Preebuilt'), ('56', '1', '1', 'bryan', '127.0.0.1', '1383180531', 'Site Updated&nbsp;&nbsp;PreeBuilt'), ('57', '1', '1', 'bryan', '127.0.0.1', '1383187155', 'Logged out'), ('58', '1', '1', 'bryan', '127.0.0.1', '1383187584', 'Logged in'), ('59', '1', '1', 'bryan', '127.0.0.1', '1383187727', 'Logged in'), ('60', '1', '1', 'bryan', '127.0.0.1', '1383249718', 'Logged in'), ('61', '1', '1', 'bryan', '127.0.0.1', '1383253475', 'Logged in'), ('62', '1', '1', 'bryan', '127.0.0.1', '1383257354', 'Logged out'), ('63', '1', '1', 'bryan', '127.0.0.1', '1383265016', 'Logged out'), ('64', '1', '1', 'bryan', '127.0.0.1', '1383269809', 'Logged in'), ('65', '1', '1', 'bryan', '127.0.0.1', '1383269882', 'Logged out'), ('66', '1', '1', 'bryan', '127.0.0.1', '1383272000', 'Logged in'), ('67', '1', '1', 'bryan', '127.0.0.1', '1383272444', 'Logged out'), ('68', '1', '1', 'bryan', '127.0.0.1', '1383272741', 'Logged in'), ('69', '1', '1', 'bryan', '127.0.0.1', '1383281869', 'Logged out'), ('70', '1', '1', 'bryan', '127.0.0.1', '1383282141', 'Logged in'), ('71', '1', '1', 'bryan', '127.0.0.1', '1383315102', 'Logged in'), ('72', '1', '1', 'bryan', '127.0.0.1', '1383315205', 'Logged in'), ('73', '1', '1', 'bryan', '127.0.0.1', '1383333156', 'Logged in'), ('74', '1', '1', 'bryan', '127.0.0.1', '1383336807', 'Logged in'), ('75', '1', '1', 'bryan', '127.0.0.1', '1383340847', 'Logged in'), ('76', '1', '1', 'bryan', '127.0.0.1', '1383361093', 'Logged in'), ('77', '1', '1', 'bryan', '127.0.0.1', '1383685727', 'Logged in'), ('78', '1', '1', 'bryan', '127.0.0.1', '1383752900', 'Logged in'), ('79', '1', '1', 'bryan', '127.0.0.1', '1383755420', 'Logged out'), ('80', '1', '1', 'bryan', '127.0.0.1', '1383761998', 'Logged in'), ('81', '1', '1', 'bryan', '127.0.0.1', '1383762306', 'Logged in'), ('82', '1', '1', 'bryan', '127.0.0.1', '1383763538', 'Logged out'), ('83', '1', '1', 'bryan', '127.0.0.1', '1383769500', 'Logged in'), ('84', '1', '1', 'bryan', '127.0.0.1', '1383772955', 'Logged in'), ('85', '1', '1', 'bryan', '127.0.0.1', '1383775399', 'Logged out'), ('86', '1', '1', 'bryan', '127.0.0.1', '1384295142', 'Logged in'), ('87', '1', '1', 'bryan', '127.0.0.1', '1384298559', 'Logged out'), ('88', '1', '1', 'bryan', '127.0.0.1', '1387400970', 'Logged in'), ('89', '1', '1', 'bryan', '127.0.0.1', '1387401279', 'Logged in'), ('90', '1', '1', 'bryan', '127.0.0.1', '1387493795', 'Logged in'), ('91', '1', '1', 'bryan', '127.0.0.1', '1387581828', 'Logged in'), ('92', '1', '1', 'bryan', '127.0.0.1', '1387581924', 'Channel Created:&nbsp;&nbsp;Resources'), ('93', '1', '1', 'bryan', '127.0.0.1', '1387582020', 'Category Group Created:&nbsp;&nbsp;Resources'), ('94', '1', '1', 'bryan', '127.0.0.1', '1387582437', 'Category Group Created:&nbsp;&nbsp;Private Resources');

-- ----------------------------
--  Table structure for `exp_cp_search_index`
-- ----------------------------
DROP TABLE IF EXISTS `exp_cp_search_index`;
CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_cp_search_index`
-- ----------------------------
INSERT INTO `exp_cp_search_index` VALUES ('1', 'admin_system', 'general_configuration', 'english', 'can_access_sys_prefs', 'Enable Multiple Site Manager Is system on? Is site on? License Number Name of your site\'s index page URL to the root directory of your site URL to your \"themes\" folder Theme Folder Path Default Control Panel Theme Default Language Default XML Language Maximum Number of Cachable URIs New Version Auto Check URL to Documentation Directory'), ('2', 'admin_system', 'output_debugging_preferences', 'english', 'can_access_sys_prefs', 'Generate HTTP Page Headers? Enable GZIP Output? Force URL query strings Redirection Method Debug Preference Display Output Profiler? Display Template Debugging?'), ('3', 'admin_system', 'database_settings', 'english', 'can_access_sys_prefs', 'Enable Database Debugging Persistent Database Connection'), ('4', 'admin_system', 'security_session_preferences', 'english', 'can_access_sys_prefs', 'cookie cookies Control Panel Session Type User Session Type Process form data in Secure Mode? Deny Duplicate Data? Apply Rank Denial to User-submitted Links? Allow members to change their username? Allow multiple log-ins from a single account? Require IP Address and User Agent for Login? Require IP Address and User Agent for posting? Apply XSS Filtering to uploaded files? Enable Password Lockout? Time Interval for Lockout Require Secure Passwords? Allow Dictionary Words as Passwords? Name of Dictionary File Minimum Username Length Minimum Password Length'), ('5', 'admin_system', 'throttling_configuration', 'english', 'can_access_sys_prefs', 'Enable Throttling Deny Access if No IP Address is Present Maximum Number of Page Loads Time Interval (in seconds) Lockout Time (in seconds) Action to Take URL for Redirect Custom Message'), ('6', 'admin_system', 'localization_settings', 'english', 'can_access_sys_prefs', 'Site Timezone Default Time Formatting'), ('7', 'admin_system', 'email_configuration', 'english', 'can_access_sys_prefs', 'Return email address for auto-generated emails Webmaster or site name for auto-generated emails Email Character Encoding Enable Email Debugging? Email Protocol SMTP Server Address SMTP Server Port SMTP Username SMTP Password Use Batch Mode? Number of Emails Per Batch Default Mail Format Enable Word-wrapping by Default? Email Console Timelock Log Email Console Messages Enable CAPTCHAs for Tell-a-Friend and Contact emails'), ('8', 'admin_system', 'cookie_settings', 'english', 'can_access_sys_prefs', 'cookies Cookie Domain Cookie Path Cookie Prefix'), ('9', 'admin_system', 'image_resizing_preferences', 'english', 'can_access_sys_prefs', 'Image Resizing Protocol Image Converter Path Image Thumbnail Suffix'), ('10', 'admin_system', 'captcha_preferences', 'english', 'can_access_sys_prefs', 'Server Path to CAPTCHA Folder Full URL to CAPTCHA Folder Use TrueType Font for CAPTCHA? Add Random Number to CAPTCHA Word Require CAPTCHA with logged-in members?'), ('11', 'admin_system', 'word_censoring', 'english', 'can_access_sys_prefs', 'Enable Word Censoring? Censoring Replacement Word Censored Words'), ('12', 'admin_system', 'mailing_list_preferences', 'english', 'can_access_sys_prefs', 'Mailing List is Enabled Enable recipient list for notification of new mailing list sign-ups Email Address of Notification Recipient(s) '), ('13', 'admin_system', 'emoticon_preferences', 'english', 'can_access_sys_prefs', 'Display Smileys? URL to the directory containing your smileys '), ('14', 'admin_system', 'tracking_preferences', 'english', 'can_access_sys_prefs', 'Enable Online User Tracking? Enable Template Hit Tracking? Enable Channel Entry View Tracking? Maximum number of recent referrers to save Suspend ALL tracking when number of online visitors exceeds:'), ('15', 'admin_system', 'search_log_configuration', 'english', 'can_access_sys_prefs', 'Enable Search Term Logging Maximum number of recent search terms to save'), ('16', 'admin_content', 'global_channel_preferences', 'english', 'can_admin_channels', 'Use Category URL Titles In Links? Category URL Indicator Auto-Assign Category Parents Clear all caches when new entries are posted? Cache Dynamic Channel Queries? Word Separator for URL Titles'), ('17', 'admin_content', 'field_group_management', 'english', 'can_admin_channels', 'Field Group Management'), ('18', 'admin_content', 'category_management', 'english', 'can_admin_categories', 'Category Management'), ('19', 'addons_accessories', 'index', 'english', 'can_access_accessories', 'Accessories'), ('20', 'addons_extensions', 'index', 'english', 'can_access_extensions', 'Extensions'), ('21', 'addons_fieldtypes', 'index', 'english', 'can_access_fieldtypes', 'fiel_index'), ('22', 'addons_modules', 'index', 'english', 'can_access_modules', 'Modules'), ('23', 'addons_plugins', 'index', 'english', 'can_access_plugins', 'Plugins'), ('24', 'content_publish', 'index', 'english', 'all', 'publish new entry publ_index'), ('25', 'content_files', 'index', 'english', 'can_access_files', 'File Manager'), ('26', 'design', 'user_message', 'english', 'can_admin_design', 'User Message Template'), ('27', 'design', 'global_template_preferences', 'english', 'can_admin_design', 'strict_urls 404 Page save_tmpl_revisions max_tmpl_revisions save_tmpl_files tmpl_file_basepath'), ('28', 'design', 'system_offline', 'english', 'can_admin_design', 'System Offline Template'), ('29', 'design', 'email_notification', 'english', 'can_admin_templates', 'Email Notification Template'), ('30', 'design', 'member_profile_templates', 'english', 'can_admin_mbr_templates', 'Member Profile Template'), ('31', 'members', 'register_member', 'english', 'can_admin_members', 'members_register_member'), ('32', 'members', 'member_validation', 'english', 'can_admin_members', 'members_member_validation'), ('33', 'members', 'view_members', 'english', 'can_access_members', 'members_view_members'), ('34', 'members', 'ip_search', 'english', 'can_admin_members', 'ip IP members_ip_search'), ('35', 'members', 'custom_profile_fields', 'english', 'can_admin_members', 'Custom Member Profile Fields'), ('36', 'members', 'member_group_manager', 'english', 'can_admin_mbr_groups', 'Member Group Management'), ('37', 'members', 'member_config', 'english', 'can_admin_members', 'members_member_config'), ('38', 'members', 'member_banning', 'english', 'can_ban_users', 'members_member_banning'), ('39', 'members', 'member_search', 'english', 'all', 'members_member_search'), ('40', 'tools_data', 'sql_manager', 'english', 'can_access_data', 'Sql Manager'), ('41', 'tools_data', 'search_and_replace', 'english', 'can_access_data', 'Search and Replace'), ('42', 'tools_data', 'recount_stats', 'english', 'can_access_data', 'Recount Stats'), ('43', 'tools_data', 'php_info', 'english', 'can_access_data', 'PHP Info'), ('44', 'tools_data', 'clear_caching', 'english', 'can_access_data', 'Clear Caching'), ('45', 'tools_logs', 'view_cp_log', 'english', 'can_access_logs', 'View Control Panel Log'), ('46', 'tools_logs', 'view_throttle_log', 'english', 'can_access_logs', 'View Throttle Log'), ('47', 'tools_logs', 'view_search_log', 'english', 'can_access_logs', 'View Search Log'), ('48', 'tools_logs', 'view_email_log', 'english', 'can_access_logs', 'View Email Log'), ('49', 'tools_logs', 'view_developer_log', 'english', 'can_access_logs', 'logs_view_developer_log'), ('50', 'tools_utilities', 'member_import', 'english', 'can_access_utilities', 'Member Import'), ('51', 'tools_utilities', 'import_from_xml', 'english', 'can_access_utilities', 'Import From XML'), ('52', 'tools_utilities', 'translation_tool', 'english', 'can_access_utilities', 'Translation Utility');

-- ----------------------------
--  Table structure for `exp_developer_log`
-- ----------------------------
DROP TABLE IF EXISTS `exp_developer_log`;
CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `template_name` varchar(100) DEFAULT NULL,
  `template_group` varchar(100) DEFAULT NULL,
  `addon_module` varchar(100) DEFAULT NULL,
  `addon_method` varchar(100) DEFAULT NULL,
  `snippets` text,
  `hash` char(32) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_developer_log`
-- ----------------------------
INSERT INTO `exp_developer_log` VALUES ('1', '1387403895', 'n', null, 'set_variable()', '91', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'view-&gt;&lt;var&gt; = &lt;value&gt;;', '0', null, null, null, null, null, 'abedcb5d14594d3fb25fe5acc355d296'), ('2', '1383687739', 'n', null, 'set_variable()', '169', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'view-&gt;&lt;var&gt; = &lt;value&gt;;', '0', null, null, null, null, null, '7d3700ade05392571566cc341f328fcd'), ('3', '1387404074', 'n', null, 'set_variable()', '302', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'view-&gt;&lt;var&gt; = &lt;value&gt;;', '0', null, null, null, null, null, '6983435520ece0588042d5a416844790'), ('4', '1387404074', 'n', null, 'generate_json()', '477', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '9eacf599f3223d030195778c582f4d71'), ('5', '1382659465', 'n', null, 'generate_json()', '801', '/Sites/idaho/httpdocs/system/codeigniter/system/libraries/Javascript.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '97867c4814735ec2640e467b2860ad44'), ('6', '1382659465', 'n', null, 'generate_json()', '797', '/Sites/idaho/httpdocs/system/codeigniter/system/libraries/Javascript.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, 'aa707c9a65f27769b43148352fae2bee'), ('7', '1387404074', 'n', null, 'generate_json()', '501', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '57ea7d07368e869f45670044ecb0c54a'), ('8', '1387404074', 'n', null, 'generate_json()', '535', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, 'c38c3e75876b44cddbdc908c06619c20'), ('9', '1387404074', 'n', null, 'generate_json()', '543', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, 'b3c83b1c756067262ff171bbfec55ea4'), ('10', '1387403899', 'n', null, 'generate_json()', '801', '/Sites/idaho/system/codeigniter/system/libraries/Javascript.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '50cc066b9c5fd9ea9b69b81d7afc2a94'), ('11', '1387403899', 'n', null, 'generate_json()', '797', '/Sites/idaho/system/codeigniter/system/libraries/Javascript.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, 'bcf8fe8ffd2c6664e09e03f90c164588'), ('12', '1382724368', 'n', null, 'set_variable()', '28', '/Sites/idaho/third_party/proxy/mcp.proxy.php', '2.6', 'view-&gt;&lt;var&gt; = &lt;value&gt;;', '0', null, null, null, null, null, '1e86b2a324a166c39f8283869be3b9a2'), ('13', '1382727419', 'n', null, 'set_variable()', '99', '/Sites/idaho/third_party/zoo_triggers/mcp.zoo_triggers.php', '2.6', 'view-&gt;&lt;var&gt; = &lt;value&gt;;', '0', null, null, null, null, null, '8ae824b109b557ab247f28c99b00964e'), ('14', '1382729197', 'n', null, 'generate_json()', '825', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '30ab0d293d967d09d869fa5794de370d'), ('15', '1382823364', 'n', null, 'set_variable()', '240', '/Sites/idaho/third_party/field_editor/mcp.field_editor.php', '2.6', 'view-&gt;&lt;var&gt; = &lt;value&gt;;', '0', null, null, null, null, null, '29c94e7d4f69ee71c953db422363f50b'), ('16', '1387403899', 'n', null, 'generate_json()', '69', '/Sites/idaho/third_party/field_editor/libraries/Package_exporter.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, 'e52605c99d15335c9abf299bbc1bf100'), ('17', '1387582048', 'n', null, 'set_variable()', '91', '/Sites/dar-es-balat/third_party/field_editor/mcp.field_editor.php', '2.6', 'view-&gt;&lt;var&gt; = &lt;value&gt;;', '0', null, null, null, null, null, 'b674fa815dcdd62a8d18ef3732f56642'), ('18', '1387582050', 'n', null, 'set_variable()', '302', '/Sites/dar-es-balat/third_party/field_editor/mcp.field_editor.php', '2.6', 'view-&gt;&lt;var&gt; = &lt;value&gt;;', '0', null, null, null, null, null, '6f0152f9cb0fd7b32a5f57b07692763f'), ('19', '1387582050', 'n', null, 'generate_json()', '477', '/Sites/dar-es-balat/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '3a02e94e03c47fec4999561f25db1276'), ('20', '1387582050', 'n', null, 'generate_json()', '801', '/Sites/dar-es-balat/system/codeigniter/system/libraries/Javascript.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, 'f05b6c6e8852bea534a8f2955dbfc1b1'), ('21', '1387582050', 'n', null, 'generate_json()', '797', '/Sites/dar-es-balat/system/codeigniter/system/libraries/Javascript.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, 'b3dd1d567b00cd01d78ef63abe217ffe'), ('22', '1387582050', 'n', null, 'generate_json()', '501', '/Sites/dar-es-balat/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '0e791211fa1771ebb63767a24d9d3fee'), ('23', '1387582050', 'n', null, 'generate_json()', '535', '/Sites/dar-es-balat/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '1a406fdfd3a6fc123664a832e7869468'), ('24', '1387582050', 'n', null, 'generate_json()', '543', '/Sites/dar-es-balat/third_party/field_editor/mcp.field_editor.php', '2.6', 'the native JSON extension (json_encode())', '0', null, null, null, null, null, '5e31480ea60e1c18ce49e9cc717ca8d6');

-- ----------------------------
--  Table structure for `exp_email_cache`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_cache`;
CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_cache_mg`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_cache_mg`;
CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_cache_ml`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_cache_ml`;
CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_console_cache`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_console_cache`;
CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_tracker`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_tracker`;
CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(45) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_email_tracker`
-- ----------------------------
INSERT INTO `exp_email_tracker` VALUES ('1', '1383333365', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('2', '1383333663', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('3', '1383334120', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('4', '1383334204', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('5', '1383334226', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('6', '1383334246', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('7', '1383334350', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('8', '1383334371', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('9', '1383334609', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('10', '1383334932', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1'), ('11', '1383343547', '127.0.0.1', 'bryan.lewis@gmail.com', 'bryan', '1');

-- ----------------------------
--  Table structure for `exp_entry_versioning`
-- ----------------------------
DROP TABLE IF EXISTS `exp_entry_versioning`;
CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_entry_versioning`
-- ----------------------------
INSERT INTO `exp_entry_versioning` VALUES ('129', '55', '4', '1', '1383337824', 'a:31:{s:8:\"entry_id\";s:2:\"55\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:16:\"Portfolio Topics\";s:10:\"field_id_4\";s:6:\"Topics\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/topic-list\";s:11:\"field_id_25\";s:9:\"portfolio\";s:11:\"field_id_28\";s:1:\"2\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:10:\"entry_date\";s:10:\"1383337440\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:17:\"portfolio-topics1\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('3', '4', '3', '1', '1382723135', 'a:16:{s:8:\"entry_id\";i:4;s:10:\"channel_id\";s:1:\"3\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"First Test Widget Group\";s:9:\"url_title\";s:23:\"first-test-widget-group\";s:10:\"field_id_7\";a:3:{s:9:\"row_order\";a:1:{i:0;s:9:\"row_new_0\";}s:9:\"row_new_0\";a:1:{s:8:\"col_id_1\";a:1:{s:10:\"selections\";a:3:{i:0;s:0:\"\";i:1;s:1:\"3\";i:2;s:1:\"2\";}}}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382723100\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"3\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";}'), ('141', '63', '2', '1', '1383362368', 'a:29:{s:8:\"entry_id\";s:2:\"63\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:22:\"Yet another blog entry\";s:9:\"url_title\";s:22:\"yet-another-blog-entry\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:0:\"\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:4383:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p>\n\n <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat..</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383362220\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"4\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:0:\"\";}'), ('132', '58', '4', '1', '1383338202', 'a:31:{s:8:\"entry_id\";i:58;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:14:\"Video Archives\";s:10:\"field_id_4\";s:8:\"Archives\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:20:\"widgets/archive-list\";s:11:\"field_id_25\";s:6:\"videos\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:10:\"entry_date\";s:10:\"1383338160\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:14:\"video-archives\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('83', '45', '6', '1', '1383089849', 'a:28:{s:8:\"entry_id\";i:45;s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Another test portfolio \";s:9:\"url_title\";s:22:\"another-test-portfolio\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383089760\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('143', '63', '2', '1', '1383362409', 'a:29:{s:8:\"entry_id\";s:2:\"63\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:22:\"Yet another blog entry\";s:9:\"url_title\";s:22:\"yet-another-blog-entry\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:4383:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p>\n\n <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat..</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383362220\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"4\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('136', '62', '5', '1', '1383343817', 'a:27:{s:8:\"entry_id\";i:62;s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:8:\"Nested 1\";s:9:\"url_title\";s:8:\"nested-1\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:11:\"field_id_24\";a:6:{s:7:\"tree_id\";s:1:\"1\";s:7:\"node_id\";s:0:\"\";s:10:\"custom_url\";s:0:\"\";s:5:\"label\";s:9:\"Nested 1b\";s:10:\"parent_lft\";s:1:\"5\";s:13:\"template_path\";s:2:\"10\";}s:10:\"entry_date\";s:10:\"1383343740\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/about/faq/nested-1\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:0:\"\";}'), ('77', '43', '4', '1', '1383076148', 'a:28:{s:8:\"entry_id\";i:43;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:15:\"About this site\";s:9:\"url_title\";s:15:\"about-this-site\";s:10:\"field_id_4\";s:15:\"About this site\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:493:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n<p><a href=\"/about\">Learn more</a></p>\";s:10:\"field_id_6\";s:1:\"0\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383076080\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('76', '42', '4', '1', '1383076061', 'a:28:{s:8:\"entry_id\";s:2:\"42\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:13:\"Home Carousel\";s:9:\"url_title\";s:13:\"home-carousel\";s:10:\"field_id_4\";s:0:\"\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:16:\"widgets/carousel\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:8:\"carousel\";s:10:\"entry_date\";s:10:\"1383075780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_10\";s:0:\"\";}'), ('98', '47', '6', '1', '1383260032', 'a:29:{s:8:\"entry_id\";i:47;s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:5:\"Photo\";s:9:\"url_title\";s:5:\"photo\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383259980\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"4\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('75', '42', '4', '1', '1383075795', 'a:28:{s:8:\"entry_id\";i:42;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:13:\"Home Carousel\";s:9:\"url_title\";s:13:\"home-carousel\";s:10:\"field_id_4\";s:0:\"\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:16:\"widgets/carousel\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383075780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_10\";s:0:\"\";}'), ('74', '41', '5', '1', '1383070258', 'a:27:{s:8:\"entry_id\";i:41;s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:26:\"Frequently Asked Questions\";s:9:\"url_title\";s:26:\"frequently-asked-questions\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:2193:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:11:\"field_id_24\";a:6:{s:7:\"tree_id\";s:1:\"1\";s:7:\"node_id\";s:0:\"\";s:10:\"custom_url\";s:0:\"\";s:5:\"label\";s:3:\"FAQ\";s:10:\"parent_lft\";s:2:\"10\";s:13:\"template_path\";s:2:\"10\";}s:10:\"entry_date\";s:10:\"1383070200\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:10:\"/about/faq\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('130', '56', '4', '1', '1383337898', 'a:31:{s:8:\"entry_id\";i:56;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:11:\"Blog Topics\";s:10:\"field_id_4\";s:14:\"Article Topics\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/topic-list\";s:11:\"field_id_25\";s:4:\"blog\";s:11:\"field_id_28\";s:1:\"2\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:10:\"entry_date\";s:10:\"1383337860\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:11:\"blog-topics\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('131', '57', '4', '1', '1383338096', 'a:31:{s:8:\"entry_id\";i:57;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:13:\"Blog Archives\";s:10:\"field_id_4\";s:16:\"Article Archives\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:20:\"widgets/archive-list\";s:11:\"field_id_25\";s:4:\"blog\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:10:\"entry_date\";s:10:\"1383338040\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:13:\"blog-archives\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('57', '24', '6', '1', '1382933806', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo11.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo11.jpg\";}'), ('58', '24', '6', '1', '1382934084', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo11.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:21:\"With content - medium\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo11.jpg\";}'), ('133', '59', '4', '1', '1383338226', 'a:31:{s:8:\"entry_id\";i:59;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:18:\"Portfolio Archives\";s:10:\"field_id_4\";s:14:\"Photo Archives\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:20:\"widgets/archive-list\";s:11:\"field_id_25\";s:9:\"portfolio\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:10:\"entry_date\";s:10:\"1383338160\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:18:\"portfolio-archives\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('134', '60', '4', '1', '1383338291', 'a:31:{s:8:\"entry_id\";i:60;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Video Topics\";s:10:\"field_id_4\";s:12:\"Video Topics\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/topic-list\";s:11:\"field_id_25\";s:6:\"videos\";s:11:\"field_id_28\";s:1:\"2\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:10:\"entry_date\";s:10:\"1383338220\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:12:\"video-topics\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('126', '54', '4', '1', '1383337271', 'a:30:{s:8:\"entry_id\";s:2:\"54\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:29:\"Widget - Recent Portfolio (4)\";s:10:\"field_id_4\";s:13:\"Newest Photos\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:11:\"field_id_27\";s:5:\"small\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_25\";s:9:\"portfolio\";s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383336480\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:25:\"widget-recent-portfolio-4\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('125', '54', '4', '1', '1383337254', 'a:30:{s:8:\"entry_id\";s:2:\"54\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:29:\"Widget - Recent Portfolio (4)\";s:10:\"field_id_4\";s:13:\"Newest Photos\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:11:\"field_id_27\";s:4:\"tiny\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_25\";s:9:\"portfolio\";s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383336480\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:25:\"widget-recent-portfolio-4\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('36', '11', '5', '1', '1382897377', 'a:26:{s:8:\"entry_id\";s:2:\"11\";s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:15:\"About this site\";s:9:\"url_title\";s:5:\"about\";s:10:\"field_id_8\";s:16:\"About Meta Title\";s:10:\"field_id_9\";s:22:\"About Meta Description\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:2:{i:0;s:0:\"\";i:1;s:2:\"28\";}}s:11:\"field_id_12\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382740320\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:6:\"/about\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('124', '54', '4', '1', '1383337170', 'a:30:{s:8:\"entry_id\";s:2:\"54\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:29:\"Widget - Recent Portfolio (4)\";s:10:\"field_id_4\";s:13:\"Newest Photos\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:11:\"field_id_27\";s:7:\"x-small\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_25\";s:9:\"portfolio\";s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383336480\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:25:\"widget-recent-portfolio-4\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('138', '43', '4', '1', '1383362037', 'a:31:{s:8:\"entry_id\";s:2:\"43\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:15:\"About this site\";s:10:\"field_id_4\";s:15:\"About this site\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:178:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</p>\n\n <p><a href=\"/about\">Learn more</a></p>\";s:10:\"field_id_6\";s:1:\"0\";s:11:\"field_id_25\";s:4:\"blog\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:10:\"entry_date\";s:10:\"1383076080\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:15:\"about-this-site\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_10\";s:0:\"\";}'), ('139', '63', '2', '1', '1383362337', 'a:29:{s:8:\"entry_id\";i:63;s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:22:\"Yet another blog entry\";s:9:\"url_title\";s:22:\"yet-another-blog-entry\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:4378:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p>\n\n <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat..</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383362220\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"4\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('59', '24', '6', '1', '1382995440', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo11.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:21:\"With content - medium\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo11.jpg\";}'), ('42', '31', '5', '1', '1382921963', 'a:25:{s:8:\"entry_id\";i:31;s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:14:\"Privacy Policy\";s:9:\"url_title\";s:14:\"privacy-policy\";s:10:\"field_id_8\";s:18:\"Privacy Meta Title\";s:10:\"field_id_9\";s:24:\"Privacy Meta Description\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382921880\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:15:\"/privacy-policy\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('43', '32', '5', '1', '1382921991', 'a:25:{s:8:\"entry_id\";i:32;s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:18:\"Terms & Conditions\";s:9:\"url_title\";s:16:\"terms-conditions\";s:10:\"field_id_8\";s:15:\"Terms metatitle\";s:10:\"field_id_9\";s:21:\"Terms metadescription\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:10:\"field_id_2\";s:2193:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382921940\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:21:\"/terms-and-conditions\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('44', '31', '5', '1', '1382922012', 'a:25:{s:8:\"entry_id\";s:2:\"31\";s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:14:\"Privacy Policy\";s:9:\"url_title\";s:14:\"privacy-policy\";s:10:\"field_id_8\";s:18:\"Privacy Meta Title\";s:10:\"field_id_9\";s:24:\"Privacy Meta Description\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:10:\"field_id_2\";s:2193:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382921880\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:15:\"/privacy-policy\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('60', '6', '2', '1', '1382995452', 'a:29:{s:8:\"entry_id\";s:1:\"6\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 2\";s:9:\"url_title\";s:17:\"test-blog-entry-2\";s:10:\"field_id_8\";s:28:\"Test Blog Entry Meta Title 2\";s:10:\"field_id_9\";s:34:\"Test Blog Entry Meta Description 2\";s:22:\"field_id_3_hidden_file\";s:10:\"images.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1371493920\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"4\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:21:\"{filedir_1}images.jpg\";}'), ('61', '19', '1', '1', '1382995459', 'a:30:{s:8:\"entry_id\";s:2:\"19\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjEiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:18:\"Another Test Video\";s:9:\"url_title\";s:18:\"another-test-video\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"field_id_1\";a:1:{s:6:\"videos\";a:1:{i:0;a:1:{s:8:\"video_id\";s:1:\"3\";}}}s:10:\"entry_date\";s:10:\"1382827800\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:3:{i:0;s:1:\"1\";i:1;s:1:\"3\";i:2;s:1:\"5\";}s:11:\"new_channel\";s:1:\"1\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:0:\"\";}'), ('47', '24', '6', '1', '1382932484', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:9:\"As banner\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('48', '24', '6', '1', '1382932616', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:21:\"With content - medium\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}');
INSERT INTO `exp_entry_versioning` VALUES ('49', '24', '6', '1', '1382932629', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - small\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('50', '24', '6', '1', '1382932789', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('51', '24', '6', '1', '1382932809', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('52', '24', '6', '1', '1382932820', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:9:\"As banner\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('55', '8', '2', '1', '1382933500', 'a:29:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 4\";s:9:\"url_title\";s:17:\"test-blog-entry-4\";s:10:\"field_id_8\";s:28:\"Test Blog Entry Meta Title 4\";s:10:\"field_id_9\";s:34:\"Test Blog Entry Meta Description 4\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382725860\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"3\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo04.jpg\";}'), ('56', '24', '6', '1', '1382933728', 'a:29:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:12:\"Test Gallery\";s:9:\"url_title\";s:12:\"test-gallery\";s:10:\"field_id_8\";s:18:\"Gallery Meta Title\";s:10:\"field_id_9\";s:24:\"Gallery Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:6:{s:9:\"row_order\";a:4:{i:0;s:8:\"row_id_2\";i:1;s:8:\"row_id_3\";i:2;s:8:\"row_id_4\";i:3;s:8:\"row_id_5\";}s:8:\"row_id_2\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo01.jpg\";}s:8:\"col_id_3\";s:17:\"This is the title\";s:8:\"col_id_4\";s:20:\"This is the caption.\";}s:8:\"row_id_3\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo04.jpg\";}s:8:\"col_id_3\";s:21:\"This is another title\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_4\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo11.jpg\";}s:8:\"col_id_3\";s:14:\"This is a tree\";s:8:\"col_id_4\";s:59:\"This is another caption but it is going to be a bit longer.\";}s:8:\"row_id_5\";a:3:{s:8:\"col_id_2\";a:2:{s:7:\"filedir\";s:1:\"1\";s:8:\"filename\";s:12:\"NatGeo02.jpg\";}s:8:\"col_id_3\";s:20:\"Polar Bears are cool\";s:8:\"col_id_4\";s:24:\"This is another caption.\";}s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382891880\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('62', '19', '1', '1', '1382995721', 'a:30:{s:8:\"entry_id\";s:2:\"19\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:18:\"Another Test Video\";s:9:\"url_title\";s:18:\"another-test-video\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"field_id_1\";a:1:{s:6:\"videos\";a:1:{i:0;a:1:{s:8:\"video_id\";s:1:\"3\";}}}s:10:\"entry_date\";s:10:\"1382827800\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:3:{i:0;s:1:\"1\";i:1;s:1:\"3\";i:2;s:1:\"5\";}s:11:\"new_channel\";s:1:\"1\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('63', '21', '1', '1', '1382995755', 'a:30:{s:8:\"entry_id\";s:2:\"21\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:20:\"First Featured Video\";s:9:\"url_title\";s:20:\"first-featured-video\";s:10:\"field_id_8\";s:46:\"Eric Hovind OFFICIALLY accepts atheism is true\";s:10:\"field_id_9\";s:447:\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:1340:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p> </p>\n\n <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"field_id_1\";a:1:{s:6:\"videos\";a:1:{i:0;a:1:{s:8:\"video_id\";s:1:\"4\";}}}s:10:\"entry_date\";s:10:\"1382828160\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:3:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"5\";}s:11:\"new_channel\";s:1:\"1\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('142', '63', '2', '1', '1383362382', 'a:29:{s:8:\"entry_id\";s:2:\"63\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:22:\"Yet another blog entry\";s:9:\"url_title\";s:22:\"yet-another-blog-entry\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:4383:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p>\n\n <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat..</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383362220\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"4\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('68', '5', '2', '1', '1382996418', 'a:29:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 1\";s:9:\"url_title\";s:17:\"test-blog-entry-1\";s:10:\"field_id_8\";s:28:\"Test Blog Entry 1 Meta Title\";s:10:\"field_id_9\";s:34:\"Test Blog Entry 1 Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1367433120\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('140', '63', '2', '1', '1383362355', 'a:29:{s:8:\"entry_id\";s:2:\"63\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:22:\"Yet another blog entry\";s:9:\"url_title\";s:22:\"yet-another-blog-entry\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:4382:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> </p>\n\n <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat</p>\n\n <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n\n <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\n\n <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat..</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383362220\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"4\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('70', '11', '5', '1', '1383069199', 'a:26:{s:8:\"entry_id\";s:2:\"11\";s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:15:\"About this site\";s:9:\"url_title\";s:5:\"about\";s:10:\"field_id_8\";s:16:\"About Meta Title\";s:10:\"field_id_9\";s:22:\"About Meta Description\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382740320\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:6:\"/about\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('123', '54', '4', '1', '1383336681', 'a:29:{s:8:\"entry_id\";s:2:\"54\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:29:\"Widget - Recent Portfolio (4)\";s:10:\"field_id_4\";s:13:\"Newest Photos\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_25\";s:9:\"portfolio\";s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383336480\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:25:\"widget-recent-portfolio-4\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('81', '8', '2', '1', '1383086858', 'a:29:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 4\";s:9:\"url_title\";s:17:\"test-blog-entry-4\";s:10:\"field_id_8\";s:28:\"Test Blog Entry Meta Title 4\";s:10:\"field_id_9\";s:34:\"Test Blog Entry Meta Description 4\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:21:\"With content - medium\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382725860\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"3\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo04.jpg\";}'), ('84', '45', '6', '1', '1383089855', 'a:29:{s:8:\"entry_id\";s:2:\"45\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:22:\"Another test portfolio\";s:9:\"url_title\";s:22:\"another-test-portfolio\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383089760\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"5\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('85', '45', '6', '1', '1383090027', 'a:29:{s:8:\"entry_id\";s:2:\"45\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:22:\"Another test portfolio\";s:9:\"url_title\";s:22:\"another-test-portfolio\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383089760\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"5\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('86', '45', '6', '1', '1383090044', 'a:29:{s:8:\"entry_id\";s:2:\"45\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:22:\"Another test portfolio\";s:9:\"url_title\";s:22:\"another-test-portfolio\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383089760\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"5\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('87', '11', '5', '1', '1383090725', 'a:26:{s:8:\"entry_id\";s:2:\"11\";s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:9:\"Our Story\";s:9:\"url_title\";s:5:\"about\";s:10:\"field_id_8\";s:16:\"About Meta Title\";s:10:\"field_id_9\";s:22:\"About Meta Description\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1382740320\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:6:\"/about\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('88', '5', '2', '1', '1383187689', 'a:29:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 1\";s:9:\"url_title\";s:17:\"test-blog-entry-1\";s:10:\"field_id_8\";s:28:\"Test Blog Entry 1 Meta Title\";s:10:\"field_id_9\";s:34:\"Test Blog Entry 1 Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:9:\"As banner\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1367433120\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('89', '5', '2', '1', '1383187765', 'a:29:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 1\";s:9:\"url_title\";s:17:\"test-blog-entry-1\";s:10:\"field_id_8\";s:28:\"Test Blog Entry 1 Meta Title\";s:10:\"field_id_9\";s:34:\"Test Blog Entry 1 Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:21:\"With content - medium\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1367433120\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('90', '5', '2', '1', '1383187818', 'a:29:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 1\";s:9:\"url_title\";s:17:\"test-blog-entry-1\";s:10:\"field_id_8\";s:28:\"Test Blog Entry 1 Meta Title\";s:10:\"field_id_9\";s:34:\"Test Blog Entry 1 Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - small\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1367433120\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('91', '5', '2', '1', '1383187831', 'a:29:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 1\";s:9:\"url_title\";s:17:\"test-blog-entry-1\";s:10:\"field_id_8\";s:28:\"Test Blog Entry 1 Meta Title\";s:10:\"field_id_9\";s:34:\"Test Blog Entry 1 Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:9:\"As banner\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1367433120\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('92', '5', '2', '1', '1383187847', 'a:29:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"2\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjIiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Test Blog Entry 1\";s:9:\"url_title\";s:17:\"test-blog-entry-1\";s:10:\"field_id_8\";s:28:\"Test Blog Entry 1 Meta Title\";s:10:\"field_id_9\";s:34:\"Test Blog Entry 1 Meta Description\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1367433120\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"2\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('127', '55', '4', '1', '1383337745', 'a:31:{s:8:\"entry_id\";i:55;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:16:\"Portfolio Topics\";s:10:\"field_id_4\";s:6:\"Topics\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/topic-list\";s:11:\"field_id_25\";s:9:\"portfolio\";s:11:\"field_id_28\";s:1:\"1\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:10:\"entry_date\";s:10:\"1383337440\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:16:\"portfolio-topics\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('137', '62', '5', '1', '1383343820', 'a:27:{s:8:\"entry_id\";s:2:\"62\";s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:8:\"Nested 1\";s:9:\"url_title\";s:8:\"nested-1\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:11:\"field_id_24\";a:6:{s:7:\"tree_id\";s:1:\"1\";s:7:\"node_id\";s:2:\"29\";s:10:\"custom_url\";s:0:\"\";s:5:\"label\";s:9:\"Nested 1b\";s:10:\"parent_lft\";s:1:\"5\";s:13:\"template_path\";s:2:\"10\";}s:10:\"entry_date\";s:10:\"1383343740\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/about/faq/nested-1\";s:24:\"pages__pages_template_id\";s:2:\"10\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('95', '46', '4', '1', '1383255886', 'a:28:{s:8:\"entry_id\";i:46;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:17:\"Get in touch now!\";s:9:\"url_title\";s:16:\"get-in-touch-now\";s:10:\"field_id_4\";s:13:\"Get in touch!\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:129:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>\n\n<p><a class=\"button\" href=\"/about\">More</a></p>\";s:10:\"field_id_6\";s:1:\"0\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:0:\"\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:5:\"panel\";s:10:\"entry_date\";s:10:\"1383255780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('99', '48', '6', '1', '1383260046', 'a:29:{s:8:\"entry_id\";i:48;s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:5:\"Photo\";s:9:\"url_title\";s:5:\"photo\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo11.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383259980\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo11.jpg\";}'), ('100', '49', '6', '1', '1383260063', 'a:29:{s:8:\"entry_id\";i:49;s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:5:\"Photo\";s:9:\"url_title\";s:5:\"photo\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383260040\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"5\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('101', '49', '6', '1', '1383260195', 'a:29:{s:8:\"entry_id\";s:2:\"49\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:5:\"Photo\";s:9:\"url_title\";s:6:\"photo2\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"Aerial04.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383260040\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"5\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}Aerial04.jpg\";}'), ('102', '47', '6', '1', '1383260222', 'a:29:{s:8:\"entry_id\";s:2:\"47\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:5:\"Photo\";s:9:\"url_title\";s:5:\"photo\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo02.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383259980\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"4\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo02.jpg\";}'), ('103', '48', '6', '1', '1383260229', 'a:29:{s:8:\"entry_id\";s:2:\"48\";s:10:\"channel_id\";s:1:\"6\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjYiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:5:\"Photo\";s:9:\"url_title\";s:6:\"photo1\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_id_9\";s:0:\"\";s:22:\"field_id_3_hidden_file\";s:12:\"NatGeo11.jpg\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:20:\"With content - large\";s:10:\"field_id_2\";s:10:\"<p>​</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383259980\";s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:8:\"category\";a:1:{i:0;s:1:\"3\";}s:11:\"new_channel\";s:1:\"6\";s:6:\"status\";s:6:\"Public\";s:6:\"author\";s:1:\"1\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:23:\"{filedir_1}NatGeo11.jpg\";}'), ('119', '53', '4', '1', '1383336483', 'a:29:{s:8:\"entry_id\";i:53;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:26:\"Widget - Recent Blog Posts\";s:10:\"field_id_4\";s:0:\"\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_25\";s:4:\"blog\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:2:\"No\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383336420\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:24:\"widget-recent-blog-posts\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('105', '50', '4', '1', '1383276880', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:23:\"Configurable Entry List\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:9:\"portfolio\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('106', '50', '4', '1', '1383276892', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:23:\"Configurable Entry List\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:6:\"videos\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('122', '53', '4', '1', '1383336668', 'a:29:{s:8:\"entry_id\";s:2:\"53\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:30:\"Widget - Recent Blog Posts (5)\";s:10:\"field_id_4\";s:12:\"Recent Posts\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_25\";s:4:\"blog\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:2:\"No\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383336420\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:24:\"widget-recent-blog-posts\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}');
INSERT INTO `exp_entry_versioning` VALUES ('108', '52', '5', '1', '1383333241', 'a:27:{s:8:\"entry_id\";i:52;s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"5\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:10:\"Contact Us\";s:9:\"url_title\";s:7:\"contact\";s:10:\"field_id_8\";s:18:\"Contact meta title\";s:10:\"field_id_9\";s:24:\"Contact meta description\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:3:{i:0;s:0:\"\";i:1;s:2:\"43\";i:2;s:2:\"50\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:11:\"field_id_24\";a:6:{s:7:\"tree_id\";s:1:\"1\";s:7:\"node_id\";s:0:\"\";s:10:\"custom_url\";s:0:\"\";s:5:\"label\";s:10:\"Contact Us\";s:10:\"parent_lft\";s:1:\"1\";s:13:\"template_path\";s:2:\"10\";}s:10:\"entry_date\";s:10:\"1383333120\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:8:\"/contact\";s:24:\"pages__pages_template_id\";s:2:\"12\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:0:\"\";}'), ('109', '52', '5', '1', '1383333305', 'a:27:{s:8:\"entry_id\";s:2:\"52\";s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:10:\"Contact Us\";s:9:\"url_title\";s:7:\"contact\";s:10:\"field_id_8\";s:18:\"Contact meta title\";s:10:\"field_id_9\";s:24:\"Contact meta description\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:3:{i:0;s:0:\"\";i:1;s:2:\"43\";i:2;s:2:\"50\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:11:\"field_id_24\";a:6:{s:7:\"tree_id\";s:1:\"1\";s:7:\"node_id\";s:2:\"26\";s:10:\"custom_url\";s:0:\"\";s:5:\"label\";s:10:\"Contact Us\";s:10:\"parent_lft\";s:1:\"1\";s:13:\"template_path\";s:2:\"42\";}s:10:\"entry_date\";s:10:\"1383333120\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:8:\"/contact\";s:24:\"pages__pages_template_id\";s:2:\"12\";s:6:\"submit\";s:6:\"Submit\";s:10:\"field_id_3\";s:0:\"\";}'), ('110', '52', '5', '1', '1383334918', 'a:26:{s:8:\"entry_id\";s:2:\"52\";s:10:\"channel_id\";s:1:\"5\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:10:\"Contact Us\";s:9:\"url_title\";s:7:\"contact\";s:10:\"field_id_8\";s:18:\"Contact meta title\";s:10:\"field_id_9\";s:24:\"Contact meta description\";s:22:\"field_id_3_hidden_file\";s:0:\"\";s:21:\"field_id_3_hidden_dir\";s:1:\"1\";s:11:\"field_id_23\";s:2:\"No\";s:10:\"field_id_2\";s:453:\"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\";s:11:\"field_id_11\";a:1:{s:10:\"selections\";a:3:{i:0;s:0:\"\";i:1;s:2:\"43\";i:2;s:2:\"50\";}}s:11:\"field_id_20\";a:1:{s:17:\"trigger_revisions\";s:1:\"1\";}s:10:\"entry_date\";s:10:\"1383333120\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:8:\"/contact\";s:24:\"pages__pages_template_id\";s:2:\"42\";s:13:\"save_revision\";s:13:\"Save Revision\";s:10:\"field_id_3\";s:0:\"\";}'), ('111', '50', '4', '1', '1383335407', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:23:\"Configurable Entry List\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:4:\"blog\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('112', '50', '4', '1', '1383335568', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:23:\"Configurable Entry List\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:4:\"blog\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('113', '50', '4', '1', '1383335714', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:23:\"Configurable Entry List\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:2:\"No\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:4:\"blog\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('114', '50', '4', '1', '1383335765', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:23:\"Configurable Entry List\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:4:\"blog\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('115', '50', '4', '1', '1383335835', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:23:\"Configurable Entry List\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:1:\"1\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:4:\"blog\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('116', '50', '4', '1', '1383336139', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:23:\"Configurable Entry List\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:3:\"1|3\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:4:\"blog\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('117', '50', '4', '1', '1383336172', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:23:\"Configurable Entry List\";s:9:\"url_title\";s:23:\"configurable-entry-list\";s:10:\"field_id_4\";s:15:\"Featured Videos\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:1:\"1\";s:11:\"field_id_16\";s:1:\"3\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:6:\"videos\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('118', '50', '4', '1', '1383336195', 'a:29:{s:8:\"entry_id\";s:2:\"50\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:28:\"Widget - Featured Videos (3)\";s:9:\"url_title\";s:15:\"featured-videos\";s:10:\"field_id_4\";s:15:\"Featured Videos\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_15\";s:1:\"1\";s:11:\"field_id_16\";s:1:\"3\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:11:\"field_id_25\";s:6:\"videos\";s:10:\"entry_date\";s:10:\"1383276780\";s:15:\"expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('120', '54', '4', '1', '1383336515', 'a:29:{s:8:\"entry_id\";i:54;s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:29:\"Widget - Recent Portfolio (4)\";s:10:\"field_id_4\";s:0:\"\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_25\";s:9:\"portfolio\";s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_id_18\";s:3:\"Yes\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383336480\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:25:\"widget-recent-portfolio-4\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}'), ('121', '53', '4', '1', '1383336527', 'a:29:{s:8:\"entry_id\";s:2:\"53\";s:10:\"channel_id\";s:1:\"4\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjQiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:30:\"Widget - Recent Blog Posts (5)\";s:10:\"field_id_4\";s:0:\"\";s:23:\"field_id_10_hidden_file\";s:0:\"\";s:22:\"field_id_10_hidden_dir\";s:1:\"1\";s:10:\"field_id_5\";s:10:\"<p>​</p>\";s:10:\"field_id_6\";s:18:\"widgets/entry-list\";s:11:\"field_id_25\";s:4:\"blog\";s:11:\"field_id_16\";s:1:\"5\";s:11:\"field_id_18\";s:2:\"No\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_id_21\";a:1:{s:10:\"selections\";a:1:{i:0;s:0:\"\";}}s:11:\"field_id_22\";s:0:\"\";s:10:\"entry_date\";s:10:\"1383336420\";s:15:\"expiration_date\";s:0:\"\";s:9:\"url_title\";s:24:\"widget-recent-blog-posts\";s:11:\"new_channel\";s:1:\"4\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"1\";s:18:\"versioning_enabled\";s:1:\"y\";s:24:\"pages__pages_template_id\";s:2:\"11\";s:16:\"pages__pages_uri\";s:19:\"/example/pages/uri/\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_10\";s:0:\"\";}');

-- ----------------------------
--  Table structure for `exp_extensions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_extensions`;
CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_extensions`
-- ----------------------------
INSERT INTO `exp_extensions` VALUES ('2', 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', '10', '1.0.1', 'y'), ('3', 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', '10', '1.0.1', 'y'), ('4', 'Low_reorder_ext', 'entry_submission_end', 'entry_submission_end', '', '5', '2.2.1', 'y'), ('5', 'Low_reorder_ext', 'channel_entries_query_result', 'channel_entries_query_result', '', '5', '2.2.1', 'y'), ('6', 'Low_reorder_ext', 'low_search_post_search', 'low_search_post_search', '', '5', '2.2.1', 'y'), ('7', 'Low_reorder_ext', 'playa_parse_relationships', 'playa_parse_relationships', '', '5', '2.2.1', 'y'), ('30', 'Low_variables_ext', 'template_fetch_template', 'template_fetch_template', 'a:8:{s:11:\"license_key\";s:36:\"43083f18-441a-4444-bd6d-2809788b741e\";s:10:\"can_manage\";a:1:{i:0;s:1:\"1\";}s:11:\"clear_cache\";s:1:\"y\";s:16:\"register_globals\";s:1:\"y\";s:20:\"register_member_data\";s:1:\"y\";s:13:\"save_as_files\";s:1:\"y\";s:9:\"file_path\";s:29:\"/Sites/biggerfuture/variables\";s:13:\"enabled_types\";a:17:{i:0;s:12:\"low_checkbox\";i:1;s:18:\"low_checkbox_group\";i:2;s:8:\"low_grid\";i:3;s:14:\"low_reorder_vt\";i:4;s:6:\"matrix\";i:5;s:5:\"playa\";i:6;s:15:\"low_radio_group\";i:7;s:10:\"low_select\";i:8;s:21:\"low_select_categories\";i:9;s:19:\"low_select_channels\";i:10;s:18:\"low_select_entries\";i:11;s:16:\"low_select_files\";i:12;s:9:\"low_table\";i:13;s:23:\"template_snippet_select\";i:14;s:14:\"low_text_input\";i:15;s:7:\"low_rte\";i:16;s:12:\"low_textarea\";}}', '2', '2.4.0', 'y'), ('10', 'Field_editor_ext', 'cp_menu_array', 'cp_menu_array', '', '10', '1.0.4', 'y'), ('11', 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', '10', '2.5.6', 'y'), ('12', 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', '9', '4.4.5', 'y'), ('13', 'Stash_ext', 'template_fetch_template', 'template_fetch_template', '', '10', '2.4.5', 'y'), ('14', 'Stash_ext', 'template_post_parse', 'template_post_parse', '', '10', '2.4.5', 'y'), ('17', 'Mx_cloner_ext', 'publish_form_entry_data', 'publish_form_entry_data', 'a:3:{s:12:\"title_suffix\";s:0:\"\";s:10:\"url_suffix\";s:0:\"\";s:11:\"update_time\";s:1:\"y\";}', '8', '1.2.1', 'y'), ('22', 'Zoo_triggers_ext', 'hook_sessions_start', 'sessions_start', 'a:2:{s:8:\"triggers\";a:4:{s:3:\"tag\";a:1:{i:0;s:3:\"tag\";}s:8:\"category\";a:2:{i:0;s:5:\"topic\";i:1;s:8:\"category\";}s:7:\"archive\";a:1:{i:0;s:7:\"archive\";}s:6:\"author\";a:1:{i:0;s:6:\"author\";}}s:8:\"settings\";a:20:{s:16:\"entries_operator\";s:1:\"|\";s:31:\"entries_title_categories_prefix\";s:11:\" in topics \";s:32:\"entries_title_categories_postfix\";s:0:\"\";s:34:\"entries_title_categories_separator\";s:2:\", \";s:39:\"entries_title_categories_separator_last\";s:5:\" and \";s:28:\"entries_title_archives_first\";s:5:\"month\";s:29:\"entries_title_archives_prefix\";s:4:\" in \";s:30:\"entries_title_archives_postfix\";s:1:\".\";s:32:\"entries_title_archives_separator\";s:1:\" \";s:27:\"entries_title_archives_year\";s:1:\"Y\";s:28:\"entries_title_archives_month\";s:1:\"F\";s:24:\"entries_title_tag_prefix\";b:0;s:25:\"entries_title_tag_postfix\";b:0;s:27:\"entries_title_tag_separator\";b:0;s:32:\"entries_title_tag_separator_last\";b:0;s:35:\"entries_title_tag_websafe_separator\";b:0;s:27:\"entries_title_author_prefix\";s:11:\" posted by \";s:28:\"entries_title_author_postfix\";s:0:\"\";s:30:\"entries_title_author_separator\";s:2:\", \";s:35:\"entries_title_author_separator_last\";s:5:\" and \";}}', '9', '1.2.2', 'y'), ('23', 'Zoo_triggers_ext', 'hook_channel_module_create_pagination', 'channel_module_create_pagination', '', '10', '1.2.2', 'y'), ('24', 'Zoo_triggers_ext', 'hook_cp_css_end', 'cp_css_end', '', '1', '1.2.2', 'y'), ('25', 'Zoo_triggers_ext', 'hook_cp_js_end', 'cp_js_end', '', '1', '1.2.2', 'y'), ('26', 'Taxonomy_ext', 'entry_submission_end', 'entry_submission_end', 'a:0:{}', '10', '3.0', 'y'), ('27', 'Taxonomy_ext', 'update_multi_entries_loop', 'update_multi_entries_loop', 'a:0:{}', '10', '3.0', 'y'), ('28', 'Taxonomy_ext', 'cp_menu_array', 'cp_menu_array', 'a:0:{}', '10', '3.0', 'y'), ('29', 'Low_variables_ext', 'sessions_end', 'sessions_end', 'a:8:{s:11:\"license_key\";s:36:\"43083f18-441a-4444-bd6d-2809788b741e\";s:10:\"can_manage\";a:1:{i:0;s:1:\"1\";}s:11:\"clear_cache\";s:1:\"y\";s:16:\"register_globals\";s:1:\"y\";s:20:\"register_member_data\";s:1:\"y\";s:13:\"save_as_files\";s:1:\"y\";s:9:\"file_path\";s:29:\"/Sites/biggerfuture/variables\";s:13:\"enabled_types\";a:17:{i:0;s:12:\"low_checkbox\";i:1;s:18:\"low_checkbox_group\";i:2;s:8:\"low_grid\";i:3;s:14:\"low_reorder_vt\";i:4;s:6:\"matrix\";i:5;s:5:\"playa\";i:6;s:15:\"low_radio_group\";i:7;s:10:\"low_select\";i:8;s:21:\"low_select_categories\";i:9;s:19:\"low_select_channels\";i:10;s:18:\"low_select_entries\";i:11;s:16:\"low_select_files\";i:12;s:9:\"low_table\";i:13;s:23:\"template_snippet_select\";i:14;s:14:\"low_text_input\";i:15;s:7:\"low_rte\";i:16;s:12:\"low_textarea\";}}', '2', '2.4.0', 'y');

-- ----------------------------
--  Table structure for `exp_field_formatting`
-- ----------------------------
DROP TABLE IF EXISTS `exp_field_formatting`;
CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=125 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_field_formatting`
-- ----------------------------
INSERT INTO `exp_field_formatting` VALUES ('1', '1', 'none'), ('2', '1', 'br'), ('3', '1', 'markdown'), ('4', '1', 'xhtml'), ('5', '2', 'none'), ('6', '2', 'br'), ('7', '2', 'markdown'), ('8', '2', 'xhtml'), ('9', '3', 'none'), ('10', '3', 'br'), ('11', '3', 'markdown'), ('12', '3', 'xhtml'), ('13', '4', 'none'), ('14', '4', 'br'), ('15', '4', 'markdown'), ('16', '4', 'xhtml'), ('17', '5', 'none'), ('18', '5', 'br'), ('19', '5', 'markdown'), ('20', '5', 'xhtml'), ('21', '6', 'none'), ('22', '6', 'br'), ('23', '6', 'markdown'), ('24', '6', 'xhtml'), ('80', '20', 'xhtml'), ('79', '20', 'markdown'), ('78', '20', 'br'), ('29', '8', 'none'), ('30', '8', 'br'), ('31', '8', 'markdown'), ('32', '8', 'xhtml'), ('33', '9', 'none'), ('34', '9', 'br'), ('35', '9', 'markdown'), ('36', '9', 'xhtml'), ('37', '10', 'none'), ('38', '10', 'br'), ('39', '10', 'markdown'), ('40', '10', 'xhtml'), ('41', '11', 'none'), ('42', '11', 'br'), ('43', '11', 'markdown'), ('44', '11', 'xhtml'), ('91', '23', 'markdown'), ('90', '23', 'br'), ('89', '23', 'none'), ('59', '15', 'markdown'), ('58', '15', 'br'), ('57', '15', 'none'), ('83', '21', 'markdown'), ('82', '21', 'br'), ('81', '21', 'none'), ('77', '20', 'none'), ('60', '15', 'xhtml'), ('61', '16', 'none'), ('62', '16', 'br'), ('63', '16', 'markdown'), ('64', '16', 'xhtml'), ('71', '18', 'markdown'), ('70', '18', 'br'), ('69', '18', 'none'), ('72', '18', 'xhtml'), ('84', '21', 'xhtml'), ('85', '22', 'none'), ('86', '22', 'br'), ('87', '22', 'markdown'), ('88', '22', 'xhtml'), ('92', '23', 'xhtml'), ('93', '24', 'none'), ('94', '24', 'br'), ('95', '24', 'markdown'), ('96', '24', 'xhtml'), ('97', '25', 'none'), ('98', '25', 'br'), ('99', '25', 'markdown'), ('100', '25', 'xhtml'), ('107', '27', 'markdown'), ('106', '27', 'br'), ('105', '27', 'none'), ('108', '27', 'xhtml'), ('109', '28', 'none'), ('110', '28', 'br'), ('111', '28', 'markdown'), ('112', '28', 'xhtml'), ('113', '29', 'none'), ('114', '29', 'br'), ('115', '29', 'markdown'), ('116', '29', 'xhtml'), ('117', '30', 'none'), ('118', '30', 'br'), ('119', '30', 'markdown'), ('120', '30', 'xhtml'), ('121', '31', 'none'), ('122', '31', 'br'), ('123', '31', 'markdown'), ('124', '31', 'xhtml');

-- ----------------------------
--  Table structure for `exp_field_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_field_groups`;
CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_field_groups`
-- ----------------------------
INSERT INTO `exp_field_groups` VALUES ('1', '1', 'Global Fields'), ('2', '1', 'Widgets');

-- ----------------------------
--  Table structure for `exp_fieldtypes`
-- ----------------------------
DROP TABLE IF EXISTS `exp_fieldtypes`;
CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_fieldtypes`
-- ----------------------------
INSERT INTO `exp_fieldtypes` VALUES ('1', 'select', '1.0', 'YTowOnt9', 'n'), ('2', 'text', '1.0', 'YTowOnt9', 'n'), ('3', 'textarea', '1.0', 'YTowOnt9', 'n'), ('4', 'date', '1.0', 'YTowOnt9', 'n'), ('5', 'file', '1.0', 'YTowOnt9', 'n'), ('6', 'grid', '1.0', 'YTowOnt9', 'n'), ('7', 'multi_select', '1.0', 'YTowOnt9', 'n'), ('8', 'checkboxes', '1.0', 'YTowOnt9', 'n'), ('9', 'radio', '1.0', 'YTowOnt9', 'n'), ('10', 'relationship', '1.0', 'YTowOnt9', 'n'), ('11', 'rte', '1.0', 'YTowOnt9', 'n'), ('18', 'low_variables', '2.4.0', 'YTowOnt9', 'n'), ('13', 'matrix', '2.5.6', 'YTowOnt9', 'y'), ('14', 'playa', '4.4.5', 'YToyOntzOjExOiJsaWNlbnNlX2tleSI7czowOiIiO3M6MTA6ImZpbHRlcl9taW4iO3M6MToiNSI7fQ==', 'y'), ('15', 'channel_videos', '3.1.3', 'YTowOnt9', 'n'), ('16', 'template_snippet_select', '1.2.6', 'YTowOnt9', 'n'), ('17', 'taxonomy', '3.0.8.2', 'YTowOnt9', 'n'), ('19', 'tagger', '3.2.1', 'YTowOnt9', 'n');

-- ----------------------------
--  Table structure for `exp_file_categories`
-- ----------------------------
DROP TABLE IF EXISTS `exp_file_categories`;
CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_file_dimensions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_file_dimensions`;
CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_file_dimensions`
-- ----------------------------
INSERT INTO `exp_file_dimensions` VALUES ('1', '1', '1', 'feature-column', 'feature-column', 'crop', '300', '75', '0'), ('2', '1', '1', 'thumb', 'thumb', 'crop', '100', '100', '0'), ('4', '1', '1', 'small', 'small', 'constrain', '150', '0', '0'), ('5', '1', '1', 'large', 'large', 'constrain', '1024', '0', '0'), ('6', '1', '1', 'medium', 'medium', 'constrain', '640', '0', '0'), ('7', '1', '1', 'category', 'category', 'crop', '200', '60', '0'), ('8', '1', '1', 'banner', 'banner', 'crop', '1200', '200', '0'), ('9', '1', '1', 'tiny', 'tiny', 'constrain', '50', '50', '0'), ('10', '1', '1', 'video', 'video', 'constrain', '75', '42', '0'), ('11', '1', '1', 'video-medium', 'video-medium', 'constrain', '200', '150', '0'), ('12', '1', '1', 'x-small', 'x-small', 'none', '25', '25', '0');

-- ----------------------------
--  Table structure for `exp_file_watermarks`
-- ----------------------------
DROP TABLE IF EXISTS `exp_file_watermarks`;
CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_files`
-- ----------------------------
DROP TABLE IF EXISTS `exp_files`;
CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_files`
-- ----------------------------
INSERT INTO `exp_files` VALUES ('1', '1', 'images.jpg', '1', '/Sites/idaho/httpdocs/images/posts/images.jpg', 'image/jpeg', 'images.jpg', '8260', null, null, null, '1', '1382658708', '1', '1383336906', '149 339'), ('2', '1', 'NatGeo01.jpg', '1', '/Sites/idaho/httpdocs/images/posts/NatGeo01.jpg', 'image/jpeg', 'NatGeo01.jpg', '288551', null, null, null, '1', '1382911422', '1', '1383336905', '750 1200'), ('3', '1', 'NatGeo02.jpg', '1', '/Sites/idaho/httpdocs/images/posts/NatGeo02.jpg', 'image/jpeg', 'NatGeo02.jpg', '125947', '', 'NatGeo', 'Upper Sumeria, 1999', '1', '1382911836', '1', '1383336905', '1024 1639'), ('4', '1', 'NatGeo04.jpg', '1', '/Sites/idaho/httpdocs/images/posts/NatGeo04.jpg', 'image/jpeg', 'NatGeo04.jpg', '355852', '', 'NatGeo', '', '1', '1382911975', '1', '1383336905', '1024 1639'), ('5', '1', 'NatGeo11.jpg', '1', '/Sites/idaho/httpdocs/images/posts/NatGeo11.jpg', 'image/jpeg', 'NatGeo11.jpg', '202357', '', 'NatGeo', '', '1', '1382912023', '1', '1383336905', '1024 1639'), ('6', '1', 'Aerial04.jpg', '1', '/Sites/idaho/httpdocs/images/posts/Aerial04.jpg', 'image/jpeg', 'Aerial04.jpg', '543139', '', 'Nat Geo', '', '1', '1382912056', '1', '1383336905', '1024 1639'), ('7', '1', 'generic-logo.png', '1', '/Sites/idaho/httpdocs/images/posts/', 'image/png', 'generic-logo.png', '7965', null, null, null, '1', '1382922748', '1', '1383336906', '80 200'), ('9', '1', 'fpo-custom-logo.jpg', '1', '/Sites/idaho/httpdocs/images/posts/fpo-custom-logo.jpg', 'image/jpeg', 'fpo-custom-logo.jpg', '7046', null, null, null, '1', '1383253286', '1', '1383336906', '100 500'), ('11', '1', 'fpo-custom-logo-topbar.jpg', '1', '/Sites/idaho/httpdocs/images/posts/fpo-custom-logo-topbar.jpg', 'image/jpeg', 'fpo-custom-logo-topbar.jpg', '4809', null, null, null, '1', '1383253890', '1', '1383336906', '42 200');

-- ----------------------------
--  Table structure for `exp_global_variables`
-- ----------------------------
DROP TABLE IF EXISTS `exp_global_variables`;
CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_global_variables`
-- ----------------------------
INSERT INTO `exp_global_variables` VALUES ('1', '1', 'snippet-analytics', '<!-- Google Analytics Code Here -->'), ('3', '1', 'var-df-year', '%Y'), ('4', '1', 'var-df-year-short', '%y'), ('5', '1', 'var-df-month-full', '%F'), ('6', '1', 'var-df-month-short', '%M'), ('7', '1', 'var-df-month-num', '%n'), ('8', '1', 'var-df-date', '%F %j, %Y'), ('9', '1', 'var-df-day', '%I'), ('10', '1', 'var-df-day-short', '%D'), ('11', '1', 'var-df-datepicker', '%Y-%m-%d %h:%m %A'), ('12', '1', 'var-disable-all', 'custom_fields|categories|category_fields|member_data|pagination|trackbacks'), ('13', '1', 'var-disable', 'categories|category_fields|member_data|pagination|trackbacks'), ('14', '1', 'var-disable-plus', 'member_data|pagination|trackbacks'), ('15', '1', 'var-sort-blog', 'orderby=\"date\"\nsort=\"desc\"'), ('17', '1', 'var-sort-alpha', 'orderby=\"title\"\nsort=\"asc\"'), ('18', '1', 'snippet-comments', '<!-- // START snippet-comments -->\n<h3><small><i class=\"fa fafw fa-comment-o\"></i> Comments ({comment_total})</small></h3>\n\n	{exp:comment:entries sort=\"asc\" limit=\"100\" entry_id=\"{entry_id}\"}\n\n	\n	<div class=\"comment\">\n		<div class=\"row\">\n			<div class=\"small-2 large-1 columns\">\n			<i class=\"fa fafw fa-quote-left fa-3x\"></i>\n				{!-- // UN-COMMENT if you want to use member avatars --}\n				{!--{if avatar_url}<p class=\"centered\"><img src=\"{avatar_url}\" width=\"50\"></p>{/if}	--}\n				\n			</div>\n			<div class=\"small-10 large-11 columns\">\n				{comment}	\n				<p class=\"item-meta\">By <a href=\"mailto:{email}\">{name}</a> on {comment_date format=\"{var-df-date}\"}</p>\n			</div>\n		</div>\n	</div>\n    {/exp:comment:entries}\n\n<!-- // END snippet-comments -->'), ('19', '1', 'snippet-comments-form', '<!-- // START snippet-comments-form -->\n\n{exp:comment:form channel=\"{pre_channel}\" }\n\n{if logged_out}\n  <div class=\"row\">\n    <div class=\"small-8\">\n      <div class=\"row\">\n        <div class=\"small-3 columns\">\n          <label for=\"name\" class=\"right inline\">Name:</label>\n        </div>\n        <div class=\"small-9 columns\">\n             <input type=\"text\" name=\"name\" value=\"{name}\" placeholder=\"John Smith\"/>\n        </div>\n      </div>\n    </div>\n  </div>\n   <div class=\"row\">\n    <div class=\"small-8\">\n      <div class=\"row\">\n        <div class=\"small-3 columns\">\n          <label for=\"email\" class=\"right inline\">Email:</label>\n        </div>\n        <div class=\"small-9 columns\">\n             <input type=\"text\" name=\"email\" value=\"{email}\" placeholder=\"john.smith@myemail.com\"/>\n        </div>\n      </div>\n    </div>\n  </div>\n   <div class=\"row\">\n    <div class=\"small-8\">\n      <div class=\"row\">\n        <div class=\"small-3 columns\">\n          <label for=\"url\" class=\"right inline\">URL:</label>\n        </div>\n        <div class=\"small-9 columns\">\n             <input type=\"text\" name=\"url\" value=\"{url}\" placeholder=\"www.mydomain.com\"/>\n        </div>\n      </div>\n    </div>\n  </div>\n  {/if}\n  <div class=\"row\">\n    <div class=\"small-8\">\n      <div class=\"row\">\n        <div class=\"small-3 columns\">\n          <label for=\"right-label\" class=\"right inline\"><label for=\"comment\">Comment:</label></label>\n        </div>\n        <div class=\"small-9 columns\">\n            <textarea name=\"comment\" cols=\"70\" rows=\"10\" placeholder=\"What are your thoughts?\">{comment}</textarea>\n        </div>\n      </div>\n    </div>\n  </div>\n    <div class=\"row\">\n    <div class=\"small-8\">\n      <div class=\"row\">\n        <div class=\"small-3 columns\">\n          <label for=\"right-label\" class=\"right inline\"></label>\n        </div>\n        <div class=\"small-9 columns\">\n            <label><input type=\"checkbox\" name=\"save_info\" value=\"yes\" {save_info} /> Remember my personal information</label><br />\n            <label><input type=\"checkbox\" name=\"notify_me\" value=\"yes\" {notify_me} /> Notify me of follow-up comments?</label><br />\n            {if captcha}\n                <label for=\"captcha\">Please enter the word you see in the image below:</label><br />\n                <p>{captcha}<br />\n                <input type=\"text\" name=\"captcha\" value=\"{captcha_word}\" maxlength=\"20\" /></p>\n            {/if}\n\n            <input type=\"submit\" name=\"submit\" value=\"Submit\" />\n            <input type=\"submit\" name=\"preview\" value=\"Preview\" />\n\n        {/exp:comment:form}\n                    \n            {exp:comment:notification_links}\n                {if subscribed}\n                <h5><a href=\"{unsubscribe_link}\">Unsubscribe to comment notification for this entry.</a></h5>\n                {if:else}\n                <h5><a href=\"{subscribe_link}\">Subscribe to comment notification for this entry.</a></h5>\n                {/if}\n            {/exp:comment:notification_links}\n        </div>\n      </div>\n    </div>\n  </div>\n</form>\n\n\n\n\n<!-- // END snippet-comments-form -->'), ('20', '1', 'snippet-search-form', ''), ('21', '1', 'snippet-head', '<!-- // START snippet-head -->\n<head>\n  <meta charset=\"utf-8\" />\n  <meta name=\"viewport\" content=\"width=device-width\" />\n  <title>{exp:stash:get name=\"block_metatitle\"}</title>\n  <meta name=\"description\" content=\"{exp:stash:get name=\"block_metadescription\"}\">\n  <!-- If you are using CSS version, only link these 2 files, you may add app.css to use for your overrides if you like. -->\n  <link rel=\"stylesheet\" href=\"/css/normalize.css\" />\n  <link rel=\"stylesheet\" href=\"/css/foundation.css\" />\n  <link rel=\"stylesheet\" href=\"/css/font-awesome/css/font-awesome.min.css\"/>\n  <link rel=\"stylesheet\" href=\"/css/fancybox/source/jquery.fancybox.css\"/>\n  <!-- If you are using the gem version, you need this only -->\n  <link rel=\"stylesheet\" href=\"/css/site.css\" />\n\n  <script src=\"/js/vendor/custom.modernizr.js\"></script>\n\n    {!-- Load up royal slider CSS if we are on the home page--}\n  {if segment_1 == \"\" OR segment_1 ==\"home\"}\n    <!-- basic stylesheet -->\n    <link rel=\"stylesheet\" href=\"/js/vendor/royalslider/royalslider.css\">\n\n    <!-- skin stylesheet (change it if you use another) -->\n    <link rel=\"stylesheet\" href=\"/js/vendor/royalslider/skins/universal/rs-universal.css\"> \n\n    <style>\n      .royalSlider {\n        width: 100%;\n        height: 200px;\n      }\n    </style>\n    {/if}\n</head>\n<!-- // END snippet-head -->'), ('22', '1', 'snippet-footer', '<!-- // START snippet-footer -->\n<hr>\n<footer class=\"row\">\n      {exp:low_variables:pair var=\"gv-footer-widgets\"}  \n        <div class=\"large-3 columns\">\n              {snippet-single-widget}\n        </div>\n      {/exp:low_variables:pair}\n      <div class=\"large-12 columns\">\n     	 {snippet-footer-nav}\n      </div>\n    <div class=\"large-12 columns\">      \n      <p>&copy; Copyright {current_time format=\"%Y\"}.</p>\n    </div> \n  </footer>    \n<!-- // END snippet-footer -->'), ('26', '1', 'snippet-local-nav-nested', '<!-- // START snippet-local-nav-nested -->\n<div class=\"widget panel\">\n<h4><small>In this section:</small></h4>\n<ul class=\"side-nav\" id=\"nav-sub\">\n{exp:taxonomy:nav \n    tree_id=\"1\" \n    depth=\"3\"\n    entry_id=\"{entry_id}\"\n    display_root=\"no\"\n    auto_expand=\"yes\"\n    active_branch_start_level=\"1\"\n    ul_css_id=\"nav-sub\"\n    ul_css_class=\"side-nav\"\n    status=\"public\"\n 	style=\"linear\"\n}\n<li class=\"{node_active}\"><a href=\"{node_url}\">{node_title}</a>{children}</li>\n\n{/exp:taxonomy:nav}\n</ul>\n</div>\n<!-- // END snippet-local-nav-nested -->'), ('50', '1', 'var-df-date-short', '%M %j'), ('29', '1', 'gv-sidebar-widgets', ''), ('30', '1', 'gv-footer-widgets', ''), ('31', '1', 'gv-blog-widgets', '[54] [widget-recent-portfolio-4] Widget - Recent Portfolio (4)\n[56] [blog-topics] Blog Topics\n[57] [blog-archives1] Blog Archives'), ('51', '1', 'gv-images-path', '/images/posts/'), ('74', '1', 'html-item-thumbnail', '		<div class=\"small-6 large-6 columns {if count==total_results}end{/if}\">\n				<a class=\"th\" href=\"/{embed:channel}/post/{url_title}\">{cf-post_featured-image:small wrap=\"image\"}</a>						\n				<p class=\"item-meta\"><a href=\"/{embed:channel}/post/{url_title}\">{title}</a><br>{entry_date format=\"{var-df-date-short}\"}</p>\n			</div>'), ('75', '1', 'html-item-text-short', '<p class=\"item-meta\">{entry_date format=\"{var-df-date-short}\"}<br>\n				<a href=\"/blog/post/{url_title}/\">			\n					{title}\n				</a>\n			</p>'), ('32', '1', 'snippet-sidebar', '<!-- // START snippet-sidebar -->\n{!-- this snippet generates sidebar widgets for dynamic pages, page-based widgets are generated on the page templates themselves --}\n<aside>\n{if segment_1 == \"blog\"}\n	{exp:low_variables:pair var=\"gv-blog-widgets\"}	\n		{snippet-single-widget}\n	{/exp:low_variables:pair}\n\n{if:elseif segment_1 == \"videos\"}\n	{exp:low_variables:pair var=\"gv-video-widgets\"}  \n		{snippet-single-widget}\n	{/exp:low_variables:pair}\n\n{if:elseif segment_1 == \"portfolio\"}\n	{exp:low_variables:pair var=\"gv-portfolio-widgets\"}  \n		{snippet-single-widget}\n	{/exp:low_variables:pair}\n\n{if:else}\n	{exp:low_variables:pair var=\"gv-resource-widgets\"}  \n		{snippet-single-widget}\n	{/exp:low_variables:pair}\n	\n{/if}\n\n</aside>\n<!-- // END snippet-sidebar -->'), ('39', '1', 'gv-video-metatitle', 'Videos'), ('40', '1', 'gv-videos-metadescription', 'Videos metadescription'), ('36', '1', 'gv-blog-metatitle', 'The Blog'), ('37', '1', 'gv-blog-metadescription', 'Blog metadescription'), ('38', '1', 'gv-video-widgets', '[46] [get-in-touch-now] Get in touch now!\n[53] [widget-recent-blog-posts] Widget - Recent Blog Posts (5)\n[58] [video-archives] Video Archives\n[60] [video-topics] Video Topics'), ('33', '1', 'snippet-single-widget', '<!-- // START snippet-single-widget -->\n\n<div class=\"widget {cf-widget_class}\">\n{if cf-widget_template}\n  {embed=\"{cf-widget_template}\" \n    channel=\"{cf-widget_channel}\"\n    filter=\"{cf-widget_filter}\" \n    limit=\"{cf-widget_limit}\" \n    heading=\"{cf-widget_title}\" \n    incl_images=\"{cf-widget_include-images}\" \n    widget_id=\"{entry_id}\"\n    image-size=\"{cf-widget_image-size}\"\n    cat_group=\"{cf-widget_cat-group}\"\n    }\n{if:else}\n	{if cf-widget_feature-image}\n		<img src=\"{cf-widget_feature-image:small}\">\n	{/if}\n	<h5>{cf-widget_title}</h5>\n	{cf-widget_body}\n{/if}\n</div>\n<!-- // END snippet-single-widget -->'), ('34', '1', 'gv-global-metatitle', 'PreeBuilt'), ('35', '1', 'gv-global-metadescription', 'Global meta description'), ('76', '1', 'snippet-filed-in', 'Filed in: {categories backspace=\"3\"}<a href=\"/blog/topics/{category_url_title}/\">{category_name}</a> | {/categories}'), ('77', '1', 'gv-blog-wrapper', 'sub-right'), ('78', '1', 'gv-video-wrapper', 'sub-right'), ('79', '1', 'gv-portfolio-wrapper', 'sub-right'), ('80', '1', 'gv-page-wrapper', 'sub-left'), ('81', '1', 'gv-contact-wrapper', 'sub-left'), ('82', '1', 'snippet-local-nav-accordion', '<div class=\"widget\">\n	<div class=\"section-container accordion\" data-section=\"accordion\" >\n		{exp:taxonomy:nav \n		    tree_id=\"1\" \n		    depth=\"3\"\n		    entry_id=\"{entry_id}\"\n		    display_root=\"no\"\n		    auto_expand=\"yes\"\n		    active_branch_start_level=\"1\"\n		    status=\"public\"\n		 	style=\"linear\"\n		}\n\n		<section class=\"section {node_active} {if node_active_parent} active{/if}\">\n			<p class=\"title\">\n				<a href=\"{node_url}\">{node_title}</a>\n			</p>			 \n\n			{if children}\n			<div class=\"content\">\n				{children}\n			</div>\n			{/if}\n		</section>\n		{/exp:taxonomy:nav}\n\n	</div><!-- .section-container -->\n</div>'), ('42', '1', 'gv-portfolio-widgets', '[46] [get-in-touch-now] Get in touch now!\n[50] [featured-videos1] Widget - Featured Videos (3)\n[55] [portfolio-topics1] Portfolio Topics'), ('43', '1', 'gv-portfolio-metatitle', 'Portfolio'), ('44', '1', 'gv-portfolio-metatdescription', 'Portfolio metadescription.'), ('59', '1', 'snippet-breadcrumbs', '{!-- renders global nav tree breadcrumbs for taxonomy-aware pages and templates --}\n<!-- // START snippet-breadcrumbs -->\n<ul class=\"breadcrumbs\">\n	{exp:taxonomy:breadcrumbs \n		status=\"open|public\" \n		tree_id=\"1\" \n		entry_id=\"{entry_id}\" 	\n		}\n		{if here}\n			<li class=\"current\">{node_title}</li>\n		{if:else}\n			<li><a href=\"{node_url}\">{node_title}</a> &rarr; </li>\n		{/if}\n	{/exp:taxonomy:breadcrumbs}	\n</ul>\n<!-- // END snippet-breadcrumbs -->'), ('56', '1', 'snippet-javascript', '<!-- // START snippet-javascript -->  \n  <script src=\'/js/vendor/jquery.js\'></script>\n  <script src=\"/js/foundation/foundation.js\"></script>\n  <script src=\"/js/foundation/foundation.alerts.js\"></script>\n  <script src=\"/js/foundation/foundation.clearing.js\"></script>\n  <script src=\"/js/foundation/foundation.cookie.js\"></script>\n  <script src=\"/js/foundation/foundation.dropdown.js\"></script>\n  <!--<script src=\"/js/foundation/foundation.forms.js\"></script>-->\n  <!--<script src=\"/js/foundation/foundation.joyride.js\"></script>-->\n  <!--<script src=\"/js/foundation/foundation.magellan.js\"></script>-->\n  <!--<script src=\"/js/foundation/foundation.orbit.js\"></script>-->\n  <script src=\"/js/foundation/foundation.placeholder.js\"></script>\n  <!--<script src=\"/js/foundation/foundation.reveal.js\"></script>-->\n  <script src=\"/js/foundation/foundation.section.js\"></script>\n  <!--<script src=\"/js/foundation/foundation.tooltips.js\"></script>-->\n  <script src=\"/js/foundation/foundation.topbar.js\"></script>\n  <!--<script src=\"/js/foundation/foundation.interchange.js\"></script>-->\n  <script src=\"/css/fancybox/source/jquery.fancybox.pack.js\"></script>\n  \n\n  <script>\n    $(document).foundation();\n  </script>\n   \n{!-- include royal slider only if on home page --}\n    \n    \n    <!-- Plugin requires jQuery 1.7+  -->\n    <!-- If you already have jQuery on your page, you shouldn\'t include it second time. -->\n    <!--<script src=\'royalslider/jquery-1.8.3.min.js\'></script>-->\n\n    <!-- Main slider JS script file --> \n\n    {!-- Include royalslider if we are on the home page --}\n    {if segment_1 ==\"\"}\n      <script src=\"/js/jquery.royalslider.custom.min.js\"></script>\n    {/if}\n\n    <script type=\"text/javascript\">\n       $(document).ready(function() {\n\n\n           $(\".modal\").fancybox();\n          \n    \n          {!-- Initiate royalslider if we are on the home page --}\n          {if segment_1 == \"\"}\n\n\n            $(\".royalSlider\").royalSlider({\n                // options go here\n                // as an example, enable keyboard arrows nav\n                keyboardNavEnabled: true,\n                loop: true,\n                slidesOrientation: \'horizontal\',\n                transitionType: \'move\',\n                transitionSpeed: 400,\n                moveEffect: \'none\',\n                sliderTouch: false,\n                addActiveClass: true,\n                rsOnUpdateNav: true,\n                usePreloader: true,\n                fadeEffect: true,\n                autoPlay: {\n                        // autoplay options go gere\n                        enabled: true,\n                        pauseOnHover: true,\n                        transitionType: \'fade\',\n              moveEffect: \'none\',\n              speed: 0,\n              delay: 5000\n                      },\n\n                \n                \n                block: {\n                        delay: 5000\n                      }\n\n            });  \n            {/if}\n\n           \n  \n\n      });\n     </script>\n \n\n  \n<!-- // END snippet-javascripts -->'), ('45', '1', 'gv-carousel', ' '), ('46', '1', 'gv-home-1', '[42] [home-carousel] Home Carousel'), ('47', '1', 'gv-home-2', '[43] [about-this-site] About this site'), ('48', '1', 'gv-home-3', '[50] [configurable-entry-list] Configurable Entry List'), ('49', '1', 'gv-home-4', '[20] [featured-videos] Featured Videos'), ('53', '1', 'snippet-footer-nav', '<!-- // START snippet-meta-nav -->\n<nav>\n    {exp:taxonomy:nav \n    	tree_id=\"3\"\n    	display_root=\"no\" \n    	\n    	ul_css_class=\"inline-list right\"\n\n    }\n        <li class=\"{node_active}\"><a href=\"{node_url}\">{node_title}</a>{children}</li>\n    {/exp:taxonomy:nav}\n</nav>\n<!-- // END snippet-meta-nav -->'), ('54', '1', 'gv-logo', ''), ('55', '1', 'snippet-post-featured-image', '{!-- This snippet checks the cf-post_featured-type to determine if and how to display the featured image on the post page --}\n	\n	<!-- /// BEGIN snippet-post-featured-image -->\n	{if cf-post_featured-type ==\"With content - large\"}\n		<a class=\"th modal\" data-fancybox-group=\"modal\" data-fancybox-title=\"{image-caption}\" rel=\"gallery_{entry_id}\" href=\"{cf-post_featured-image}\"><img src=\"{cf-post_featured-image:large}\"></a>\n		<p class=\"item-meta\">{cf-post_featured-image}{credit} {location}{/cf-post_featured-image}</p>\n	{/if}\n	{if cf-post_featured-type ==\"With content - medium\"}\n		<a class=\"th modal\" data-fancybox-group=\"modal\" data-fancybox-title=\"{image-caption}\" rel=\"gallery_{entry_id}\" href=\"{cf-post_featured-image}\"><img src=\"{cf-post_featured-image:medium}\"></a>\n		<p class=\"item-meta\">{cf-post_featured-image}{credit} {location}{/cf-post_featured-image}</p>\n	{/if}\n	{if cf-post_featured-type ==\"With content - small\"}\n		<a class=\"th modal\" data-fancybox-group=\"modal\" data-fancybox-title=\"{image-caption}\" rel=\"gallery_{entry_id}\" href=\"{cf-post_featured-image}\"><img src=\"{cf-post_featured-image:small}\"></a>\n		<p class=\"item-meta\">{cf-post_featured-image}{credit} {location}{/cf-post_featured-image}</p>\n	{/if}\n	{if cf-post_featured-type ==\"With content - thumbnail\"}\n		<a class=\"th modal\" data-fancybox-group=\"modal\" data-fancybox-title=\"{image-caption}\" rel=\"gallery_{entry_id}\" href=\"{cf-post_featured-image}\"><img src=\"{cf-post_featured-image:thumb}\"></a>\n	{/if}\n	<!-- /// END snippet-post-featured-image -->\n			'), ('61', '1', 'gv-videos-title', 'Videos'), ('62', '1', 'gv-blog-title', 'Blog'), ('73', '1', 'gv-contact-recipients', 'bryan@openmotive.com'), ('63', '1', 'gv-portfolio-title', 'Portfolio'), ('66', '1', 'snippet-header-topbar', '<!-- // START snippet-header-topbar -->\n<div class=\"contain-to-grid sticky\">\n<nav class=\"top-bar\" data-options=\"is_hover:false\">\n  <ul class=\"title-area\">\n    <!-- Title Area -->\n    <li class=\"name\">\n      \n        <h1>\n        {if gv-logo}\n        <a href=\"/\">\n         <img src=\"{gv-logo}\" alt=\"{site_name}\">\n         </a>\n        {if:else}\n          <a href=\"/\">{site_name}</a> \n        {/if}\n          \n        </h1>\n    </li>\n    \n    <li class=\"toggle-topbar menu-icon\"><a href=\"#\"><span>menu</span></a></li>\n  </ul>\n  <section class=\"top-bar-section\">\n     <!-- Right Nav Section -->\n     \n    {exp:taxonomy:nav \n      tree_id=\"1\"\n      depth=\"2\"\n      display_root=\"no\" \n      entry_status=\"open|public\"\n      ul_css_class:level_3=\"dropdown\"\n      ul_css_class:level_2=\"dropdown\"\n      ul_css_class:level_1=\"left\"\n      template_path=\"{segment_1}\"\n    }\n     \n      <li class=\"node_{node_id}{if node_has_children == \"yes\"} has-dropdown{/if}{if node_active_parent} active_parent{/if}\">\n      <a href=\"{node_url}\">{node_title}</a>\n      {children}\n      </li>\n        <li class=\"divider\"></li>\n    {/exp:taxonomy:nav}\n\n    {if logged_in}\n    <ul class=\"right\">\n    <li class=\"has-dropdown\">\n      <a href=\"#\"><i class=\"fa fa-fw fa-user\"></i> {screen_name}</a>\n      <ul class=\"dropdown\">\n        <li><a href=\"/admin.php\">Control panel</a></li>\n        <li><a href=\"#\">My Profile</a></li>\n        <li><a href=\"#\">Log-out</a></li>\n      </ul>\n    </li>\n    </ul>\n    {/if}\n    \n    </section>\n</nav>\n</div>\n<!-- // END snippet-header-topbar -->'), ('72', '1', 'gv-content-contact-form-response', '<h1>Thank you!</h1>\n<p>Thank you for your interest in us. We\'ll get back to you shortly.</p>'), ('71', '1', 'gv-words-in-summary', '70'), ('65', '1', 'snippet-paginate', ' {paginate}\n  {pagination_links}\n  <div class=\"pagination-centered\">\n  <ul class=\"pagination\">\n	  {first_page}\n	          <li><a href=\"{pagination_url}\" ><i class=\"fa fa-fw fa-step-backward\"></i></a></li>\n	  {/first_page}\n\n	  {previous_page}\n	          <li><a href=\"{pagination_url}\" ><i class=\"fa fa-fw fa-angle-left\"></i></a></li>\n	  {/previous_page}\n\n	  {page}\n	          <li class=\"{if current_page}current{/if}\"><a href=\"{pagination_url}\" >{pagination_page_number}</a></li>\n	  {/page}\n\n	  {next_page}\n	          <li><a href=\"{pagination_url}\"><i class=\"fa fa-fw fa-angle-right\"></i></a></li>\n	  {/next_page}\n\n	  {last_page}\n	          <li><a href=\"{pagination_url}\" ><i class=\"fa fa-fw fa-step-forward\"></i></a></li>\n	  {/last_page}\n	 </ul>\n	 </div>\n  {/pagination_links}\n  {/paginate}\n'), ('67', '1', 'snippet-header-default', '<!-- // START snippet-header-default -->\n\n\n\n\n<div class=\"row\">\n      <div class=\"large-12 columns\">\n    \n        <div class=\"nav-bar\">\n          {exp:taxonomy:nav \n          	tree_id=\"1\"\n          	depth=\"1\"\n          	display_root=\"no\" \n          	entry_status=\"open|public\"\n          	ul_css_class=\"button-group right\" 	        	\n          }\n              <li class=\"{node_active}\"><a class=\"button\" href=\"{node_url}\">{node_title}</a>{children}</li>\n          {/exp:taxonomy:nav}\n        </div>\n\n        \n        <h1>\n        {if gv-logo}\n        <a href=\"/\">\n         <img src=\"{gv-logo}\" alt=\"{site_name}\">\n         </a>\n        {if:else}\n          <a href=\"/\">{site_name}</a> \n        {/if}\n          <small>{gv-site-subhead}</small>\n        </h1>\n                         \n        <hr>\n      </div>\n    </div>\n\n    <!-- // END snippet-header-default -->'), ('69', '1', 'gv-site-subhead', 'a really cool place to start.'), ('70', '1', 'snippet-header-text-only', '<!-- // START snippet-header-default -->\n\n\n\n\n<div class=\"row\">\n      <div class=\"large-12 columns\">\n    \n        <div class=\"nav-bar\">\n          {exp:taxonomy:nav \n            tree_id=\"1\"\n            depth=\"1\"\n            display_root=\"no\" \n            entry_status=\"open|public\"\n            ul_css_class=\"inline-list right\"            \n          }\n              <li class=\"{node_active}\"><a href=\"{node_url}\">{node_title}</a>{children}</li>\n          {/exp:taxonomy:nav}\n        </div>\n\n        <h1>\n        {if gv-logo}\n        <a href=\"/\">\n         <img src=\"{gv-logo}\" alt=\"{site_name}\">\n         </a>\n        {if:else}\n          <a href=\"/\">{site_name}</a> \n        {/if}\n          <small>{gv-site-subhead}</small></h1>\n               \n        <hr>\n      </div>\n    </div>\n\n    <!-- // END snippet-header-default -->');

-- ----------------------------
--  Table structure for `exp_grid_columns`
-- ----------------------------
DROP TABLE IF EXISTS `exp_grid_columns`;
CREATE TABLE `exp_grid_columns` (
  `col_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned DEFAULT NULL,
  `content_type` varchar(50) DEFAULT NULL,
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_type` varchar(50) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_instructions` text,
  `col_required` char(1) DEFAULT NULL,
  `col_search` char(1) DEFAULT NULL,
  `col_width` int(3) unsigned DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `field_id` (`field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_grid_columns`
-- ----------------------------
INSERT INTO `exp_grid_columns` VALUES ('1', '45', 'low_variables', '0', 'file', 'Carousel Image', 'carousel_image', '', 'y', 'n', '0', '{\"field_content_type\":\"image\",\"allowed_directories\":\"1\",\"show_existing\":\"y\",\"num_existing\":\"50\",\"field_required\":\"y\"}'), ('2', '45', 'low_variables', '1', 'rte', 'Copy', 'copy', '', 'n', 'n', '0', '{\"field_ta_rows\":\"10\",\"field_text_direction\":\"ltr\",\"field_required\":\"n\"}'), ('3', '45', 'low_variables', '2', 'text', 'Link Label', 'link_label', '', 'n', 'n', '0', '{\"field_fmt\":\"none\",\"field_content_type\":\"all\",\"field_text_direction\":\"ltr\",\"field_maxl\":\"50\",\"field_required\":\"n\"}'), ('4', '45', 'low_variables', '3', 'text', 'Link URL', 'link_url', '', 'n', 'n', '0', '{\"field_fmt\":\"none\",\"field_content_type\":\"all\",\"field_text_direction\":\"ltr\",\"field_maxl\":\"256\",\"field_required\":\"n\"}');

-- ----------------------------
--  Table structure for `exp_html_buttons`
-- ----------------------------
DROP TABLE IF EXISTS `exp_html_buttons`;
CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_html_buttons`
-- ----------------------------
INSERT INTO `exp_html_buttons` VALUES ('1', '1', '0', 'b', '<strong>', '</strong>', 'b', '1', '1', 'btn_b'), ('2', '1', '0', 'i', '<em>', '</em>', 'i', '2', '1', 'btn_i'), ('3', '1', '0', 'blockquote', '<blockquote>', '</blockquote>', 'q', '3', '1', 'btn_blockquote'), ('4', '1', '0', 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', '4', '1', 'btn_a'), ('5', '1', '0', 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', '5', '1', 'btn_img');

-- ----------------------------
--  Table structure for `exp_layout_publish`
-- ----------------------------
DROP TABLE IF EXISTS `exp_layout_publish`;
CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_layout_publish`
-- ----------------------------
INSERT INTO `exp_layout_publish` VALUES ('6', '1', '1', '4', 'a:7:{s:7:\"publish\";a:5:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:7:\"dynamic\";a:10:{s:10:\"_tab_label\";s:7:\"Dynamic\";i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:25;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:28;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:16;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:3:\"50%\";}i:15;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:3:\"50%\";}i:18;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:3:\"50%\";}i:27;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:3:\"50%\";}i:22;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:21;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:3:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"Revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:5:\"pages\";a:3:{s:10:\"_tab_label\";s:5:\"Pages\";s:24:\"pages__pages_template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:16:\"pages__pages_uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'), ('7', '1', '1', '7', 'a:5:{s:7:\"publish\";a:15:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:9;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:23;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:11;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:20;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:29;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:30;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:24;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:31;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:5:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:5:\"pages\";a:3:{s:10:\"_tab_label\";s:5:\"Pages\";s:16:\"pages__pages_uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:24:\"pages__pages_template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');

-- ----------------------------
--  Table structure for `exp_low_reorder_orders`
-- ----------------------------
DROP TABLE IF EXISTS `exp_low_reorder_orders`;
CREATE TABLE `exp_low_reorder_orders` (
  `set_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  `sort_order` text,
  PRIMARY KEY (`set_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_low_reorder_sets`
-- ----------------------------
DROP TABLE IF EXISTS `exp_low_reorder_sets`;
CREATE TABLE `exp_low_reorder_sets` (
  `set_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `set_label` varchar(100) NOT NULL,
  `set_name` varchar(50) NOT NULL,
  `set_notes` text NOT NULL,
  `new_entries` enum('append','prepend') NOT NULL DEFAULT 'append',
  `clear_cache` enum('y','n') NOT NULL DEFAULT 'y',
  `channels` varchar(255) NOT NULL,
  `cat_option` enum('all','some','one','none') NOT NULL DEFAULT 'all',
  `cat_groups` varchar(255) NOT NULL,
  `parameters` text NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`set_id`),
  KEY `site_id` (`site_id`),
  KEY `set_name` (`set_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_low_variable_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_low_variable_groups`;
CREATE TABLE `exp_low_variable_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(6) unsigned NOT NULL DEFAULT '1',
  `group_label` varchar(100) NOT NULL DEFAULT '',
  `group_notes` text NOT NULL,
  `group_order` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_low_variable_groups`
-- ----------------------------
INSERT INTO `exp_low_variable_groups` VALUES ('1', '1', 'Miscellaneous Content', 'Content that is not directly related to, or tied to, an entry, category, or channel.', '6'), ('3', '1', 'Shortcuts', 'These variables are used as shortcuts for hard-to-remember-and-keep-consistent channel:entries tag parameters. They provide easy markup of common ordering, sorting, disabling, and filtering of entries.', '11'), ('4', '1', 'Home Page', '', '2'), ('5', '1', 'Code Snippets', 'Snippets are re-usable, straightforward blocks of ExpressionEngine code and HTML that help keep our code DRY.', '7'), ('6', '1', 'HTML Blocks', 'Re-useable HTML constructs to display post data in different ways.', '9'), ('7', '1', 'Channel Widgets', '', '3'), ('8', '1', 'SEO', '', '4'), ('10', '1', 'Site and Channel Settings', '', '1'), ('11', '1', 'Services', '', '5'), ('12', '1', 'Headers & Navigation', '', '8'), ('13', '1', 'Ajax Snippets', '', '10'), ('14', '2', 'Common Forms', '', '0'), ('15', '2', 'Snippets', '', '0'), ('16', '2', 'Shortcuts', '', '0');

-- ----------------------------
--  Table structure for `exp_low_variable_groups-bak`
-- ----------------------------
DROP TABLE IF EXISTS `exp_low_variable_groups-bak`;
CREATE TABLE `exp_low_variable_groups-bak` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(6) unsigned NOT NULL DEFAULT '1',
  `group_label` varchar(100) NOT NULL DEFAULT '',
  `group_notes` text NOT NULL,
  `group_order` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_low_variable_groups-bak`
-- ----------------------------
INSERT INTO `exp_low_variable_groups-bak` VALUES ('1', '1', 'Site and Channel Settings', '', '0'), ('2', '1', 'Home Page', '', '0'), ('3', '1', 'Channel Widgets', '', '0'), ('4', '1', 'SEO', '', '0'), ('5', '1', 'Services', '', '0'), ('6', '1', 'Miscellaneous Content', '', '0'), ('7', '1', 'Code Snippets', '', '0'), ('8', '1', 'Headers & Navigation', '', '0'), ('9', '1', 'HTML Blocks', '', '0'), ('10', '1', 'Shortcuts', '', '0');

-- ----------------------------
--  Table structure for `exp_low_variables`
-- ----------------------------
DROP TABLE IF EXISTS `exp_low_variables`;
CREATE TABLE `exp_low_variables` (
  `variable_id` int(6) unsigned NOT NULL,
  `group_id` int(6) unsigned NOT NULL DEFAULT '0',
  `variable_label` varchar(100) NOT NULL DEFAULT '',
  `variable_notes` text NOT NULL,
  `variable_type` varchar(50) NOT NULL DEFAULT 'low_textarea',
  `variable_settings` text NOT NULL,
  `variable_order` int(4) unsigned NOT NULL DEFAULT '0',
  `early_parsing` char(1) NOT NULL DEFAULT 'n',
  `is_hidden` char(1) NOT NULL DEFAULT 'n',
  `save_as_file` char(1) NOT NULL DEFAULT 'n',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variable_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_low_variables`
-- ----------------------------
INSERT INTO `exp_low_variables` VALUES ('1', '11', 'Google Analytics Code', 'This code is inserted just before the closing of each document.', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '1', 'y', 'n', 'n', '1382999000'), ('3', '3', 'Year (1999)', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '8', 'y', 'y', 'y', '1387497293'), ('4', '3', 'Year (99)', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '9', 'y', 'y', 'y', '1387497293'), ('5', '3', 'Month - full (March)', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '14', 'n', 'y', 'y', '1387497293'), ('6', '3', 'Month - short (Mar)', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '13', 'y', 'y', 'y', '1387497293'), ('7', '3', 'Month - num (03)', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '12', 'y', 'y', 'y', '1387497293'), ('8', '3', 'Default Full Date', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '6', 'y', 'y', 'y', '1387497293'), ('9', '3', 'Day of week (Monday)', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '10', 'y', 'y', 'y', '1387497293'), ('10', '3', 'Day of week - short (Mon)', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '11', 'y', 'y', 'y', '1387497293'), ('11', '3', 'Datepicker compatible', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '15', 'y', 'y', 'y', '1387497293'), ('12', '3', 'Disable all', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo1OiJsYXJnZSI7czo3OiJwYXR0ZXJuIjtzOjA6IiI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjt9', '3', 'n', 'y', 'y', '1387497293'), ('13', '3', 'Standard disables', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo1OiJsYXJnZSI7czo3OiJwYXR0ZXJuIjtzOjA6IiI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjt9', '1', 'n', 'y', 'y', '1387497293'), ('14', '3', 'Allow Category info (plus)', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo1OiJsYXJnZSI7czo3OiJwYXR0ZXJuIjtzOjA6IiI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjt9', '2', 'n', 'y', 'y', '1387497293'), ('15', '3', 'Standard Blog sorting', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '5', 'n', 'y', 'y', '1387497293'), ('17', '3', 'Alphabetical', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '4', 'n', 'y', 'y', '1387497293'), ('18', '5', 'Comments', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '4', 'y', 'y', 'y', '1387495031'), ('19', '5', 'Comment form', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '5', 'y', 'y', 'y', '1387495080'), ('20', '5', 'Search form', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '3', 'y', 'y', 'y', '1387495006'), ('21', '5', 'HTML Head', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '11', 'y', 'y', 'y', '1387495211'), ('22', '5', 'Footer', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '1', 'y', 'y', 'y', '1387403216'), ('26', '12', 'Local Nav - nested', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '4', 'y', 'y', 'y', '1387495704'), ('29', '7', 'Page Widgets', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6InkiO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJuIjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6MTp7aTowO3M6NDoib3BlbiI7fXM6Nzoib3JkZXJieSI7czo1OiJ0aXRsZSI7czo0OiJzb3J0IjtzOjM6IkFTQyI7fQ', '5', 'y', 'n', 'n', '1383863345'), ('30', '7', 'Footer Widgets', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6InkiO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJuIjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6MTp7aTowO3M6NDoib3BlbiI7fXM6Nzoib3JkZXJieSI7czo1OiJ0aXRsZSI7czo0OiJzb3J0IjtzOjM6IkFTQyI7fQ', '4', 'y', 'n', 'n', '1383863345'), ('31', '7', 'News Widgets', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6InkiO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJuIjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6MTp7aTowO3M6NDoib3BlbiI7fXM6Nzoib3JkZXJieSI7czo1OiJ0aXRsZSI7czo0OiJzb3J0IjtzOjM6IkFTQyI7fQ', '1', 'y', 'n', 'n', '1383863345'), ('32', '5', 'Sidebar', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '8', 'y', 'y', 'y', '1387495114'), ('33', '5', 'Build 1 Widget', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '9', 'y', 'y', 'y', '1387495148'), ('34', '8', 'Global Metatitle', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '1', 'n', 'n', 'n', '1385146859'), ('35', '8', 'Global Metadescription', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', '2', 'n', 'n', 'n', '1385146859'), ('36', '8', 'Blog Metatitle', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '3', 'y', 'n', 'n', '1385146859'), ('37', '8', 'Blog Metadescription', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', '4', 'y', 'n', 'n', '1385146859'), ('38', '7', 'Video Widgets', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6InkiO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJuIjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6MTp7aTowO3M6NDoib3BlbiI7fXM6Nzoib3JkZXJieSI7czo1OiJ0aXRsZSI7czo0OiJzb3J0IjtzOjM6IkFTQyI7fQ', '3', 'y', 'n', 'n', '1383863345'), ('39', '8', 'Clients and Markets Metatitle', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo1OiJsYXJnZSI7czo3OiJwYXR0ZXJuIjtzOjA6IiI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjt9', '5', 'n', 'n', 'n', '1385146859'), ('40', '8', 'Clients and Markets Metadescription', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', '6', 'n', 'n', 'n', '1385146859'), ('42', '7', 'Portfolio Widgets', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6InkiO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJuIjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6MTp7aTowO3M6NDoib3BlbiI7fXM6Nzoib3JkZXJieSI7czo1OiJ0aXRsZSI7czo0OiJzb3J0IjtzOjM6IkFTQyI7fQ', '2', 'y', 'n', 'n', '1383863345'), ('43', '8', 'Portfolio Metatitle', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '7', 'n', 'n', 'n', '1385146859'), ('44', '8', 'Portfolio Metadescription', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czowOiIiO30', '8', 'n', 'n', 'n', '1385146859'), ('45', '4', 'Carousel', '', 'low_grid', 'YToyOntzOjEzOiJncmlkX21pbl9yb3dzIjtpOjA7czoxMzoiZ3JpZF9tYXhfcm93cyI7czowOiIiO30', '1', 'n', 'n', 'n', '1384898388'), ('46', '4', 'Home area 1', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6Im4iO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJ5IjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6Mjp7aTowO3M6NDoib3BlbiI7aToxO3M6NjoiUHVibGljIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjt9', '2', 'y', 'n', 'n', '1384898388'), ('47', '4', 'Home area 2', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6Im4iO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJ5IjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6Mjp7aTowO3M6NDoib3BlbiI7aToxO3M6NjoiUHVibGljIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjt9', '3', 'y', 'n', 'n', '1384898388'), ('48', '4', 'Home area 3', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6Im4iO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJ5IjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6Mjp7aTowO3M6NDoib3BlbiI7aToxO3M6NjoiUHVibGljIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjt9', '4', 'y', 'n', 'n', '1384898388'), ('49', '4', 'Home area 4', '', 'playa', 'YTo4OntzOjU6Im11bHRpIjtzOjE6Im4iO3M6NzoiZXhwaXJlZCI7czoxOiJuIjtzOjY6ImZ1dHVyZSI7czoxOiJ5IjtzOjg6ImVkaXRhYmxlIjtzOjE6Im4iO3M6ODoiY2hhbm5lbHMiO2E6MTp7aTowO3M6MToiNCI7fXM6ODoic3RhdHVzZXMiO2E6Mjp7aTowO3M6NDoib3BlbiI7aToxO3M6NjoiUHVibGljIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjt9', '5', 'y', 'n', 'n', '1384898388'), ('50', '3', 'Default Short Date', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '7', 'y', 'y', 'y', '1387497293'), ('51', '10', 'Upload folder root path', 'Necessary for rendering category thumbnail images.', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '7', 'y', 'y', 'n', '1385149552'), ('53', '12', 'Footer Nav', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '5', 'y', 'y', 'y', '1387495704'), ('54', '10', 'Custom Site Logo', 'Note: If using a \"topbar\" global nav, the logo should be no more than 42 pixels in height.', 'low_select_files', 'YTo1OntzOjc6ImZvbGRlcnMiO2E6MTp7aTowO3M6MToiMSI7fXM6NjoidXBsb2FkIjtzOjE6IjEiO3M6OToic2VwYXJhdG9yIjtzOjc6Im5ld2xpbmUiO3M6MTU6Im11bHRpX2ludGVyZmFjZSI7czo2OiJzZWxlY3QiO3M6ODoibXVsdGlwbGUiO3M6MDoiIjt9', '1', 'y', 'n', 'n', '1385149552'), ('55', '5', 'Display post featured image', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '10', 'y', 'y', 'y', '1387495188'), ('56', '5', 'Javascript', 'Site-wide javascripts that are inserted before the closing body tag.', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '2', 'y', 'y', 'y', '1387494964'), ('59', '12', 'Breadcrumbs', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '6', 'y', 'y', 'y', '1387495704'), ('61', '10', 'Public title for VIDEOS channel', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '3', 'n', 'n', 'n', '1385149552'), ('62', '10', 'Public title for BLOG channel', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '4', 'n', 'n', 'n', '1385149552'), ('63', '10', 'Public title for PORTFOLIO channel', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '5', 'n', 'n', 'n', '1385149552'), ('65', '12', 'Paginate', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7czoxMToiY29kZV9mb3JtYXQiO3M6MToieSI7fQ', '7', 'y', 'y', 'y', '1387495704'), ('66', '12', 'Topbar Header', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '8', 'y', 'y', 'y', '1387495704'), ('67', '12', 'Default Header', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '9', 'y', 'y', 'y', '1387495704'), ('69', '10', 'Site Tagline', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '2', 'n', 'n', 'n', '1385149552'), ('70', '12', 'Header with Text only global nav', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '10', 'y', 'y', 'y', '1387495704'), ('71', '10', '# of words to use in summary excerpts', '', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '6', 'y', 'n', 'n', '1385149552'), ('72', '1', 'Contact form response', '', 'low_rte', 'YToyOntzOjQ6InJvd3MiO3M6MjoiMTAiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '1', 'n', 'n', 'n', '0'), ('73', '10', 'Contact form recipients', 'Separate multiple e-mail addresses with a \",\". (eg. john@somesite.com, mary@anothersite.com)', 'low_text_input', 'YTo0OntzOjk6Im1heGxlbmd0aCI7czowOiIiO3M6NDoic2l6ZSI7czo2OiJtZWRpdW0iO3M6NzoicGF0dGVybiI7czowOiIiO3M6MTQ6InRleHRfZGlyZWN0aW9uIjtzOjM6Imx0ciI7fQ', '8', 'y', 'n', 'n', '1385149552'), ('74', '6', 'Item Thumbnail', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '1', 'y', 'y', 'y', '1387495796'), ('75', '6', 'Item as short text', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '2', 'y', 'y', 'y', '1387495849'), ('76', '12', '\"Filed in:\" snippet', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '8', 'y', 'y', 'y', '1387495704'), ('77', '10', 'HTML Wrapper for Blog', '', 'low_select', 'YTo0OntzOjc6Im9wdGlvbnMiO3M6MjQ6ImluZGV4CnN1Yi1sZWZ0CnN1Yi1yaWdodCI7czo5OiJzZXBhcmF0b3IiO3M6NzoibmV3bGluZSI7czoxNToibXVsdGlfaW50ZXJmYWNlIjtzOjY6InNlbGVjdCI7czo4OiJtdWx0aXBsZSI7czowOiIiO30', '9', 'n', 'n', 'n', '1385149552'), ('78', '10', 'HTML Wrapper for Videos', '', 'low_select', 'YTo0OntzOjc6Im9wdGlvbnMiO3M6MjQ6ImluZGV4CnN1Yi1sZWZ0CnN1Yi1yaWdodCI7czo5OiJzZXBhcmF0b3IiO3M6NzoibmV3bGluZSI7czoxNToibXVsdGlfaW50ZXJmYWNlIjtzOjY6InNlbGVjdCI7czo4OiJtdWx0aXBsZSI7czowOiIiO30', '10', 'n', 'n', 'n', '1385149552'), ('79', '10', 'HTML Wrapper for Portfolio', '', 'low_select', 'YTo0OntzOjc6Im9wdGlvbnMiO3M6MjQ6ImluZGV4CnN1Yi1sZWZ0CnN1Yi1yaWdodCI7czo5OiJzZXBhcmF0b3IiO3M6NzoibmV3bGluZSI7czoxNToibXVsdGlfaW50ZXJmYWNlIjtzOjY6InNlbGVjdCI7czo4OiJtdWx0aXBsZSI7czowOiIiO30', '11', 'n', 'n', 'n', '1385149552'), ('80', '10', 'HTML Wrapper for Pages', '', 'low_select', 'YTo0OntzOjc6Im9wdGlvbnMiO3M6MjQ6ImluZGV4CnN1Yi1sZWZ0CnN1Yi1yaWdodCI7czo5OiJzZXBhcmF0b3IiO3M6NzoibmV3bGluZSI7czoxNToibXVsdGlfaW50ZXJmYWNlIjtzOjY6InNlbGVjdCI7czo4OiJtdWx0aXBsZSI7czowOiIiO30', '12', 'n', 'n', 'n', '1385149552'), ('81', '10', 'HTML Wrapper for Contact page', '', 'low_select', 'YTo0OntzOjc6Im9wdGlvbnMiO3M6MjQ6ImluZGV4CnN1Yi1sZWZ0CnN1Yi1yaWdodCI7czo5OiJzZXBhcmF0b3IiO3M6NzoibmV3bGluZSI7czoxNToibXVsdGlfaW50ZXJmYWNlIjtzOjY6InNlbGVjdCI7czo4OiJtdWx0aXBsZSI7czowOiIiO30', '13', 'n', 'n', 'n', '1385149552'), ('82', '12', 'Local Nav - Accordion', '', 'low_textarea', 'YTozOntzOjQ6InJvd3MiO3M6MToiMyI7czoxNDoidGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJjb2RlX2Zvcm1hdCI7czoxOiJ5Ijt9', '9', 'y', 'y', 'y', '1387495704');

-- ----------------------------
--  Table structure for `exp_low_variables-bak`
-- ----------------------------
DROP TABLE IF EXISTS `exp_low_variables-bak`;
CREATE TABLE `exp_low_variables-bak` (
  `variable_id` int(6) unsigned NOT NULL,
  `group_id` int(6) unsigned NOT NULL DEFAULT '0',
  `variable_label` varchar(100) NOT NULL DEFAULT '',
  `variable_notes` text NOT NULL,
  `variable_type` varchar(50) NOT NULL DEFAULT 'low_textarea',
  `variable_settings` text NOT NULL,
  `variable_order` int(4) unsigned NOT NULL DEFAULT '0',
  `early_parsing` char(1) NOT NULL DEFAULT 'n',
  `is_hidden` char(1) NOT NULL DEFAULT 'n',
  `save_as_file` char(1) NOT NULL DEFAULT 'n',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variable_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_low_variables-bak`
-- ----------------------------
INSERT INTO `exp_low_variables-bak` VALUES ('1', '0', '', '', 'low_textarea', '', '0', 'n', 'n', 'n', '1387401354'), ('3', '10', '', '', 'low_textarea', '', '1', 'n', 'n', 'n', '1387401354'), ('4', '10', '', '', 'low_textarea', '', '2', 'n', 'n', 'n', '1387401354'), ('5', '10', '', '', 'low_textarea', '', '3', 'n', 'n', 'n', '1387401354'), ('6', '0', '', '', 'low_textarea', '', '4', 'n', 'n', 'n', '1387401354'), ('7', '10', '', '', 'low_textarea', '', '5', 'n', 'n', 'n', '1387401354'), ('8', '10', '', '', 'low_textarea', '', '6', 'n', 'n', 'n', '1387401354'), ('9', '10', '', '', 'low_textarea', '', '7', 'n', 'n', 'n', '1387401354'), ('10', '10', '', '', 'low_textarea', '', '8', 'n', 'n', 'n', '1387401354'), ('11', '10', '', '', 'low_textarea', '', '9', 'n', 'n', 'n', '1387401354'), ('12', '10', '', '', 'low_textarea', '', '10', 'n', 'n', 'n', '1387401354'), ('13', '10', '', '', 'low_textarea', '', '11', 'n', 'n', 'n', '1387401354'), ('14', '10', '', '', 'low_textarea', '', '12', 'n', 'n', 'n', '1387401354'), ('15', '10', '', '', 'low_textarea', '', '13', 'n', 'n', 'n', '1387401354'), ('17', '10', '', '', 'low_textarea', '', '14', 'n', 'n', 'n', '1387401354'), ('18', '0', '', '', 'low_textarea', '', '15', 'n', 'n', 'n', '1387401354'), ('19', '0', '', '', 'low_textarea', '', '16', 'n', 'n', 'n', '1387401354'), ('20', '0', '', '', 'low_textarea', '', '17', 'n', 'n', 'n', '1387401354'), ('21', '0', '', '', 'low_textarea', '', '18', 'n', 'n', 'n', '1387401354'), ('22', '0', '', '', 'low_textarea', '', '19', 'n', 'n', 'n', '1387401354'), ('26', '0', '', '', 'low_textarea', '', '20', 'n', 'n', 'n', '1387401354'), ('29', '0', '', '', 'low_textarea', '', '21', 'n', 'n', 'n', '1387401354'), ('30', '0', '', '', 'low_textarea', '', '22', 'n', 'n', 'n', '1387401354'), ('31', '0', '', '', 'low_textarea', '', '23', 'n', 'n', 'n', '1387401354'), ('32', '0', '', '', 'low_textarea', '', '24', 'n', 'n', 'n', '1387401354'), ('33', '0', '', '', 'low_textarea', '', '25', 'n', 'n', 'n', '1387401354'), ('34', '0', '', '', 'low_textarea', '', '26', 'n', 'n', 'n', '1387401354'), ('35', '0', '', '', 'low_textarea', '', '27', 'n', 'n', 'n', '1387401354'), ('36', '0', '', '', 'low_textarea', '', '28', 'n', 'n', 'n', '1387401354'), ('37', '0', '', '', 'low_textarea', '', '29', 'n', 'n', 'n', '1387401354'), ('38', '0', '', '', 'low_textarea', '', '30', 'n', 'n', 'n', '1387401354'), ('39', '0', '', '', 'low_textarea', '', '31', 'n', 'n', 'n', '1387401354'), ('40', '0', '', '', 'low_textarea', '', '32', 'n', 'n', 'n', '1387401354'), ('42', '0', '', '', 'low_textarea', '', '33', 'n', 'n', 'n', '1387401354'), ('43', '0', '', '', 'low_textarea', '', '34', 'n', 'n', 'n', '1387401354'), ('44', '0', '', '', 'low_textarea', '', '35', 'n', 'n', 'n', '1387401354'), ('45', '0', '', '', 'low_textarea', '', '36', 'n', 'n', 'n', '1387401354'), ('46', '0', '', '', 'low_textarea', '', '37', 'n', 'n', 'n', '1387401354'), ('47', '0', '', '', 'low_textarea', '', '38', 'n', 'n', 'n', '1387401354'), ('48', '0', '', '', 'low_textarea', '', '39', 'n', 'n', 'n', '1387401354'), ('49', '0', '', '', 'low_textarea', '', '40', 'n', 'n', 'n', '1387401354'), ('50', '0', '', '', 'low_textarea', '', '41', 'n', 'n', 'n', '1387401354'), ('51', '0', '', '', 'low_textarea', '', '42', 'n', 'n', 'n', '1387401354'), ('53', '0', '', '', 'low_textarea', '', '43', 'n', 'n', 'n', '1387401354'), ('54', '0', '', '', 'low_textarea', '', '44', 'n', 'n', 'n', '1387401354'), ('55', '0', '', '', 'low_textarea', '', '45', 'n', 'n', 'n', '1387401354'), ('56', '0', '', '', 'low_textarea', '', '46', 'n', 'n', 'n', '1387401354'), ('59', '0', '', '', 'low_textarea', '', '47', 'n', 'n', 'n', '1387401354'), ('61', '0', '', '', 'low_textarea', '', '48', 'n', 'n', 'n', '1387401354'), ('62', '0', '', '', 'low_textarea', '', '49', 'n', 'n', 'n', '1387401354'), ('63', '0', '', '', 'low_textarea', '', '50', 'n', 'n', 'n', '1387401354'), ('65', '0', '', '', 'low_textarea', '', '51', 'n', 'n', 'n', '1387401354'), ('66', '0', '', '', 'low_textarea', '', '52', 'n', 'n', 'n', '1387401354'), ('67', '0', '', '', 'low_textarea', '', '53', 'n', 'n', 'n', '1387401354'), ('69', '0', '', '', 'low_textarea', '', '54', 'n', 'n', 'n', '1387401354'), ('70', '0', '', '', 'low_textarea', '', '55', 'n', 'n', 'n', '1387401354'), ('71', '0', '', '', 'low_textarea', '', '56', 'n', 'n', 'n', '1387401354'), ('72', '0', '', '', 'low_textarea', '', '57', 'n', 'n', 'n', '1387401354'), ('73', '0', '', '', 'low_textarea', '', '58', 'n', 'n', 'n', '1387401354'), ('74', '0', '', '', 'low_textarea', '', '59', 'n', 'n', 'n', '1387401354'), ('75', '0', '', '', 'low_textarea', '', '60', 'n', 'n', 'n', '1387401354'), ('76', '0', '', '', 'low_textarea', '', '61', 'n', 'n', 'n', '1387401354'), ('77', '0', '', '', 'low_textarea', '', '62', 'n', 'n', 'n', '1387401354'), ('78', '0', '', '', 'low_textarea', '', '63', 'n', 'n', 'n', '1387401354'), ('79', '0', '', '', 'low_textarea', '', '64', 'n', 'n', 'n', '1387401354'), ('80', '0', '', '', 'low_textarea', '', '65', 'n', 'n', 'n', '1387401354'), ('81', '0', '', '', 'low_textarea', '', '66', 'n', 'n', 'n', '1387401354'), ('82', '0', '', '', 'low_textarea', '', '67', 'n', 'n', 'n', '1387401354');

-- ----------------------------
--  Table structure for `exp_low_variables_grid_field_45`
-- ----------------------------
DROP TABLE IF EXISTS `exp_low_variables_grid_field_45`;
CREATE TABLE `exp_low_variables_grid_field_45` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `row_order` int(10) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_3` text,
  `col_id_4` text,
  PRIMARY KEY (`row_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_low_variables_grid_field_45`
-- ----------------------------
INSERT INTO `exp_low_variables_grid_field_45` VALUES ('1', '45', '0', '{filedir_1}NatGeo01.jpg', '​This is the copy for this slide.', 'GO here!', '/blog'), ('2', '45', '1', '{filedir_1}NatGeo04.jpg', '​This is the copy for slide 2', 'Go here as well!', '/videos');

-- ----------------------------
--  Table structure for `exp_matrix_cols`
-- ----------------------------
DROP TABLE IF EXISTS `exp_matrix_cols`;
CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_matrix_cols`
-- ----------------------------
INSERT INTO `exp_matrix_cols` VALUES ('2', '1', '20', null, 'image', 'Image', '', 'file', 'y', 'n', '0', '30%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30='), ('3', '1', '20', null, 'image-title', 'Title', '', 'text', 'n', 'n', '1', '', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30='), ('4', '1', '20', null, 'image-caption', 'Caption', '', 'text', 'n', 'n', '2', '', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMjAwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');

-- ----------------------------
--  Table structure for `exp_matrix_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_matrix_data`;
CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_2` text,
  `col_id_3` text,
  `col_id_4` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_matrix_data`
-- ----------------------------
INSERT INTO `exp_matrix_data` VALUES ('2', '1', '24', '20', null, '0', '1', '{filedir_1}NatGeo01.jpg', 'This is the title', 'This is the caption.'), ('3', '1', '24', '20', null, '0', '2', '{filedir_1}NatGeo04.jpg', 'This is another title', 'This is another caption but it is going to be a bit longer.'), ('4', '1', '24', '20', null, '0', '3', '{filedir_1}NatGeo11.jpg', 'This is a tree', 'This is another caption but it is going to be a bit longer.'), ('5', '1', '24', '20', null, '0', '4', '{filedir_1}NatGeo02.jpg', 'Polar Bears are cool', 'This is another caption.');

-- ----------------------------
--  Table structure for `exp_member_bulletin_board`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_bulletin_board`;
CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_member_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_data`;
CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_member_data`
-- ----------------------------
INSERT INTO `exp_member_data` VALUES ('1');

-- ----------------------------
--  Table structure for `exp_member_fields`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_fields`;
CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_member_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_groups`;
CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_member_groups`
-- ----------------------------
INSERT INTO `exp_member_groups` VALUES ('1', '1', 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', '0', 'y', '20', '60', 'y', 'y', 'y', 'y', 'y'), ('2', '1', 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', '60', 'n', '20', '60', 'n', 'n', 'n', 'n', 'n'), ('3', '1', 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', '15', 'n', '20', '60', 'n', 'n', 'n', 'n', 'n'), ('4', '1', 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', '15', 'n', '20', '60', 'n', 'n', 'n', 'n', 'n'), ('5', '1', 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', '10', 'y', '20', '60', 'y', 'n', 'n', 'y', 'y');

-- ----------------------------
--  Table structure for `exp_member_homepage`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_homepage`;
CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_member_homepage`
-- ----------------------------
INSERT INTO `exp_member_homepage` VALUES ('1', 'l', '1', 'l', '2', 'n', '0', 'r', '1', 'n', '0', 'r', '2', 'r', '0', 'l', '0');

-- ----------------------------
--  Table structure for `exp_member_search`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_search`;
CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_members`
-- ----------------------------
DROP TABLE IF EXISTS `exp_members`;
CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '28',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_members`
-- ----------------------------
INSERT INTO `exp_members` VALUES ('1', '1', 'bryan', 'Bryan Lewis', '440114ce3d83417ac3fd0713375cfedc39e39fa0ee0c271f699a1ddcac125f75df76925f97064ec03a808134c6230bdc0dfd4c05ade7d48682efbd6ebe4b3923', 'VkWSD=SG!4+?\"5jCvI-#(|nEo%&)Cjf>:>Tij6Kv36Xl&_(}4Wi0xEv%fzS>C]\'F%[[daj3[m8TjY`cC?o0gAxnoR.vS`Iip$}~#r$|a)\'#QR&S%mM6k#EH%K8G]\\>C,', '18f461409b2646dac9de82eddbe5d0dfe1c12b22', 'd89b774bdd0a214aca49395f503d290ac98d132b', null, 'bryan.lewis@gmail.com', null, null, null, null, null, null, null, null, null, null, null, null, null, 'default_set/expression_radar.jpg', '100', '100', null, null, null, null, null, null, null, '0', 'y', '0', '0', '127.0.0.1', '1382651440', '1387495884', '1387582432', '33', '7', '0', '0', '1383362337', '1383273044', '0', '0', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'America/Chicago', 'us', null, null, null, null, '28', '- Footer Widgets\n- Home Page\n  * Slider Items\n  * Recent Items\n  * Home Widgets', '18', '', 'Pages|C=addons_modules&M=show_module_cp&module=pages|1\nVariables & Settings|C=addons_modules&M=show_module_cp&module=low_variables|2\nFields|C=addons_modules&M=show_module_cp&module=field_editor|3\nPost Fields|C=addons_modules&M=show_module_cp&module=field_editor&method=edit&group_id=1&field_id=|4\nCategories|C=admin_content&M=category_management|5\nTemplates|C=design&M=manager|6', 'n', '0', 'y', '0');

-- ----------------------------
--  Table structure for `exp_message_attachments`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_attachments`;
CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_message_copies`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_copies`;
CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_message_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_data`;
CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_message_folders`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_folders`;
CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_message_folders`
-- ----------------------------
INSERT INTO `exp_message_folders` VALUES ('1', 'InBox', 'Sent', '', '', '', '', '', '', '', '');

-- ----------------------------
--  Table structure for `exp_message_listed`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_listed`;
CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_metaweblog_api`
-- ----------------------------
DROP TABLE IF EXISTS `exp_metaweblog_api`;
CREATE TABLE `exp_metaweblog_api` (
  `metaweblog_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `metaweblog_pref_name` varchar(80) NOT NULL DEFAULT '',
  `metaweblog_parse_type` varchar(1) NOT NULL DEFAULT 'y',
  `entry_status` varchar(50) NOT NULL DEFAULT 'NULL',
  `field_group_id` int(5) unsigned NOT NULL DEFAULT '0',
  `excerpt_field_id` int(7) unsigned NOT NULL DEFAULT '0',
  `content_field_id` int(7) unsigned NOT NULL DEFAULT '0',
  `more_field_id` int(7) unsigned NOT NULL DEFAULT '0',
  `keywords_field_id` int(7) unsigned NOT NULL DEFAULT '0',
  `upload_dir` int(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`metaweblog_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_metaweblog_api`
-- ----------------------------
INSERT INTO `exp_metaweblog_api` VALUES ('1', 'Default', 'y', 'NULL', '1', '0', '2', '0', '0', '1');

-- ----------------------------
--  Table structure for `exp_moblogs`
-- ----------------------------
DROP TABLE IF EXISTS `exp_moblogs`;
CREATE TABLE `exp_moblogs` (
  `moblog_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `moblog_full_name` varchar(80) DEFAULT '',
  `moblog_short_name` varchar(20) DEFAULT '',
  `moblog_enabled` char(1) DEFAULT 'y',
  `moblog_file_archive` char(1) DEFAULT 'n',
  `moblog_time_interval` int(4) unsigned DEFAULT '0',
  `moblog_type` varchar(10) DEFAULT '',
  `moblog_gallery_id` int(6) DEFAULT '0',
  `moblog_gallery_category` int(10) unsigned DEFAULT '0',
  `moblog_gallery_status` varchar(50) DEFAULT '',
  `moblog_gallery_comments` varchar(10) DEFAULT 'y',
  `moblog_gallery_author` int(10) unsigned DEFAULT '1',
  `moblog_channel_id` int(4) unsigned DEFAULT '1',
  `moblog_categories` varchar(25) DEFAULT '',
  `moblog_field_id` varchar(5) DEFAULT '',
  `moblog_status` varchar(50) DEFAULT '',
  `moblog_author_id` int(10) unsigned DEFAULT '1',
  `moblog_sticky_entry` char(1) DEFAULT 'n',
  `moblog_allow_overrides` char(1) DEFAULT 'y',
  `moblog_auth_required` char(1) DEFAULT 'n',
  `moblog_auth_delete` char(1) DEFAULT 'n',
  `moblog_upload_directory` int(4) unsigned DEFAULT '1',
  `moblog_template` text,
  `moblog_image_size` int(10) DEFAULT '0',
  `moblog_thumb_size` int(10) DEFAULT '0',
  `moblog_email_type` varchar(10) DEFAULT '',
  `moblog_email_address` varchar(125) DEFAULT '',
  `moblog_email_server` varchar(100) DEFAULT '',
  `moblog_email_login` varchar(125) DEFAULT '',
  `moblog_email_password` varchar(125) DEFAULT '',
  `moblog_subject_prefix` varchar(50) DEFAULT '',
  `moblog_valid_from` text,
  `moblog_ignore_text` text,
  PRIMARY KEY (`moblog_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_module_member_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_module_member_groups`;
CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_modules`
-- ----------------------------
DROP TABLE IF EXISTS `exp_modules`;
CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  `settings` text,
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_modules`
-- ----------------------------
INSERT INTO `exp_modules` VALUES ('1', 'Comment', '2.3.1', 'y', 'n', null), ('2', 'Email', '2.0', 'n', 'n', null), ('3', 'Emoticon', '2.0', 'n', 'n', null), ('4', 'File', '1.0.0', 'n', 'n', null), ('5', 'Jquery', '1.0', 'n', 'n', null), ('6', 'Metaweblog_api', '2.1', 'y', 'n', null), ('7', 'Moblog', '3.2', 'y', 'n', null), ('8', 'Query', '2.0', 'n', 'n', null), ('9', 'Rss', '2.0', 'n', 'n', null), ('10', 'Search', '2.2.1', 'n', 'n', null), ('22', 'Channel_videos', '3.1.3', 'y', 'n', null), ('12', 'Channel', '2.0.1', 'n', 'n', null), ('13', 'Member', '2.1', 'n', 'n', null), ('14', 'Stats', '2.0', 'n', 'n', null), ('15', 'Rte', '1.0.1', 'y', 'n', null), ('16', 'Deeploy_helper', '2.0.3', 'y', 'n', null), ('17', 'Low_reorder', '2.2.1', 'y', 'n', null), ('28', 'Low_variables', '2.4.0', 'y', 'n', null), ('19', 'Field_editor', '1.0.4', 'y', 'n', null), ('20', 'Playa', '4.4.5', 'n', 'n', null), ('21', 'Stash', '2.4.5', 'n', 'n', null), ('25', 'Zoo_triggers', '1.2.2', 'y', 'n', null), ('26', 'Pages', '2.2', 'y', 'y', null), ('27', 'Taxonomy', '3.0.8.2', 'y', 'n', null), ('29', 'Tagger', '3.2.1', 'y', 'n', null);

-- ----------------------------
--  Table structure for `exp_online_users`
-- ----------------------------
DROP TABLE IF EXISTS `exp_online_users`;
CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_online_users`
-- ----------------------------
INSERT INTO `exp_online_users` VALUES ('100', '1', '0', 'n', '', '127.0.0.1', '1387493794', ''), ('99', '1', '1', 'n', 'Bryan Lewis', '127.0.0.1', '1387402107', ''), ('95', '1', '0', 'n', '', '127.0.0.1', '1387493794', ''), ('98', '1', '0', 'n', '', '127.0.0.1', '1387493794', ''), ('97', '1', '0', 'n', '', '10.0.1.12', '1385697737', ''), ('96', '1', '0', 'n', '', '127.0.0.1', '1387493794', ''), ('101', '1', '0', 'n', '', '127.0.0.1', '1387581820', '');

-- ----------------------------
--  Table structure for `exp_pages_configuration`
-- ----------------------------
DROP TABLE IF EXISTS `exp_pages_configuration`;
CREATE TABLE `exp_pages_configuration` (
  `configuration_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `configuration_name` varchar(60) NOT NULL,
  `configuration_value` varchar(100) NOT NULL,
  PRIMARY KEY (`configuration_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_pages_configuration`
-- ----------------------------
INSERT INTO `exp_pages_configuration` VALUES ('6', '1', 'homepage_display', 'nested'), ('7', '1', 'default_channel', '5'), ('8', '1', 'template_channel_5', '10');

-- ----------------------------
--  Table structure for `exp_password_lockout`
-- ----------------------------
DROP TABLE IF EXISTS `exp_password_lockout`;
CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_playa_relationships`
-- ----------------------------
DROP TABLE IF EXISTS `exp_playa_relationships`;
CREATE TABLE `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `parent_is_draft` int(1) unsigned DEFAULT '0',
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=499 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_playa_relationships`
-- ----------------------------
INSERT INTO `exp_playa_relationships` VALUES ('406', '52', '11', null, null, null, '0', '50', '1'), ('498', null, null, null, null, '38', '0', '53', '3'), ('497', null, null, null, null, '38', '0', '46', '2'), ('405', '52', '11', null, null, null, '0', '43', '0'), ('496', null, null, null, null, '38', '0', '58', '1'), ('491', null, null, null, null, '31', '0', '54', '2'), ('490', null, null, null, null, '31', '0', '57', '1'), ('495', null, null, null, null, '38', '0', '60', '0'), ('494', null, null, null, null, '42', '0', '46', '2'), ('493', null, null, null, null, '42', '0', '50', '1'), ('492', null, null, null, null, '42', '0', '55', '0'), ('387', null, null, null, null, '48', '0', '50', '0'), ('386', null, null, null, null, '47', '0', '43', '0'), ('385', null, null, null, null, '46', '0', '42', '0'), ('489', null, null, null, null, '31', '0', '56', '0');

-- ----------------------------
--  Table structure for `exp_relationships`
-- ----------------------------
DROP TABLE IF EXISTS `exp_relationships`;
CREATE TABLE `exp_relationships` (
  `relationship_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `child_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `grid_field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `grid_col_id` int(10) unsigned NOT NULL DEFAULT '0',
  `grid_row_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`relationship_id`),
  KEY `parent_id` (`parent_id`),
  KEY `child_id` (`child_id`),
  KEY `field_id` (`field_id`),
  KEY `grid_row_id` (`grid_row_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_remember_me`
-- ----------------------------
DROP TABLE IF EXISTS `exp_remember_me`;
CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_reset_password`
-- ----------------------------
DROP TABLE IF EXISTS `exp_reset_password`;
CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_revision_tracker`
-- ----------------------------
DROP TABLE IF EXISTS `exp_revision_tracker`;
CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_rte_tools`
-- ----------------------------
DROP TABLE IF EXISTS `exp_rte_tools`;
CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_rte_tools`
-- ----------------------------
INSERT INTO `exp_rte_tools` VALUES ('1', 'Blockquote', 'Blockquote_rte', 'y'), ('2', 'Bold', 'Bold_rte', 'y'), ('3', 'Headings', 'Headings_rte', 'y'), ('4', 'Image', 'Image_rte', 'y'), ('5', 'Italic', 'Italic_rte', 'y'), ('6', 'Link', 'Link_rte', 'y'), ('7', 'Ordered List', 'Ordered_list_rte', 'y'), ('8', 'Underline', 'Underline_rte', 'y'), ('9', 'Unordered List', 'Unordered_list_rte', 'y'), ('10', 'View Source', 'View_source_rte', 'y');

-- ----------------------------
--  Table structure for `exp_rte_toolsets`
-- ----------------------------
DROP TABLE IF EXISTS `exp_rte_toolsets`;
CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_rte_toolsets`
-- ----------------------------
INSERT INTO `exp_rte_toolsets` VALUES ('1', '0', 'Default', '3|2|5|1|9|7|6|4|10', 'y');

-- ----------------------------
--  Table structure for `exp_search`
-- ----------------------------
DROP TABLE IF EXISTS `exp_search`;
CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_search_log`
-- ----------------------------
DROP TABLE IF EXISTS `exp_search_log`;
CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_security_hashes`
-- ----------------------------
DROP TABLE IF EXISTS `exp_security_hashes`;
CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  `used` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM AUTO_INCREMENT=11032 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_security_hashes`
-- ----------------------------
INSERT INTO `exp_security_hashes` VALUES ('10712', '1387401495', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '7acce2a6639125d5b1baeec7564a10e195155ace', '1'), ('10713', '1387401499', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'fc1cc30cb69881d0158c826535d68ffe56b80d23', '0'), ('10714', '1387401499', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '5ce8bc90cb4131f985ca77c16c26294b8e14410c', '0'), ('10715', '1387401501', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'd89c35e737067e417421fd55bacd192cc5200029', '1'), ('10716', '1387401506', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'ae489cc536f831473e6e378d346a2c52804144cd', '0'), ('10717', '1387401506', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'b9c9a072567e062839c8bbd33dd70783140c18f6', '0'), ('10893', '1387495901', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '5ccff7fb2c9c335b8b4e4af7c791831a39d07ed6', '0'), ('10892', '1387495900', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '3bcecde00b0109ee6d21c0a76fa33a224e95b27e', '0'), ('10891', '1387495899', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '7dd3be62ab32138370b40e3108a6b016c62b0a4c', '0'), ('10890', '1387495897', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'cdbef6b71d1f2837b2e971f0b79bffe443f81ea1', '0'), ('10889', '1387495894', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '47d4996bf5025817f9be3f021146ec3a8f3cefcc', '0'), ('10888', '1387495890', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '3df4bc4331413f2a08121bc4ffc430c00898c3ba', '0'), ('10887', '1387495889', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '1f1f9260945bbbf63c10f4a2faff18783ad1758e', '0'), ('10886', '1387495887', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '0c81e2f3fa70de1bdd5a7b3bafba8ff539eab8cd', '0'), ('10885', '1387495886', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '87422310d6497e64493aa47da8d56b2367665874', '0'), ('10884', '1387495884', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'ebfc236643fb79e7f551447521fc35d50d02c3d6', '0'), ('10883', '1387495883', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '958cf15578b83a3442fb2f41d26417c848876914', '0'), ('10882', '1387495882', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '145895e42d08962ffdb2fc0821d80710195da9ec', '0'), ('10881', '1387495880', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'c942e59dd3cc6884c966e9f8494feb71a1ee20a7', '0'), ('10880', '1387495879', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '0bcb5a0d4e42fddbabcd36261305a342ea5d4d51', '0'), ('10879', '1387495878', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'a5a8a892e05127c218104b9616ade9347af5975e', '0'), ('10878', '1387495870', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '06953f6edc31a4e208843e2d053bddf58122ff3b', '0'), ('10877', '1387495718', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'c69ce9c9290c9d7dce29780aac36cf95972769b1', '0'), ('10876', '1387495717', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'ac2e61926d9d0ef1e7cb7bf5dade954a519025dc', '0'), ('10875', '1387495708', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'e7cee81a9e3bd9e9668e5d6e55ebd07c1902ee97', '0'), ('10874', '1387495583', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'a282e995b3158cbbb5d7d0da6f98f8848050bbb9', '0'), ('10873', '1387495252', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '7b675ac0007d4f0bd16587b684bcc584bf97bc32', '0'), ('10872', '1387495250', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'ea7022cf68b0ca8bad65c321408a6d2186869f2e', '0'), ('10871', '1387495248', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '4a3bd40ab071800095d0735265bbf2b2e1c7e6a7', '0'), ('10870', '1387495247', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'f2a2266ab09efb663896dc45fc6b0809f7d2533b', '0'), ('10869', '1387495244', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '35aabd6d63f028484eed5d4e67ceb521c87a0038', '0'), ('10868', '1387495243', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'ff4baaea7b2b6acc79ea74fc534725f790fe1baa', '0'), ('10867', '1387495242', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'b5814ab685e77ea5ad082162c2f2e1283f2a847f', '0'), ('10866', '1387495241', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'e1f5a07b79c74ead90dd996dd6358e30c412fd14', '0'), ('10865', '1387495239', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '3929a179363daf6198002342451da48ac0bf55d6', '0'), ('10864', '1387495225', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '0bd9a53feb03acc37d7dea78e31ba96466047fbd', '0'), ('10863', '1387494900', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '6daef5885d8f0a36eadb2d75707d7208f61d8350', '0'), ('10862', '1387494900', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '1379e56c763c2f8f5e6e891015a60a5c66383af9', '0'), ('10861', '1387494876', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '44f37c20fc88a4443e2f3062be50abc5512dd092', '1'), ('10860', '1387494874', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '6069ae15d180ddc05ce9d78da8a44d74fe63306c', '0'), ('10859', '1387494872', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'c4e041f00a06c538c3f13166a58796fea6f39322', '0'), ('10858', '1387494871', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '2d82802c33eee33c2b5453a694c0e446c1f2c409', '0'), ('10857', '1387494868', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '0768b54c9f2dcb67958d8b66df5f1239fbe4a49f', '0'), ('10856', '1387494866', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '880b5f72d97e52e22a9621d3a500b3c187aa3e01', '0'), ('10855', '1387494863', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'b17ae4283c92c7e3e8bedb06c177bbd2e9f41e09', '0'), ('10854', '1387494861', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'a1b621b25afd7aa1ede277d0ed7241b122b8158b', '0'), ('10853', '1387494860', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'a147d5829740c1167b66c2c15bd971f3297d3c2b', '0'), ('10852', '1387494854', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '0899aa31181eaa434526a1941958e4a1dc6f09a5', '0'), ('10851', '1387494854', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '31f4a4c324823752d59117d62eed0b52fef3cdfd', '0'), ('10850', '1387494779', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'be3492e437cc39cac6e196f5f635a14dfdc67bea', '0'), ('10849', '1387494776', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '37895d5bb36abcc88328f1819e103a23baa23f1c', '0'), ('10848', '1387494776', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '03e7c31d224f9eab5441b31c873802ed58079a01', '0'), ('10847', '1387494776', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '4202291ccfdd636cc829662d385d5cbcd14df42b', '0'), ('10846', '1387494776', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'a1986ebfcfaf5a65670a5f9c034c59319a8f76b7', '0'), ('10845', '1387494747', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'e33351a23286f3483bbc8a217e9b1ec8799e28a3', '0'), ('10844', '1387494733', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '7451c12ab6597d2b440e471789b7081216685d6a', '0'), ('10843', '1387494472', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '21769e8d2d17a1f378daf3ebb61462aff7c9adb5', '0'), ('10842', '1387494469', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '6f3952e5df58f5c81d929797c69d911840d8a501', '0'), ('10841', '1387494468', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'ce3ecf8e1ca955c5f82a023baa96a05264beada9', '0'), ('10840', '1387494466', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '4b5cdc91b93fcb42001d0dc3ae6169e16ccc9222', '0'), ('10839', '1387494465', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '7b7ba9c12d5d3070f96c97180d5342b496f2d1b2', '0'), ('10838', '1387494464', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'a680cb37065a82a04e081a8082e652baf1bf9662', '0'), ('10837', '1387494462', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '55b3a04af517841a9cdc8051ce3233c2106c09f0', '0'), ('10836', '1387494461', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '1a9e87108a09ced25800f118a0b3ddd4b9592824', '0'), ('10835', '1387494456', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '540d867a5159b9d00cfbe9393b5b2f667b654480', '0'), ('10834', '1387494256', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '87a2e197652e7a9f7c48fa2cd17913b57202c578', '0'), ('10833', '1387494253', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '008f8c788fbc4ba2e15dc01f962e1e097031f387', '0'), ('10832', '1387494251', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '09e65b941ed4b5f9bba9bdb8e70ed290f1a20fab', '0'), ('10831', '1387494249', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '6aba2d492bfea91e2770ac4521edbbab121bb8ce', '0'), ('10830', '1387494247', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'e16cdd1c8f0508396dcd6b38111837b84034bf98', '0'), ('10829', '1387494239', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '60542601d707ae4169e77cbcbb4a0dcd8870192e', '0'), ('10828', '1387493935', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'd11d736f29c2953f0bc8e9036de6f320ec10858c', '0'), ('10827', '1387493911', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '9c47faadff9a637f705ba9aa599610e343ab5b7d', '0'), ('10826', '1387493844', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'cd7f5a746bd1b1931ec31b6bda5b681717297bec', '0'), ('10825', '1387493842', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '2b9be92cd4c42e1ed2fd5e1f0a68927522bee474', '0'), ('10824', '1387493841', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '8e72b099467e24111c0987e72a8ed7cdf9b43e4a', '0'), ('10823', '1387493839', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '7bc9c95159699b77dec982b557370b99f54b5181', '0'), ('10822', '1387493828', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '988b31c5edefa1c3d9c8e2513cd6e1814bf4892a', '0'), ('10821', '1387493828', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'f5a42ba98b9eb0684845bcbcc639d89008d6221e', '0'), ('10820', '1387493828', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'ffcd08adea16bb14b88e68f7428fa07767f4bc65', '0'), ('10819', '1387493827', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'b25e51359d93727b3ec1d299d0891173935fe4eb', '0'), ('10818', '1387493827', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '2d2480efbdcf88af6a5a408e091efcbbcae5123c', '0'), ('10817', '1387493810', 'ade348efd9eb3d551825a1a96386e4727b7bf810', '92e64c5b95e72543d97900c94fa23956e5a9bf51', '0'), ('10816', '1387493795', 'ade348efd9eb3d551825a1a96386e4727b7bf810', 'c1295652774559e7379df407320f1c6f4dc6663f', '0'), ('10815', '1387493795', '0', 'e711d5265d7f93482d3a8d977bd8b1406d5485db', '0'), ('10814', '1387493794', '0', '057d63e7c7f2dfbb98a054511c1b30a19b3d7098', '0'), ('10813', '1387493793', '0', '34ffa63d0df2c2c5611de65dffdd488113ea6bd4', '0'), ('10812', '1387493793', '0', '13d8f165cc2cf289a699f3674d085f3fa662508d', '1'), ('10811', '1387493793', '0', '8b9def244db003ad009e160e9369ef0bd781005d', '0'), ('10810', '1387493793', '0', '5ce068ff10e811dccccba8333cdbb3d0998d6b19', '0'), ('10809', '1387410912', '0', '0260b99f11e82a97000cf70f743c1411d77f48f8', '0'), ('10808', '1387410912', '0', 'e158a379b1a38cc0b1fa138161c4aab0edab1c6b', '0'), ('10807', '1387410912', '0', 'a56446133c00bdc6b2c45ffd89818ce06112c07c', '0'), ('10806', '1387410912', '0', 'dce04c257afb4adcdaa7e803936a032b13434c43', '0'), ('10805', '1387410911', '0', '2b196faed6410f5f0d89503961b0f7be641e9a36', '0'), ('10804', '1387410911', '0', '9e21342f0e33b1efc5c9b82c95ae1af825ca0eb0', '0'), ('10803', '1387410911', '0', '2a7f90008c044f2c4e5ded109701fe31bda83763', '0'), ('10802', '1387410911', '0', 'bf8e00587f5579e484931929ac190542f2b2d3ca', '0'), ('10801', '1387410911', '0', 'a520aa5f602f516cf491b6222c4c86609495ae5a', '0'), ('10800', '1387410911', '0', '4a4ba709f076bc4354b2b5c339553ae614ebef9f', '0'), ('10799', '1387410911', '0', 'e22907ef8073244cec9b785f7fa089c1bedb3299', '0'), ('10798', '1387410911', '0', 'abe761c66c60817078f1f6eb91831f52ed365d1b', '0'), ('10797', '1387410910', '0', '579efc0eaeefad88290302f3cbe3c45779f10c2f', '0'), ('10796', '1387410910', '0', '7532d2ae297d813738cb25cda1544dcff456b7f5', '0'), ('10795', '1387404815', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '41dbc9b05d96c6716096b195c18d7bc84c03851c', '0'), ('10794', '1387404813', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'dff4a234410b60683d23d9678ee48d1e4688caf3', '0'), ('10793', '1387404376', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '55c9832cda4726c8f3e76f304413ce8be1b61251', '0'), ('10792', '1387404366', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '38b5a0660cac23e98801b52947bf67d8d7e566e3', '0'), ('10791', '1387404284', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'a39ef33ef39d57c38ccc5cde2b72c40d2ea2af6a', '0'), ('10790', '1387404251', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '65057544039c7a2ac5d9181f8dcd3571ac34e3e5', '1'), ('10789', '1387404183', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'd49cd80d74140c6d34d596387b1c74d426615ade', '1'), ('10788', '1387404074', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '7dd10f7fe7c1193b1485cc207408184eb485383a', '1'), ('10787', '1387404072', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'f45d4c6adbdfe82f2a8a66f65ffb72117ad41feb', '0'), ('10786', '1387403957', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '9d4a41de36b31303a7c632ffab166e8f5947ebe1', '0'), ('10785', '1387403901', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '99f68238dc67b149d2eb0d3de19bd8ebeb530540', '0'), ('10784', '1387403899', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '995fd6cf7d9451ec4d065459540d81313e838ebc', '0'), ('10783', '1387403895', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'e042b5dfce764bc9c62004c6efed960ff094b47b', '0'), ('10782', '1387403653', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'f954ac8595a2414c5fcab78fb96fafee06d171ce', '0'), ('10781', '1387403638', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '723be1926d7562318d10ad559e7e07a2da2fc8d0', '0'), ('10780', '1387403621', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'c9ae0ae570e8d4c33e18c241a2dd86c97efa69de', '0'), ('10779', '1387403619', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '18a006b6795e4856de3ef719e5057b1bbff7cfe2', '0'), ('10778', '1387403614', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '99e8dded5cb2c036311324919fbcc2d18ca9f919', '0'), ('10777', '1387403612', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '4f106f8861323776851be7b37151624504c3b44f', '0'), ('10776', '1387403610', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'a1bc6fa9bb4ee69387a7338e84e513713a0442b9', '0'), ('10775', '1387403608', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '015efd615c69cbfe996b290d324e4591f26bb30b', '0'), ('10774', '1387403608', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '67260f9cc8a15ce4263b0ed19ddfa0c440eb2fb1', '0'), ('10773', '1387403608', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'd0fad59159afa94c8cff8958bcfd63b453eef49a', '0'), ('10772', '1387403607', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '36837e02664eb1cf99204d83afbd03233e425fde', '0'), ('10771', '1387403315', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'b4d3ab715c9e0eda0b876e71f4110fa3ad4b6239', '0'), ('10770', '1387403314', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '9ee94ec8f9a497f86c6b21097ca5ddf2d55bb299', '0'), ('10769', '1387403313', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'b7ea0b68c27e9180f60ae9093b76df2c963e67e8', '0'), ('10768', '1387403312', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'f35cfa3b25e322c573ba13d3ef707b70c631d57a', '0'), ('10767', '1387403310', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '592c146a3ba4bee77aefb51a5445895eb57c3fc9', '0'), ('10766', '1387403308', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '60d29f2a36806cf72c543ad689c1aa5ebfcccb3b', '0'), ('10765', '1387403307', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'b4e007252550f76d0ef1c3d442eb2e193aa6aa3c', '0'), ('10764', '1387403306', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '038befb3dae26ac65eb788f3cfc76999a5b4cc36', '0'), ('10763', '1387403304', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'a25fd38d304330aa572c4059177a40ce342f802f', '0'), ('10762', '1387403302', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '8ebc94b95c2a46089412adac7a9373a19fe7d025', '0'), ('10761', '1387403301', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'f73b8d857d8e5b2de0e7b3893c4f7abbbdef383b', '0'), ('10760', '1387403299', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'd3f48f0a5b0a331659ef23083f1be54fc33d1b4e', '0'), ('10759', '1387403246', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '308144cff19f391d60eeabd2ecae3d7bcd3fc563', '0'), ('10758', '1387403216', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '5b1a328ae28774e25fd6552de507426299f2a3cc', '0'), ('10757', '1387403047', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '25c8ddf701fdc3db81e3caee88c299bd45733e70', '0'), ('10756', '1387403024', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '5ac57c16bb2d26550f542f8861ee711fba90d1c2', '0'), ('10755', '1387402107', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'db9469f93dd99702e1e0b5046bbe5f15248822e5', '0'), ('10754', '1387402107', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'a30591e6d43da2a53f81553aff661d06f05067e9', '0'), ('10753', '1387401814', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '15f5359dfb6134fe51e3b9965a3fecb5be9ba048', '0'), ('10752', '1387401814', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '0661df1733227893bb900ccb8ba6e9b997727448', '0'), ('10751', '1387401810', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '6b7f7f2d43b19eb42d8b3fb825449a48e47560c1', '0'), ('10750', '1387401805', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '98e9e374ce8c227fd47354a006b478c49a158054', '0'), ('10749', '1387401679', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '5276d7ae81ede542785515762deb4eff8c2e99c4', '0'), ('10748', '1387401677', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'ad54e352205098cd1127e0f137f1163ca84e7425', '0'), ('10746', '1387401657', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '6bf6b5355205339bcbcba8198726ab82bfb0de2a', '0'), ('10745', '1387401653', '0', '60d35ea57f0c0cccf9c94e9b4d8302009539b008', '0'), ('10744', '1387401653', '0', '53c466429038806c705da408e78fb871e2810efa', '0'), ('10743', '1387401653', '0', '7920fcf15b460e7b349a8ddd2d358d3224d878a8', '0'), ('10742', '1387401644', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '0585822ff2a7b0746de0eff77ede8bf109cf3615', '0'), ('10741', '1387401622', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'cc7f10997514226e6893e6cb169c33c4058e7a40', '0'), ('10740', '1387401610', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'c630d0e48a90b41868e7a764fb495fcb13d95cda', '0'), ('10739', '1387401574', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '9304fdc044a15c0d7d1e5bed9342b3fb0fc64a34', '0'), ('10738', '1387401574', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '42775a9003b6fafb89da5b94afd778ff149d2910', '0'), ('10737', '1387401551', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '64fde9333d55f9b683b2682267688bdab2ca76b7', '1'), ('10736', '1387401550', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'adef2a5b9eb1d319b838d9c0a210f4ca57c494cc', '0'), ('10735', '1387401548', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '9d5965fc559bbe256571141beb57d0e50d61b3ff', '0'), ('10734', '1387401548', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'cfb1d2778fe1a8858b1402c691b94441a3cb65a8', '0'), ('10733', '1387401543', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'cd5c39b05d5b7e14d8a6c0b5cf2d62d5c3425bef', '1'), ('10732', '1387401541', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '4b09b40f2fba5dd1ef9173ba640ff291e318de66', '0'), ('10731', '1387401541', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'b38a1c4da300c3336b7b9d430850e20aee4ed874', '0'), ('10730', '1387401535', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '1c87a627550a81dcc0fd22caebac146668a465af', '1'), ('10729', '1387401534', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'bd88218874681d958c87838d96a405bcd38881c2', '0'), ('10728', '1387401534', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '2ee5ddd43fd5d4e902747752f4d88f94eb58dca9', '0'), ('10727', '1387401528', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '98f57842052d6054f122bd00a511afad64040103', '1'), ('10726', '1387401527', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '449654f872313c98629418b690de07864e8505e5', '0'), ('10725', '1387401527', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'f2794cb79618d8831601c84240b4aaf27cf9e5fa', '0'), ('10724', '1387401521', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'a323fdee33381ba138492fbd0a34d663089538a3', '1'), ('10723', '1387401519', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '1fae07af3f60b6b8aac06240f7a42c03f31d84ad', '0'), ('10722', '1387401519', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'baeec635c7faedfe8cad5e4b35a1fe111eb2d4f5', '0'), ('10721', '1387401511', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'e6cad07f8fb1bb492a6211ce99e6dcd864522d46', '1'), ('10720', '1387401510', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'fccbaa5412e87d8ebd7af78ea2d5c06b62a8f87d', '0'), ('10719', '1387401510', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'c31b0e60bf19e0e08b713adc579c778d34aff433', '0'), ('10718', '1387401507', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '41767fd5d9ffd67734a7e636b881903fbec85843', '1'), ('10711', '1387401494', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '13ac0aafeb7dbdcfa2060fd2139d0abaaa2b75bb', '0'), ('10710', '1387401494', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'da9a7ed54f0c46b00fbf5ceeb1b332cab2a23d2e', '0'), ('10709', '1387401490', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'e1f22ad34d59dc8fd29b656ff528749d1ede8ac7', '1'), ('10708', '1387401487', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'd8457618e9cf992bd6fac787ff3291f84812a269', '0'), ('10707', '1387401487', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '33377b8af5234778c9e62eb17128cb9fc21e06bd', '0'), ('10706', '1387401479', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '2286dc2de48e882039fe347abea8f8499159c79e', '1'), ('10705', '1387401464', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '1ace67b8c05b4c930763bd7d822e326c3c61dd51', '0'), ('10704', '1387401463', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '4fdc25bf1bb241a14e6f926c4dc7f5f089ce8471', '0'), ('10703', '1387401461', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '30c6d35d41f331b639af34fbf1ef494196278111', '0'), ('10702', '1387401427', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'c07ecb2737fcc28eca5ee27244913e736100ed03', '0'), ('10701', '1387401425', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '798e412a060c652ef3ff56fa33ba3e39d7ef67f7', '0'), ('10699', '1387401418', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '52ad71ff07d815c73596ca46939f6e5d303795a8', '0'), ('10700', '1387401419', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '41c22c5e70642544a83da82ee659909fdeccd9a1', '0'), ('10698', '1387401416', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '464f0947264e9578260fdf4bd65362bbe16904fa', '0'), ('10697', '1387401354', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '5c17224e13ccf615e67b89732f58621ad122bea1', '0'), ('10696', '1387401352', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '26b4b28c93cf0115022734c9b895df0b363459b2', '0'), ('10695', '1387401352', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '6975a24fc63faf17928ffe61f2de49cd69f7ce93', '0'), ('10694', '1387401329', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'bd2c49fc216cd2f068d844b0c736ae51f91e54dc', '1'), ('10693', '1387401329', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '9bca662a98eedf4625540136ef1ed2da1e53f9a1', '0'), ('10692', '1387401322', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'f9e6947db865049137953337943adf75cbfa2792', '1'), ('10691', '1387401319', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'cd3d5032ca717a0cc2c7cb2d37bb6bb8baa5dfdc', '0'), ('10690', '1387401315', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '7eb849f0728067789e60be0618cb3f5c5917fda5', '0'), ('10689', '1387401311', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '796bd5e4e58b2f9ee08445b1437333d76dbc9ef4', '0'), ('10688', '1387401308', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'b41e5eb3362fd690f72e3c6547b9747f6b43e069', '0'), ('10687', '1387401308', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'a91d6bcc973f314c0607c10adfc72b01faf678fa', '0'), ('10686', '1387401307', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '641c0aff68b74a76e387d65bfb4d65efb702c3f8', '1'), ('10685', '1387401307', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'f78e9692e859040402bda0d4109f0d864aacf0dc', '0'), ('10684', '1387401302', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '0291905983a478cb0c959054352a5b1d5da749d7', '0'), ('10683', '1387401302', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '2d8907dc11662da9d5418ad4e7fe6aadcdb7639e', '0'), ('10682', '1387401300', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '20f63618e04bea2398eef00ddb869aa6209552de', '1'), ('10681', '1387401300', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '0b89fa326648d40eba4c930e8a9faae2100dba39', '0'), ('10680', '1387401298', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '7d2f0370dab778a9794cb4cd67a469bd9fb1ca9d', '1'), ('10679', '1387401296', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', 'b25a433ec1495f485a729b2e4b56b5537322799a', '0'), ('10678', '1387401282', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '67f01c049c08a55ce66cf7cecc9681859717ea5d', '0'), ('10677', '1387401279', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '1dc7772577748156041eb96f7bef77c2a82212f0', '0'), ('10676', '1387401279', '0', '097b9695066485ae1ff05023573902c3bef375bd', '0'), ('10675', '1387401276', '0', '1f3e2d7bbeab3fcd83a35c77afec55886e1309d4', '1'), ('10674', '1387401276', '0', 'fcbaa6ee321a2a9f22edb7895b7475f1ca05108f', '0'), ('10673', '1387401276', '0', '1b3aa65c5f8240ecc85e281d8d74b4d46ebdd0c3', '0'), ('10672', '1387400979', '024b659a4b83de821f95d749d152fdb152ce8817', '2930d2d9c280d5972a4edc692d1c1a2d8f3b968d', '0'), ('10671', '1387400978', '024b659a4b83de821f95d749d152fdb152ce8817', 'ba8c938ca1c906a110bf2704c1e8bab53ebb9335', '0'), ('10670', '1387400976', '024b659a4b83de821f95d749d152fdb152ce8817', '994be4e8ef6d254bea08afb583b0ae13b17b0925', '0'), ('10669', '1387400970', '024b659a4b83de821f95d749d152fdb152ce8817', '5e401e34386514b37a0069395b327bbfcccff676', '0'), ('10668', '1387400970', '0', 'a65d168e89367133b837eac740176d7f90391912', '0'), ('10667', '1387400968', '0', '3687bf4692fb82d4c22178c0654fca8e7003080d', '0'), ('10666', '1387400967', '0', '14c0f889807165c5c5ad62617e7e2a4e4b448697', '1'), ('10665', '1387400967', '0', '6120e26a1101e719c86cb481be0be8955ed789e0', '0'), ('10664', '1387400967', '0', 'c97a9eb8205a4d2fc58311b4c46a54bb21e6f5c1', '0'), ('10663', '1387400964', '0', 'fc9076cdb5f3b684be928f4f5f91f6bdbbc6d672', '0'), ('10747', '1387401668', '7bcc85a551d036de6df6bc1d2ebed03060fb908f', '14fb4d92b8f8fc06f5af9a39632c23ac0b4d1386', '0'), ('10894', '1387581820', '0', '37a85e4b70ce3b226e93f8894ab28d2c8b138c02', '0'), ('10895', '1387581825', '0', '01c42c2ed5d57e31c1c0e9141d20a00e03b56f6a', '0'), ('10896', '1387581825', '0', 'e40c82d4df13e8a7dcd09a6b166bffc620c26076', '0'), ('10897', '1387581825', '0', '68825b613ff10e64fe0936ae16e7bdf7f4521e6e', '1'), ('10898', '1387581828', '0', 'fa35e1823b072ba65deff31788c4679a573de986', '0'), ('10899', '1387581828', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'dea6dba6312a5114b8f48da00ee0ab5503380a14', '0'), ('10900', '1387581834', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'd590e8d9805382cf19608f6af6b2b6379a0749d9', '0'), ('10901', '1387581866', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'dc8a1dd47744082a75d28dfaa736e83893af321e', '1'), ('10902', '1387581924', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'dd946af55803bf0b5ebfeafe7bc41058eb5b40d2', '0'), ('10903', '1387581924', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '1a8d9f3cb8977b7bb75364afd047e52984131a10', '0'), ('10904', '1387581935', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '44107c388cff6156a5ba06a7672fcf00c259c300', '0'), ('10905', '1387581938', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '3a424cc06d24dfdf0603a6cce65d4eee809fd0ec', '1'), ('10906', '1387581943', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '8f119add764ab13a571647a2ba03cc89505b319e', '0'), ('10907', '1387581943', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '74da46e47b8fe748cccc12e71a055375fdb54f0f', '0'), ('10908', '1387581946', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '6dbd04115d6dcba75e1b9b7767960963149be35f', '0'), ('10909', '1387581954', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '08bb1ed775ebe4fe31e626f88d0f640907d0f122', '1'), ('10910', '1387581977', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'f948b10440e5d61aefb8f323bd9d633a350f9adf', '0'), ('10911', '1387581977', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '2ca2f5e047174c37bbb4f5d2c8851b2b490c7b49', '0'), ('10912', '1387581986', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '654aee6105b95d63672e3003b1e7d3cfe7e86511', '1'), ('10913', '1387581998', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'cb87060bd995edae318d7f677bc488e41b01309d', '0'), ('10914', '1387581998', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'aeae94f72efffe60feea46634498c6debef5ae5f', '0'), ('10915', '1387582002', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '1060877e47b7a3fe95842d2a65eaa1d514834be6', '1'), ('10916', '1387582009', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '0598de45ff8c5ecdc3fd97ef7b6cb3f5664bd9b6', '0'), ('10917', '1387582009', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '13a59f28fb83c78fe2cfbda2c6ac71c1d27cf3b5', '0'), ('10918', '1387582013', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'cbcde297b717c10d34c354e2a7d342fc44b878ec', '0'), ('10919', '1387582015', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '9f753c962584917b903ef2a32da3a7e012f6e14b', '1'), ('10920', '1387582020', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'a4d82e1347c99c45e46607ac8e7428231aae9fd8', '0'), ('10921', '1387582020', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'f214ebb9a09b292f6ea3564c1a0cddf9ad9ebba4', '0'), ('10922', '1387582024', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'be4d85f37c976b21df578c1020ad133a2b110f2a', '0'), ('10923', '1387582029', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'c8817e692c141bfff3a124bd5d58c90857aa25a2', '1'), ('10924', '1387582034', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '7ac3ea40199624474fc28e7c19203a2b504a6c83', '0'), ('10925', '1387582034', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '9a0f8538dc9f52428c76aa45c1b36e117e8b1288', '0'), ('10926', '1387582040', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '6e711414b7c7b047f23203fad7d771aaac49f0c9', '0'), ('10927', '1387582043', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '745f3d3c1fa9027c73f4dd54a7dcc0f64a4c7d9b', '0'), ('10928', '1387582048', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '0c4fcd3ade9996ef3214a84bd6ea5da6604481a4', '0'), ('10929', '1387582050', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '881c2f1a00916598262b58511c158b93a5fd1006', '0'), ('10930', '1387582070', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'eb95d9cbc594a862784997193fda56cf9043010d', '0'), ('10931', '1387582075', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '58ac08180484a8ab7e19c160a9e6883567f39617', '0'), ('10932', '1387582076', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '90cc81537f3802aafebf316a5ef6859b7ab4017e', '1'), ('10933', '1387582077', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '5ea1f798a93b5ecfd105ffeed32e70697a11468e', '0'), ('10934', '1387582077', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'a80f13084a8647961e5ff7fcd99383b27c195c5f', '0'), ('10935', '1387582080', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'fba060b746ac6b2b6218d532f8dd8eb188ce3626', '0'), ('10936', '1387582083', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '0757ed35bbd45d5b511f123b3e0cfcaebf367589', '1'), ('10937', '1387582107', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '6d5996af98a1c5e43021afcccc23bc2115d03334', '1'), ('10938', '1387582118', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '462eb55367b669c4c54fc6040cd853056e273439', '0'), ('10939', '1387582118', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '9c0bbca56897f4f7c63bf245cf6c419715f08f2c', '0'), ('10940', '1387582124', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '2821de0f315e9e88626345f1c23ced1b91f83be1', '0'), ('10941', '1387582131', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '3f3dbedb1dba6dfbc1d3327b54c1a7ab0835d093', '0'), ('10942', '1387582135', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'd667c02c744265368d25c8fe801d5b2022708da6', '0'), ('10943', '1387582137', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '37932fbc34a5469c805d2999d8372d52e71c7808', '0'), ('10944', '1387582140', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '8d6624fa9bbe37c3b0bee07ba9b5d573324df144', '1'), ('10945', '1387582148', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'd14fc6f70a3fa6aba0bc9a01d2f372f66aa9a6fb', '0'), ('10946', '1387582148', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'a4090358aacd082ef16a4d7127566d58bfd2e660', '0'), ('10947', '1387582150', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'f99e2921c28505de4b334ec89e18f42ea1fe71b8', '0'), ('10948', '1387582165', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '172de512867b7aedf695c3b82476c3de559023a8', '1'), ('10949', '1387582166', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'b530aab87d48172db432e6ca83113d5a51797545', '0'), ('10950', '1387582166', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '12e3d83d90a62b7635fc2ddf39207233b248604a', '0'), ('10951', '1387582166', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '54c942fbf262c406c7f054b81c131477a21040e3', '0'), ('10952', '1387582253', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '1cfcc87786c66a4d97d2d204810a7381ea84fec3', '0'), ('10953', '1387582268', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'd73c54925ac5dd56dfce47bf02d54050dfa90f35', '0'), ('10954', '1387582275', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'b36012a9e9a617fef57848d3e1998266646f392e', '0'), ('10955', '1387582280', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'ff1fef36a2801884be00331a8c5e5e4c7ac715ac', '0'), ('10956', '1387582283', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '071cd3f368c7fabd0bc9f0c6b14f5835a1fca2c8', '0'), ('10957', '1387582286', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '711022f88e83955f439b33fda3e287af527c9bc4', '1'), ('10958', '1387582290', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '52e67e31637a8e5a2ce5543650096da55eb1d3eb', '0'), ('10959', '1387582291', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'f763f1e3060a5a1c6e6e5507d392dc97ca4487ff', '0'), ('10960', '1387582295', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '54f3a75bf51b713652ec2b9fca3f96daf2b257a1', '0'), ('10961', '1387582295', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'cccc42bb7a29c4771b5e542515f1f0806d1fa39c', '0'), ('10962', '1387582295', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '088233f8cf06f403a46223bdfa504e8136b80271', '0'), ('10963', '1387582295', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'e8898193767916c1e66b5b288b1f341d562e6835', '0'), ('10964', '1387582299', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '749aaf1946bef2f041fdc2a668f5055504a6a4cf', '0'), ('10965', '1387582301', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '0c98ccae98b337e34504bff73a8a5b18457e110c', '0'), ('10966', '1387582303', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '225d2839437e17f46ac26d86d1dd881173c312d7', '1'), ('10967', '1387582303', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '63ff98385c7d7b1ac383482e3b7634c926827fab', '0'), ('10968', '1387582303', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'eb37876bd9b04a9537058c39b75aac6cb1c58860', '0'), ('10969', '1387582303', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '6f5ec6547e0cc8880966569ae4639338fb3b4bcb', '0'), ('10970', '1387582329', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '56950be666ce262ab48c14ae73595ca5549a99d5', '0'), ('10971', '1387582329', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'b12e0b1de54d34b52335db734352d8b2b125b10a', '0'), ('10972', '1387582332', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '4bcb9bfd8dcf760b2d4a94f43915e87d103c3260', '1'), ('10973', '1387582332', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '024605fbe6c67546dfefc28f3e2d2cf3bfd31153', '0'), ('10974', '1387582332', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '192b0f98c037fe9e4a890b981e348535f902d9e1', '0'), ('10975', '1387582332', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'bc73e8caaff9bf7334d1394617650edddf5e245a', '0'), ('10976', '1387582343', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '04b0bcae7edfc86e61a710630ba579b5f8312a20', '0'), ('10977', '1387582344', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'a6b7cde4c910e3a517505211f6fa2388db8c0ddb', '0'), ('10978', '1387582345', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '2b94f94ac371c0d4422f30db864f0a3a925671c9', '1'), ('10979', '1387582346', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'f3969077f6843ca9595b9e10e22c2a89ec50800d', '0'), ('10980', '1387582346', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'e3df0da11ba5183f82040336b1dd4f333ccb6ce1', '0'), ('10981', '1387582346', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '835c95b91bbefd09ce9b6978aac2c73c2df06214', '0'), ('10982', '1387582379', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '77962f6b19730758514ea407108caf4c2bc71760', '0'), ('10983', '1387582379', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'd969a19891ada27b50acce10eba209e87fcd28e4', '0'), ('10984', '1387582382', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '73ae4bcf9ee5ae2f1b82bd1c3dff316676dff83b', '1'), ('10985', '1387582382', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'cf0d4645ff5579ac6a2140093cace5c5e23774af', '0'), ('10986', '1387582382', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '06dc714fa50eeece899269a3f9e58872941121f5', '0'), ('10987', '1387582382', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'fc9a74e771c40977d711c44743757c2eb8d17de0', '0'), ('10988', '1387582389', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '72326c5b44376ad6821ccd571eeb62e98717b01b', '0'), ('10989', '1387582389', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '0894e4eeff85bd2077aab63512d1eda15e80378c', '0'), ('10990', '1387582392', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '13125b8b1203d298a13e46f99bbde10a5617c20f', '1'), ('10991', '1387582392', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'afc237040bcbcabb6e7279c946c61900a5358898', '0'), ('10992', '1387582392', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'eb95743a89cd97985456726265cc21a857f12c54', '0'), ('10993', '1387582392', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '347f305caaa48c9319d493d4484c397461562c6f', '0'), ('10994', '1387582396', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'b118cd103ddf7841224f53dd23eebd6651976ba2', '0'), ('10995', '1387582396', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '610a59f4c71fff6b572384453063e2cada255541', '0'), ('10996', '1387582407', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '3b75b9b387fb4bfc33387727058174b6a2c917e8', '0'), ('10997', '1387582408', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'fd20466f06833e6ab5ab755457572e5c90fc124a', '0'), ('10998', '1387582408', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'd33e9c09ac4dffa1de6005d93764b1ed88c2d564', '0'), ('10999', '1387582408', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '76deca25bbf7bbf2c796b7d20878f14dc87d32c6', '0'), ('11000', '1387582418', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'a2649fccf5a2a2ea9bac4b1304c152b08f937224', '0'), ('11001', '1387582424', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '0c2e80e751444f63c629d93bfea65fdd8eb9658d', '1'), ('11002', '1387582431', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '211dd8d52a68fda8ba6392e6539c70f45bf4d572', '0'), ('11003', '1387582431', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'b713bc438060e8e3d97588c2e5f63884769491de', '0'), ('11004', '1387582432', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '15d0854062c48b1d612a78e821b5dd3d9ae33a61', '1'), ('11005', '1387582437', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '6882f6acd1581032bb9c9c9f5b080e9d62fedac1', '0'), ('11006', '1387582437', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'a6bba0d21cd9451e09a28cd68769125f94e02620', '0'), ('11007', '1387582440', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'b66185a36789b64f739aa84d0b0e1ff44defffef', '0'), ('11008', '1387582442', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '54190ca9ed0aa846a0728b6964e3b538f0590ff6', '1'), ('11009', '1387582442', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '16ad6c1622cf66d0b98a83988a0b989a19811018', '0'), ('11010', '1387582442', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'f3bf38e383262f7795540336e9603fa3b8be361b', '0'), ('11011', '1387582442', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'e99c5221526471d753e7ddb0cdfc42948e3ffcb9', '0'), ('11012', '1387582456', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '54f21e71b0eb2c397f1e7a56430c0f6c6bf2e095', '0'), ('11013', '1387582457', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'bd199ad88855db5656dc4b1bb4d9f480fc2310c7', '0'), ('11014', '1387582458', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'db79de9601a79027dd7ada1fdc8ae2e61bf058a6', '1'), ('11015', '1387582458', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '565d7fb56f26b14bc8c90f56ae4d40401bab36b9', '0'), ('11016', '1387582458', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '46e601f00f535a2fbf9a19f57c5d41659e667f6b', '0'), ('11017', '1387582458', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '6b9898d32d1f82cbbc92886bae4ec1f0617513a8', '0'), ('11018', '1387582473', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '90338adf3e2a36e05dbbf08f29296ccfe96db98d', '0'), ('11019', '1387582473', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'df3d3327e42cd6bf7f73a0931998a55dbd933e0e', '0'), ('11020', '1387582480', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'aab2fcaa9febe3dfe3752a937f06b6889d61443e', '0'), ('11021', '1387582480', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'b09806802d07942cf71666b01a34a91ad3b996ae', '0'), ('11022', '1387582481', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'b2c024e1e85a93e8173a48d2ba9165ec7edfc6c8', '0'), ('11023', '1387582481', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '4c65a529d475b5e02c190ee395cd71c62def6f9f', '0'), ('11024', '1387582502', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'e843e0ad2e681e8abdd989ae24e2802e60a903da', '0'), ('11025', '1387582505', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '888bf1a3aae7fb02838a04854baa3fca55497c38', '1'), ('11026', '1387582509', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '3f6425bf3e308d452193f8dc1e79de8acef1189f', '0'), ('11027', '1387582509', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '8d84a8de54d84612a2e1201d0f1e350ebdd93959', '0'), ('11028', '1387582513', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '8118b037b3a309af40cb86ea2bc1bcc69829a40a', '0'), ('11029', '1387582513', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '1ff6df5efc8256b5f3c1805be9a1c62be3361d95', '0'), ('11030', '1387582514', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '5fa697f3239f67300f24eb3056d082b4c746a1ac', '0'), ('11031', '1387582514', 'bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', 'fff18d93f2a3857e289db52449df0a6df2793e06', '0');

-- ----------------------------
--  Table structure for `exp_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_sessions`;
CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `fingerprint` varchar(40) NOT NULL,
  `sess_start` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_sessions`
-- ----------------------------
INSERT INTO `exp_sessions` VALUES ('bef4404c3bd467d33f6d88e891dd739c0ea9f4e4', '1', '1', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.3 Safari/537.36', '4fa21c575c21c34d35f0eb777cd82aef', '1387581828', '1387582514');

-- ----------------------------
--  Table structure for `exp_sites`
-- ----------------------------
DROP TABLE IF EXISTS `exp_sites`;
CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` text NOT NULL,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_sites`
-- ----------------------------
INSERT INTO `exp_sites` VALUES ('1', 'PreeBuilt', 'default_site', '', 'YTo4OTp7czoxMDoic2l0ZV9pbmRleCI7czoxOiIvIjtzOjg6InNpdGVfdXJsIjtzOjEzOiJodHRwOi8vaWRhaG8vIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjIwOiJodHRwOi8vaWRhaG8vdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjIxOiJicnlhbi5sZXdpc0BnbWFpbC5jb20iO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjI5OiJodHRwOi8vaWRhaG8vaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9wYXRoIjtzOjM4OiIvU2l0ZXMvaWRhaG8vaHR0cGRvY3MvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9mb250IjtzOjE6InkiO3M6MTI6ImNhcHRjaGFfcmFuZCI7czoxOiJ5IjtzOjIzOiJjYXB0Y2hhX3JlcXVpcmVfbWVtYmVycyI7czoxOiJuIjtzOjE3OiJlbmFibGVfZGJfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJlbmFibGVfc3FsX2NhY2hpbmciO3M6MToieSI7czoxODoiZm9yY2VfcXVlcnlfc3RyaW5nIjtzOjE6Im4iO3M6MTM6InNob3dfcHJvZmlsZXIiO3M6MToibiI7czoxODoidGVtcGxhdGVfZGVidWdnaW5nIjtzOjE6Im4iO3M6MTU6ImluY2x1ZGVfc2Vjb25kcyI7czoxOiJuIjtzOjEzOiJjb29raWVfZG9tYWluIjtzOjA6IiI7czoxMToiY29va2llX3BhdGgiO3M6MDoiIjtzOjE3OiJ1c2VyX3Nlc3Npb25fdHlwZSI7czoxOiJjIjtzOjE4OiJhZG1pbl9zZXNzaW9uX3R5cGUiO3M6MjoiY3MiO3M6MjE6ImFsbG93X3VzZXJuYW1lX2NoYW5nZSI7czoxOiJ5IjtzOjE4OiJhbGxvd19tdWx0aV9sb2dpbnMiO3M6MToieSI7czoxNjoicGFzc3dvcmRfbG9ja291dCI7czoxOiJ5IjtzOjI1OiJwYXNzd29yZF9sb2Nrb3V0X2ludGVydmFsIjtzOjE6IjEiO3M6MjA6InJlcXVpcmVfaXBfZm9yX2xvZ2luIjtzOjE6InkiO3M6MjI6InJlcXVpcmVfaXBfZm9yX3Bvc3RpbmciO3M6MToieSI7czoyNDoicmVxdWlyZV9zZWN1cmVfcGFzc3dvcmRzIjtzOjE6Im4iO3M6MTk6ImFsbG93X2RpY3Rpb25hcnlfcHciO3M6MToieSI7czoyMzoibmFtZV9vZl9kaWN0aW9uYXJ5X2ZpbGUiO3M6MDoiIjtzOjE3OiJ4c3NfY2xlYW5fdXBsb2FkcyI7czoxOiJ5IjtzOjE1OiJyZWRpcmVjdF9tZXRob2QiO3M6ODoicmVkaXJlY3QiO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxMzoic2VydmVyX29mZnNldCI7czowOiIiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czoxNToiQW1lcmljYS9DaGljYWdvIjtzOjEzOiJtYWlsX3Byb3RvY29sIjtzOjQ6Im1haWwiO3M6MTE6InNtdHBfc2VydmVyIjtzOjA6IiI7czoxMzoic210cF91c2VybmFtZSI7czowOiIiO3M6MTM6InNtdHBfcGFzc3dvcmQiO3M6MDoiIjtzOjExOiJlbWFpbF9kZWJ1ZyI7czoxOiJuIjtzOjEzOiJlbWFpbF9jaGFyc2V0IjtzOjU6InV0Zi04IjtzOjE1OiJlbWFpbF9iYXRjaG1vZGUiO3M6MToibiI7czoxNjoiZW1haWxfYmF0Y2hfc2l6ZSI7czowOiIiO3M6MTE6Im1haWxfZm9ybWF0IjtzOjU6InBsYWluIjtzOjk6IndvcmRfd3JhcCI7czoxOiJ5IjtzOjIyOiJlbWFpbF9jb25zb2xlX3RpbWVsb2NrIjtzOjE6IjUiO3M6MjI6ImxvZ19lbWFpbF9jb25zb2xlX21zZ3MiO3M6MToieSI7czo4OiJjcF90aGVtZSI7czo5OiJjb3Jwb3JhdGUiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6Mjg6Imh0dHA6Ly9pZGFoby9pbWFnZXMvc21pbGV5cy8iO3M6MTk6InJlY291bnRfYmF0Y2hfdG90YWwiO3M6NDoiMTAwMCI7czoxNzoibmV3X3ZlcnNpb25fY2hlY2siO3M6MToieSI7czoxNzoiZW5hYmxlX3Rocm90dGxpbmciO3M6MToibiI7czoxNzoiYmFuaXNoX21hc2tlZF9pcHMiO3M6MToieSI7czoxNDoibWF4X3BhZ2VfbG9hZHMiO3M6MjoiMTAiO3M6MTM6InRpbWVfaW50ZXJ2YWwiO3M6MToiOCI7czoxMjoibG9ja291dF90aW1lIjtzOjI6IjMwIjtzOjE1OiJiYW5pc2htZW50X3R5cGUiO3M6NzoibWVzc2FnZSI7czoxNDoiYmFuaXNobWVudF91cmwiO3M6MDoiIjtzOjE4OiJiYW5pc2htZW50X21lc3NhZ2UiO3M6NTA6IllvdSBoYXZlIGV4Y2VlZGVkIHRoZSBhbGxvd2VkIHBhZ2UgbG9hZCBmcmVxdWVuY3kuIjtzOjE3OiJlbmFibGVfc2VhcmNoX2xvZyI7czoxOiJ5IjtzOjE5OiJtYXhfbG9nZ2VkX3NlYXJjaGVzIjtzOjM6IjUwMCI7czoxNzoidGhlbWVfZm9sZGVyX3BhdGgiO3M6Mjk6Ii9TaXRlcy9pZGFoby9odHRwZG9jcy90aGVtZXMvIjtzOjEwOiJpc19zaXRlX29uIjtzOjE6InkiO3M6MTE6InJ0ZV9lbmFibGVkIjtzOjE6InkiO3M6MjI6InJ0ZV9kZWZhdWx0X3Rvb2xzZXRfaWQiO3M6MToiMSI7czo2OiJjcF91cmwiO3M6Mjk6Imh0dHA6Ly9pZGFoby9zeXN0ZW0vaW5kZXgucGhwIjt9', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6Mjg6Imh0dHA6Ly9pZGFoby9pbWFnZXMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjM3OiIvU2l0ZXMvaWRhaG8vaHR0cGRvY3MvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjM0OiJodHRwOi8vaWRhaG8vaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjEwOiJwaG90b19wYXRoIjtzOjQzOiIvU2l0ZXMvaWRhaG8vaHR0cGRvY3MvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NDI6Imh0dHA6Ly9pZGFoby9pbWFnZXMvc2lnbmF0dXJlX2F0dGFjaG1lbnRzLyI7czoxMjoic2lnX2ltZ19wYXRoIjtzOjUxOiIvU2l0ZXMvaWRhaG8vaHR0cGRvY3MvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo0NDoiL1NpdGVzL2lkYWhvL2h0dHBkb2NzL2ltYWdlcy9wbV9hdHRhY2htZW50cy8iO3M6MjM6InBydl9tc2dfbWF4X2F0dGFjaG1lbnRzIjtzOjE6IjMiO3M6MjI6InBydl9tc2dfYXR0YWNoX21heHNpemUiO3M6MzoiMjUwIjtzOjIwOiJwcnZfbXNnX2F0dGFjaF90b3RhbCI7czozOiIxMDAiO3M6MTk6InBydl9tc2dfaHRtbF9mb3JtYXQiO3M6NDoic2FmZSI7czoxODoicHJ2X21zZ19hdXRvX2xpbmtzIjtzOjE6InkiO3M6MTc6InBydl9tc2dfbWF4X2NoYXJzIjtzOjQ6IjYwMDAiO3M6MTk6Im1lbWJlcmxpc3Rfb3JkZXJfYnkiO3M6MTE6InRvdGFsX3Bvc3RzIjtzOjIxOiJtZW1iZXJsaXN0X3NvcnRfb3JkZXIiO3M6NDoiZGVzYyI7czoyMDoibWVtYmVybGlzdF9yb3dfbGltaXQiO3M6MjoiMjAiO30=', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJ5IjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czoyMzoiL1NpdGVzL2lkYWhvL3RlbXBsYXRlcy8iO30=', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJ5IjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjU6InRvcGljIjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjMxOiIvU2l0ZXMvaWRhaG8vaHR0cGRvY3MvaW5kZXgucGhwIjtzOjMyOiI4MDVkMmNhOWZlNzU5YWY3NWY2NzAzNDU4MTYwMjhjOSI7czozODoiL1NpdGVzL2Rhci1lcy1iYWxhdC9odHRwZG9jcy9pbmRleC5waHAiO3M6MzI6IjgwNWQyY2E5ZmU3NTlhZjc1ZjY3MDM0NTgxNjAyOGM5Ijt9', 'YToxOntpOjE7YTozOntzOjQ6InVyaXMiO2E6Njp7aToxMTtzOjY6Ii9hYm91dCI7aTozMTtzOjE1OiIvcHJpdmFjeS1wb2xpY3kiO2k6MzI7czoyMToiL3Rlcm1zLWFuZC1jb25kaXRpb25zIjtpOjQxO3M6MTA6Ii9hYm91dC9mYXEiO2k6NTI7czo4OiIvY29udGFjdCI7aTo2MjtzOjE5OiIvYWJvdXQvZmFxL25lc3RlZC0xIjt9czo5OiJ0ZW1wbGF0ZXMiO2E6Njp7aToxMTtzOjI6IjEwIjtpOjMxO3M6MjoiMTAiO2k6MzI7czoyOiIxMCI7aTo0MTtzOjI6IjEwIjtpOjUyO3M6MjoiNDIiO2k6NjI7czoyOiIxMCI7fXM6MzoidXJsIjtzOjEzOiJodHRwOi8vaWRhaG8vIjt9fQ==');

-- ----------------------------
--  Table structure for `exp_snippets`
-- ----------------------------
DROP TABLE IF EXISTS `exp_snippets`;
CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_specialty_templates`
-- ----------------------------
DROP TABLE IF EXISTS `exp_specialty_templates`;
CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_specialty_templates`
-- ----------------------------
INSERT INTO `exp_specialty_templates` VALUES ('1', '1', 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'), ('2', '1', 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'), ('3', '1', 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'), ('4', '1', 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'), ('5', '1', 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'), ('6', '1', 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'), ('7', '1', 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'), ('8', '1', 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'), ('9', '1', 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'), ('10', '1', 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'), ('11', '1', 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'), ('12', '1', 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'), ('13', '1', 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'), ('14', '1', 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'), ('15', '1', 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

-- ----------------------------
--  Table structure for `exp_stash`
-- ----------------------------
DROP TABLE IF EXISTS `exp_stash`;
CREATE TABLE `exp_stash` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `session_id` varchar(40) DEFAULT NULL,
  `bundle_id` int(11) unsigned NOT NULL DEFAULT '1',
  `key_name` varchar(255) NOT NULL,
  `key_label` varchar(255) DEFAULT NULL,
  `created` int(10) unsigned NOT NULL,
  `expire` int(10) unsigned NOT NULL DEFAULT '0',
  `parameters` mediumtext,
  PRIMARY KEY (`id`),
  KEY `bundle_id` (`bundle_id`),
  KEY `key_session` (`key_name`,`session_id`),
  KEY `key_name` (`key_name`),
  KEY `site_id` (`site_id`),
  CONSTRAINT `exp_stash_fk` FOREIGN KEY (`bundle_id`) REFERENCES `exp_stash_bundles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_stash_bundles`
-- ----------------------------
DROP TABLE IF EXISTS `exp_stash_bundles`;
CREATE TABLE `exp_stash_bundles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bundle_name` varchar(255) NOT NULL,
  `bundle_label` varchar(255) DEFAULT NULL,
  `is_locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `bundle` (`bundle_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_stash_bundles`
-- ----------------------------
INSERT INTO `exp_stash_bundles` VALUES ('1', 'default', 'Default', '1'), ('2', 'templates', 'Templates', '1'), ('3', 'static', 'Static', '1');

-- ----------------------------
--  Table structure for `exp_stats`
-- ----------------------------
DROP TABLE IF EXISTS `exp_stats`;
CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_stats`
-- ----------------------------
INSERT INTO `exp_stats` VALUES ('1', '1', '1', '1', 'Bryan Lewis', '31', '0', '0', '9', '1383343740', '0', '1383273044', '1387581820', '8', '1384295076', '1388006457');

-- ----------------------------
--  Table structure for `exp_status_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_status_groups`;
CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_status_groups`
-- ----------------------------
INSERT INTO `exp_status_groups` VALUES ('1', '1', 'Statuses');

-- ----------------------------
--  Table structure for `exp_status_no_access`
-- ----------------------------
DROP TABLE IF EXISTS `exp_status_no_access`;
CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `exp_statuses`;
CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_statuses`
-- ----------------------------
INSERT INTO `exp_statuses` VALUES ('1', '1', '1', 'open', '1', '48BFFA'), ('2', '1', '1', 'closed', '2', 'F5C6D0'), ('3', '1', '1', 'Draft', '3', 'F7C539'), ('4', '1', '1', 'Public', '4', '009933');

-- ----------------------------
--  Table structure for `exp_tagger`
-- ----------------------------
DROP TABLE IF EXISTS `exp_tagger`;
CREATE TABLE `exp_tagger` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) DEFAULT NULL,
  `site_id` tinyint(3) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '1',
  `entry_date` int(10) unsigned DEFAULT '1',
  `edit_date` int(10) unsigned DEFAULT '1',
  `hits` int(10) unsigned DEFAULT '0',
  `total_entries` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`tag_id`),
  KEY `tag_name` (`tag_name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_tagger`
-- ----------------------------
INSERT INTO `exp_tagger` VALUES ('1', 'design', '1', '1', '1387413639', '1387413639', '0', '1'), ('2', 'ideas', '1', '1', '1387413639', '1387413639', '0', '1'), ('3', 'html', '1', '1', '1387413850', '1387413850', '0', '1'), ('4', 'resource', '1', '1', '1387413850', '1387413850', '0', '1');

-- ----------------------------
--  Table structure for `exp_tagger_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_tagger_groups`;
CREATE TABLE `exp_tagger_groups` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_title` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `group_desc` varchar(255) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT '0',
  `site_id` tinyint(3) unsigned DEFAULT '1',
  `order` mediumint(8) unsigned DEFAULT '1',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_tagger_groups`
-- ----------------------------
INSERT INTO `exp_tagger_groups` VALUES ('1', 'Resources', 'resources', '', '0', '1', '1');

-- ----------------------------
--  Table structure for `exp_tagger_groups_entries`
-- ----------------------------
DROP TABLE IF EXISTS `exp_tagger_groups_entries`;
CREATE TABLE `exp_tagger_groups_entries` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned DEFAULT '0',
  `group_id` int(10) unsigned DEFAULT '0',
  `order` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`rel_id`),
  KEY `group_id` (`group_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_tagger_links`
-- ----------------------------
DROP TABLE IF EXISTS `exp_tagger_links`;
CREATE TABLE `exp_tagger_links` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` tinyint(3) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` smallint(5) unsigned DEFAULT '0',
  `field_id` mediumint(8) unsigned DEFAULT '0',
  `tag_id` int(10) unsigned DEFAULT '0',
  `author_id` int(10) unsigned DEFAULT '1',
  `type` smallint(5) unsigned DEFAULT '1',
  `tag_order` smallint(5) unsigned DEFAULT '0',
  PRIMARY KEY (`rel_id`),
  KEY `tag_id` (`tag_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_tagger_links`
-- ----------------------------
INSERT INTO `exp_tagger_links` VALUES ('1', '1', '1', '4', '8', '1', '1', '1', '1'), ('2', '1', '1', '4', '8', '2', '1', '1', '2'), ('3', '1', '2', '4', '8', '3', '1', '1', '1'), ('4', '1', '2', '4', '8', '4', '1', '1', '2');

-- ----------------------------
--  Table structure for `exp_taxonomy_tree_1`
-- ----------------------------
DROP TABLE IF EXISTS `exp_taxonomy_tree_1`;
CREATE TABLE `exp_taxonomy_tree_1` (
  `node_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `lft` mediumint(8) unsigned DEFAULT NULL,
  `rgt` mediumint(8) unsigned DEFAULT NULL,
  `depth` mediumint(8) unsigned DEFAULT NULL,
  `parent` mediumint(8) unsigned DEFAULT NULL,
  `moved` tinyint(1) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `entry_id` int(10) DEFAULT NULL,
  `template_path` varchar(255) DEFAULT NULL,
  `custom_url` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `field_data` text,
  PRIMARY KEY (`node_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_taxonomy_tree_1`
-- ----------------------------
INSERT INTO `exp_taxonomy_tree_1` VALUES ('1', '1', '28', '0', '0', '0', 'Home', '0', '--', '/', 'custom', ''), ('22', '23', '24', '2', '10', '0', 'Standard', '0', '27', '', 'template', null), ('3', '14', '19', '1', '1', '0', 'Videos', '0', '22', '/videos', 'custom', ''), ('4', '12', '13', '1', '1', '0', 'Blog', '0', '11', '', 'template', ''), ('10', '20', '27', '1', '1', '0', 'Portfolio', '0', '27', '/portfolio', 'custom', ''), ('23', '25', '26', '2', '10', '0', 'By Topic', '0', '--', '/portfolio/topics', 'custom', null), ('17', '3', '4', '2', '19', '0', 'Our story', '11', '--', '', 'entry', ''), ('18', '5', '8', '2', '19', '0', 'FAQ', '41', '10', '', 'entry|template', null), ('19', '2', '11', '1', '1', '0', 'About', '0', '--', '/about', 'custom', ''), ('21', '21', '22', '2', '10', '0', 'Filtered Portfolio', '0', '--', '/portfolio/filtered/', 'custom', null), ('24', '15', '16', '2', '3', '0', 'Overview', '0', '22', '', 'template', ''), ('25', '17', '18', '2', '3', '0', 'Filtered', '0', '40', '', 'template', ''), ('26', '9', '10', '2', '19', '0', 'Contact Us', '52', '42', '', 'entry|template', ''), ('29', '6', '7', '3', '18', '0', 'Nested 1', '62', '10', '', 'template|entry', '');

-- ----------------------------
--  Table structure for `exp_taxonomy_tree_3`
-- ----------------------------
DROP TABLE IF EXISTS `exp_taxonomy_tree_3`;
CREATE TABLE `exp_taxonomy_tree_3` (
  `node_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `lft` mediumint(8) unsigned DEFAULT NULL,
  `rgt` mediumint(8) unsigned DEFAULT NULL,
  `depth` mediumint(8) unsigned DEFAULT NULL,
  `parent` mediumint(8) unsigned DEFAULT NULL,
  `moved` tinyint(1) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `entry_id` int(10) DEFAULT NULL,
  `template_path` varchar(255) DEFAULT NULL,
  `custom_url` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `field_data` text,
  PRIMARY KEY (`node_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_taxonomy_tree_3`
-- ----------------------------
INSERT INTO `exp_taxonomy_tree_3` VALUES ('1', '1', '6', '0', null, '0', 'Footer Nav', '0', '--', '/', 'custom', ''), ('2', '2', '3', '1', '1', '0', 'Privacy Policy', '31', '--', '', 'entry', null), ('3', '4', '5', '1', '1', '0', 'Terms & Conditions', '32', '--', '', 'entry', null);

-- ----------------------------
--  Table structure for `exp_taxonomy_trees`
-- ----------------------------
DROP TABLE IF EXISTS `exp_taxonomy_trees`;
CREATE TABLE `exp_taxonomy_trees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `label` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `templates` varchar(250) DEFAULT 'all',
  `channels` varchar(250) DEFAULT 'all',
  `member_groups` varchar(250) DEFAULT NULL,
  `last_updated` int(10) DEFAULT NULL,
  `fields` text,
  `taxonomy` longtext,
  `max_depth` int(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_taxonomy_trees`
-- ----------------------------
INSERT INTO `exp_taxonomy_trees` VALUES ('1', '1', 'Global Nav', 'nav-global', '11|14|33|42|10|39|27|28|37|12|13|3|40|22|24|23', '2|5|6|1', '', '1383361563', '[]', '[{\"id\":\"1\",\"level\":0,\"depth\":\"0\",\"children\":[{\"id\":\"19\",\"level\":1,\"depth\":\"1\",\"children\":[{\"id\":\"17\",\"level\":2,\"depth\":\"2\"},{\"id\":\"18\",\"level\":2,\"depth\":\"2\",\"children\":[{\"id\":\"29\",\"level\":3,\"depth\":\"3\"}]},{\"id\":\"26\",\"level\":2,\"depth\":\"2\"}]},{\"id\":\"4\",\"level\":1,\"depth\":\"1\"},{\"id\":\"3\",\"level\":1,\"depth\":\"1\",\"children\":[{\"id\":\"24\",\"level\":2,\"depth\":\"2\"},{\"id\":\"25\",\"level\":2,\"depth\":\"2\"}]},{\"id\":\"10\",\"level\":1,\"depth\":\"1\",\"children\":[{\"id\":\"21\",\"level\":2,\"depth\":\"2\"},{\"id\":\"22\",\"level\":2,\"depth\":\"2\"},{\"id\":\"23\",\"level\":2,\"depth\":\"2\"}]}]}]', '3'), ('3', '1', 'Footer Nav', 'footer_nav', '11|14|33|10|27|28|12|13|22|24|23', '2|5|6|1', '', '1383315442', '[]', '[{\"id\":\"1\",\"level\":0,\"depth\":\"0\",\"children\":[{\"id\":\"2\",\"level\":1,\"depth\":\"1\"},{\"id\":\"3\",\"level\":1,\"depth\":\"1\"}]}]', '1');

-- ----------------------------
--  Table structure for `exp_template_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_template_groups`;
CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`),
  KEY `group_name_idx` (`group_name`),
  KEY `group_order_idx` (`group_order`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_template_groups`
-- ----------------------------
INSERT INTO `exp_template_groups` VALUES ('3', '1', 'sitemap', '1', 'n'), ('14', '1', 'portfolio', '8', 'n'), ('5', '1', 'widgets', '3', 'n'), ('9', '1', 'pages', '6', 'n'), ('7', '1', '_wrappers', '5', 'n'), ('15', '1', 'manage', '9', 'n'), ('10', '1', 'blog', '7', 'n'), ('11', '1', 'site', '8', 'y'), ('13', '1', 'videos', '7', 'n');

-- ----------------------------
--  Table structure for `exp_template_member_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_template_member_groups`;
CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_template_no_access`
-- ----------------------------
DROP TABLE IF EXISTS `exp_template_no_access`;
CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_templates`
-- ----------------------------
DROP TABLE IF EXISTS `exp_templates`;
CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_templates`
-- ----------------------------
INSERT INTO `exp_templates` VALUES ('3', '1', '3', 'index', 'n', 'webpage', '', null, '1382654973', '0', 'n', '0', '', 'n', 'n', 'o', '0'), ('6', '1', '5', 'index', 'y', 'webpage', '', null, '1382722457', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('10', '1', '9', 'index', 'y', 'webpage', '{exp:stash:set parse_tags=\"yes\"}\n\n	\n	{exp:channel:entries\n		channel=\"pages\"\n		status=\"public|open\"			\n	}\n\n		{if no_results}{redirect=\"404\"}{/if}		\n		\n		{stash:chunk_metadescription}{cf-post_metadescription}{/stash:chunk_metadescription}\n		\n		{stash:chunk_metatitle}{cf-post_metatitle} | {gv-global-metatitle}{/stash:chunk_metatitle}\n\n		{stash:chunk_global-nav}\n			{snippet-global-nav-topbar}\n		{/stash:chunk_global-nav}\n\n		{stash:chunk_local-nav}\n			{snippet-local-nav-nested}	\n		{/stash:chunk_local-nav}\n		\n		{stash:chunk_breadcrumbs}\n			{snippet-breadcrumbs}\n		{/stash:chunk_breadcrumbs}\n\n		{stash:chunk_main-body}		\n\n			{!-- // Display featured image --}\n			{if cf-post_display-image ==\"As banner\"}<img src=\"{cf-post_featured-image:banner}\">{/if}\n\n                        {!-- // Display post content --}\n			<h1>{title}</h1>\n			{snippet-post-featured-image}\n			{cf-post_body}			  	\n\n  		{/stash:chunk_main-body}\n\n  		{stash:chunk_sidebar}\n  		\n  			{cf-post_sidebar disable=\"category_fields|member_data|pagination\"}  \n  				{snippet-single-widget}\n  			{/cf-post_sidebar}\n  		\n  		{/stash:chunk_sidebar}\n\n\n	{/exp:channel:entries}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/sub-left\"}', '', '1383177698', '1', 'n', '0', '', 'n', 'n', 'o', '1124'), ('8', '1', '7', 'index', 'y', 'webpage', '<!DOCTYPE html>\n<!--[if IE 8]><html class=\"no-js lt-ie9\" lang=\"en\"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\" lang=\"en\"> <!--<![endif]-->\n{snippet-head}\n\n<body>\n\n  <div class=\"row header\">\n    {exp:stash:get name=\"block_global-nav\"}      \n    {!--{exp:stash:get name=\"block_breadcrumbs\"}--}\n  </div>\n  \n  <div class=\"row\">        \n    <div class=\"small-12 columns\">     \n      {exp:stash:get name=\"block_main-body\"}\n    </div>    \n  </div>    \n			{snippet-footer}\n\n{snippet-javascript}\n\n</body>\n</html>\n', '', '1383316106', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('11', '1', '10', 'index', 'y', 'webpage', '{exp:stash:set parse_tags=\"yes\"}\n\n	{stash:block_global-nav}\n		{snippet-header-topbar}\n	{/stash:block_global-nav}\n\n	{stash:block_metadescription}{gv-blog-metadescription}{/stash:block_metadescription}\n\n	{stash:block_metatitle}{gv-blog-metatitle}{triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag} | {gv-global-metatitle}{/stash:block_metatitle}\n		\n	{stash:block_main-body}\n\n\n		{!--// Blog Archive Title --}\n		<h1>\n			{gv-blog-title} \n			<small>{triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag}</small>\n		</h1>\n		\n		<article>\n\n			{exp:channel:entries\n				channel=\"blog\"\n				status=\"public\"\n				disable=\"{var-disable}\"\n				limit=\"10\"\n				{var-sort-blog}\n				dynamic=\"off\"\n				paginate=\"bottom\"\n				paginate_base=\"/blog/\"\n				{triggers:entries}\n			}\n				\n				{if no_results}<li>Sorry, no posts match that filter.</li>{/if}\n		\n					\n					<div class=\"row\">\n						{if cf-post_featured-type != \"\"}\n							<div class=\"small-6 columns\">\n						{if:else}\n							<div class=\"small-12 column\">\n						{/if}\n\n									{!-- // Blog Archive List Item --}\n							       <h3>\n\n								       	<a href=\"/blog/post/{url_title}/\">{title}</a>\n								       \n								       	<small>Filed in: {categories}{category_name}{/categories}</small>\n\n							       </h3>\n							       \n							       <h6>Written by <a href=\"#\">{author}</a> on {entry_date format=\"{var-df-date}\"}</h6>\n									\n\n								<p class=\"summary\">\n									{exp:eehive_hacksaw					\n									words = \"{gv-words-in-summary}\"\n									append=\"...\"\n									}\n\n									{cf-post_body}		\n\n							 		{/exp:eehive_hacksaw}\n					 			</p>\n		     				</div>\n\n		     				{if cf-post_featured-type != \"\"}\n		     					<div class=\"small-6 columns\">\n		     						<a class=\"th\" href=\"/blog/post/{url_title}\"><img src=\"{cf-post_featured-image:medium}\"></a>\n		     					</div>\n		     				{/if}\n	     			</div>\n	     		\n		     		{snippet-paginate}\n			{/exp:channel:entries}\n		</article>\n\n\n\n\n\n	{/stash:block_main-body}\n	\n	{stash:block_sidebar}\n		{snippet-sidebar}			\n	{/stash:block_sidebar}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/sub-right\"}', '', '1383316212', '1', 'n', '0', '', 'n', 'n', 'o', '496'), ('13', '1', '11', 'index', 'y', 'webpage', '<!-- // START videos/post -->\n\n{exp:stash:set parse_tags=\"yes\"}\n		\n		{stash:block_global-nav}\n			{snippet-header-topbar}\n		{/stash:block_global-nav}\n		\n		{stash:block_metadescription}{cf-global_metadescription}{/stash:block_metadescription}\n\n		{stash:block_metatitle}{gv-global-metatitle}{/stash:block_metatitle}\n			\n		{stash:block_main-body}\n\n			<!--carousel-->\n			<div class=\"row hide-for-small\">			\n				{exp:low_variables:pair var=\"gv-home-1\"}  \n						{snippet-single-widget}\n				{/exp:low_variables:pair}\n			</div>\n\n			<!--mobile header-->\n			<div class=\"row show-for-small\">\n			  <div class=\"small-12\"><br>\n			    <img src=\"http://placehold.it/1000x600&text=For Small Screens\" />\n			  </div>\n			</div>\n\n			<div class=\"row\">\n			  <div class=\"large-12 columns\">\n			  <h3>Recent items</h3>\n			    <div class=\"row\">\n\n			  <!-- Recent Thumbnails -->\n			  	{exp:channel:entries\n			  		channel=\"blog|videos|portfolio\"\n			  		disable=\"categories|category_fields|member_data|pagination|trackbacks\"\n			  		dynamic=\"off\"\n			  		require_entry=\"yes\"\n			  		category=\"1\"\n			  		limit=\"4\"\n			  		status=\"public\"\n			  		{var-sort-blog}\n			  	}\n				  	<div class=\"large-3 small-6 columns\">\n				        <a href=\"/{channel_short_name}/post/{url_title}/\"><img src=\"{cf-post_featured-image:medium}\"></a>\n				        <h6 class=\"panel\"><a href=\"/{channel_short_name}/post/{url_title}/\">{title}</a></h6>\n			      </div>	\n			  		\n			  	{/exp:channel:entries}\n			     \n			  <!-- End Thumbnails -->\n\n			    </div>\n			  </div>\n			</div>\n\n			<!--home-2 and home-3 and home-4 -->\n			<div class=\"row\">\n				<div class=\"large-12 columns\">\n					<div class=\"row\">\n\n						<!-- Content -->\n\n						<div class=\"large-8 columns\">\n							<div class=\"panel radius\">\n\n								<div class=\"row\">\n									<div class=\"large-6 small-6 columns\">\n										{exp:low_variables:pair var=\"gv-home-2\"}  \n												{snippet-single-widget}\n										{/exp:low_variables:pair}\n									</div>\n									<div class=\"large-6 small-6 columns\">\n										{exp:low_variables:pair var=\"gv-home-3\"}  \n												{snippet-single-widget}\n										{/exp:low_variables:pair}\n									</div>\n\n								</div>\n							</div>\n						</div>\n\n						<div class=\"large-4 columns hide-for-small\">\n							{exp:low_variables:pair var=\"gv-home-4\"}  \n									{snippet-single-widget}\n							{/exp:low_variables:pair}\n						</div>\n\n						<!-- End Content -->\n\n					</div>\n				</div>\n			</div>\n			\n\n  		{/stash:block_main-body}\n\n		{stash:block_sidebar}\n			\n\n		{/stash:block_sidebar}\n	\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/index\"}\n<!-- // END site/index -->\n', '', '1383316097', '1', 'n', '0', '', 'n', 'n', 'o', '245'), ('14', '1', '10', 'post', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:chunk_global-nav}\n		{snippet-global-nav-flat}\n	{/stash:chunk_global-nav}\n\n	{stash:chunk_main-body}\n		{exp:channel:entries\n			channel=\"blog|videos\"\n			status=\"public\"\n			disable=\"{var-disable}\"\n			limit=\"1\"\n			require_entry=\"yes\"\n			dynamic=\"on\"\n		}\n			{if no_results}{redirect=\"404\"}{/if}\n			\n			<h1>{title}<br><small>{entry_date format=\"{var-df-date}\"}</small></h1>\n			<p>Filed in: {categories}{category_name}{/categories}</p>\n			\n			<dd>{cf-post_body}</dd>\n			\n\n		{/exp:channel:entries}\n	{/stash:chunk_main-body}\n	\n	{stash:chunk_sidebar}\n		{snippet-local-nav-nested}			\n	{/stash:chunk_sidebar}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/index\"}', null, '1382806068', '1', 'n', '0', '', 'n', 'n', 'o', '224'), ('22', '1', '13', 'index', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:chunk_global-nav}\n		{snippet-global-nav-flat}\n	{/stash:chunk_global-nav}\n\n	{stash:chunk_metadescription}{gv-blog-metadescription}{/stash:chunk_metadescription}\n	{stash:chunk_metatitle}{gv-blog-metatitle}{triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag} | {gv-global-metatitle}{/stash:chunk_metatitle}\n		\n	{stash:chunk_main-body}\n\n		<h1>Blog {triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag}</h1>\n	\n		<dl>\n			{exp:channel:entries\n				channel=\"blog|videos\"\n				status=\"public\"\n				disable=\"{var-disable}\"\n				limit=\"10\"\n				{var-sort-blog}\n				dynamic=\"off\"\n				paginate=\"bottom\"\n				paginate_base=\"/starters/\"\n				{triggers:entries}\n			}\n				\n				{if no_results}<li>Sorry, no posts match that filter.</li>{/if}\n\n				<dt>{entry_date format=\"{var-df-date}\"}<br><a href=\"/blog/post/{url_title}/\">{title}</a><br>\n				Filed in: {categories}{category_name}{/categories}</dt>\n				<dd>{cf-post_body}</dd>\n				 \n\n				 {paginate}\n			        <p>Page {current_page} of {total_pages} pages {pagination_links}</p>\n			    {/paginate}\n\n			{/exp:channel:entries}\n		</dl>\n\n\n	{/stash:chunk_main-body}\n	\n	{stash:chunk_sidebar}\n		{snippet-sidebar}			\n	{/stash:chunk_sidebar}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/index\"}', null, '1382825534', '1', 'n', '0', '', 'n', 'n', 'o', '385'), ('19', '1', '5', 'nav-local', 'y', 'webpage', '{exp:taxonomy:nav \n    tree_id=\"1\" \n    depth=\"2\"\n    entry_id=\"{entry_id}\"\n    display_root=\"no\"\n    auto_expand=\"yes\"\n    active_branch_start_level=\"1\"\n    ul_css_id=\"nav-sub\"\n    ul_css_class=\"side-nav\"\n \n}\n	<li class=\"{node_active}\"><a href=\"{node_url}\">{node_title}</a>{children}</li>\n{/exp:taxonomy:nav}  ', null, '1382809517', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('23', '1', '13', 'post', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:chunk_global-nav}\n		{snippet-global-nav-flat}\n	{/stash:chunk_global-nav}\n\n	{stash:chunk_main-body}\n		{exp:channel:entries\n			channel=\"blog|videos\"\n			status=\"public\"\n			disable=\"{var-disable}\"\n			limit=\"1\"\n			require_entry=\"yes\"\n			dynamic=\"yes\"\n		}\n			{if no_results}{redirect=\"404\"}{/if}\n\n			{stash:chunk_metadescription}{cf-post_metadescription}{/stash:chunk_metadescription}\n\n			{stash:chunk_metatitle}{cf-post_metatitle} | {gv-global-metatitle}{/stash:chunk_metatitle}\n		\n			<h1>{title}<br><small>{entry_date format=\"{var-df-date}\"}</small></h1>\n			<p>Filed in: {categories}{category_name}{/categories}</p>\n			\n			<dd>{cf-post_body}</dd>\n			\n			<h3>Discussion</h3>\n			{snippet-comments}\n			{snippet-comments-form}\n\n		{/exp:channel:entries}\n	{/stash:chunk_main-body}\n\n	{stash:chunk_sidebar}\n		{snippet-sidebar}			\n	{/stash:chunk_sidebar}	\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/index\"}', null, '1382825534', '1', 'n', '0', '', 'n', 'n', 'o', '57'), ('38', '1', '7', 'sub-right', 'y', 'webpage', '<!DOCTYPE html>\n<!--[if IE 8]><html class=\"no-js lt-ie9\" lang=\"en\"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\" lang=\"en\"> <!--<![endif]-->\n\n{snippet-head}\n\n<body>\n \n  <div class=\"row header\">\n    {exp:stash:get name=\"block_global-nav\"}      \n    {!--{exp:stash:get name=\"block_breadcrumbs\"}--}\n  </div>\n  \n  <div class=\"row\">    \n    \n    {!--// MAIN CONTENT AREA --}\n    <!-- This has been source ordered to come first in the markup (and on small devices) but to be to the right of the nav on larger screens -->\n    <div class=\"large-9 columns\">     \n      {exp:stash:get name=\"block_main-body\"}\n    </div>\n    \n    {!--// SIDEBAR AREA --}\n    <!-- This is source ordered to be pulled to the left on larger screens -->\n    <div class=\"large-3 columns\">\n\n      {!-- display local nav if set --}\n      {exp:stash:get name=\"block_local-nav\"}\n\n      {!-- output sidebar widgets --}\n      {exp:stash:get name=\"block_sidebar\"}  \n        \n    </div>\n    \n  </div>\n    \n  {snippet-footer}\n  \n  {snippet-javascript}\n\n  </body>\n</html>\n', null, '1383255515', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('24', '1', '13', 'listing', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:chunk_global-nav}\n		{snippet-global-nav-flat}\n	{/stash:chunk_global-nav}\n\n	{stash:chunk_metadescription}{triggers:cat_description_1}{/stash:chunk_metadescription}\n	{stash:chunk_metatitle}{gv-blog-metatitle}{triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag} | {gv-global-metatitle}{/stash:chunk_metatitle}\n		\n	{stash:chunk_main-body}\n\n		<h1>Videos {triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag}</h1>\n	\n		<dl>\n			{exp:channel:entries\n				channel=\"videos\"\n				status=\"public\"\n				disable=\"{var-disable}\"\n				limit=\"10\"\n				{var-sort-blog}\n				dynamic=\"off\"\n				paginate=\"bottom\"\n				paginate_base=\"/blog/\"\n				{triggers:entries}\n			}\n				\n				{if no_results}<li>Sorry, no posts match that filter.</li>{/if}\n\n				<dt>{entry_date format=\"{var-df-date}\"}<br><a href=\"/blog/post/{url_title}/\">{title}</a><br>\n				Filed in: {categories}{category_name}{/categories}</dt>\n				<dd>{cf-post_body}</dd>\n				 \n\n				 {paginate}\n			        <p>Page {current_page} of {total_pages} pages {pagination_links}</p>\n			    {/paginate}\n\n			{/exp:channel:entries}\n		</dl>\n\n\n	{/stash:chunk_main-body}\n	\n	{stash:chunk_sidebar}\n		{snippet-sidebar}			\n	{/stash:chunk_sidebar}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/index\"}', null, '1382890882', '1', 'n', '0', '', 'n', 'n', 'o', '88'), ('27', '1', '14', 'index', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:chunk_global-nav}\n		{snippet-global-nav-flat}\n	{/stash:chunk_global-nav}\n\n	{stash:chunk_metadescription}{gv-blog-metadescription}{/stash:chunk_metadescription}\n	{stash:chunk_metatitle}{gv-blog-metatitle}{triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag} | {gv-global-metatitle}{/stash:chunk_metatitle}\n		\n	{stash:chunk_main-body}\n\n		<h1>Blog {triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag}</h1>\n	\n		<dl>\n			{exp:channel:entries\n				channel=\"portfolio\"\n				status=\"public\"\n				disable=\"{var-disable}\"\n				limit=\"10\"\n				{var-sort-blog}\n				dynamic=\"off\"\n				paginate=\"bottom\"\n				paginate_base=\"/blog/\"\n				{triggers:entries}\n			}\n				\n				{if no_results}<li>Sorry, no posts match that filter.</li>{/if}\n\n				<dt>{entry_date format=\"{var-df-date}\"}<br><a href=\"/portfolio/post/{url_title}/\">{title}</a><br>\n				Filed in: {categories}{category_name}{/categories}</dt>\n				<dd><img src=\"{cf-post_feature-image:thumb}\"></dd>\n				 \n\n				 {paginate}\n			        <p>Page {current_page} of {total_pages} pages {pagination_links}</p>\n			    {/paginate}\n\n			{/exp:channel:entries}\n		</dl>\n\n\n	{/stash:chunk_main-body}\n	\n	{stash:chunk_sidebar}\n		{snippet-sidebar}			\n	{/stash:chunk_sidebar}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/index\"}', null, '1382892553', '1', 'n', '0', '', 'n', 'n', 'o', '275'), ('28', '1', '14', 'post', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:chunk_global-nav}\n		{snippet-global-nav-flat}\n	{/stash:chunk_global-nav}\n\n	{stash:chunk_main-body}\n		{exp:channel:entries\n			channel=\"blog|videos\"\n			status=\"public\"\n			disable=\"{var-disable}\"\n			limit=\"1\"\n			require_entry=\"yes\"\n			dynamic=\"yes\"\n		}\n			{if no_results}{redirect=\"404\"}{/if}\n\n			{stash:chunk_metadescription}{cf-post_metadescription}{/stash:chunk_metadescription}\n\n			{stash:chunk_metatitle}{cf-post_metatitle} | {gv-global-metatitle}{/stash:chunk_metatitle}\n		\n			<h1>{title}<br><small>{entry_date format=\"{var-df-date}\"}</small></h1>\n			<p>Filed in: {categories}{category_name}{/categories}</p>\n			\n			<dd>{cf-post_body}</dd>\n			\n			<h3>Discussion</h3>\n			{snippet-comments}\n			{snippet-comments-form}\n\n		{/exp:channel:entries}\n	{/stash:chunk_main-body}\n\n	{stash:chunk_sidebar}\n		{snippet-sidebar}			\n	{/stash:chunk_sidebar}	\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/index\"}', null, '1382892553', '1', 'n', '0', '', 'n', 'n', 'o', '189'), ('32', '1', '5', 'featured-post', 'y', 'webpage', ' <ul class=\"side-nav\">\n	 	<li class=\"nav-header\">Topics</li>\n		{exp:zoo_triggers:categories channel=\"videos\" status=\"public\" path=\"/videos/listing/topic\" all_text=\"Show all categories\" show_counter=\"no\"}\n		<li><a href=\"/videos/\">Browse All Topics</li>\n	</ul>', null, '1382897093', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('33', '1', '15', 'index', 'y', 'webpage', '', null, '1382900906', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('34', '1', '5', 'carousel', 'y', 'webpage', '<h3>A carousel will be put in here.</h3>\n<p>It will pull it\'s info from global variables.</p>', null, '1382900906', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('35', '1', '7', 'sub-left', 'y', 'webpage', '\n<!DOCTYPE html>\n<!--[if IE 8]><html class=\"no-js lt-ie9\" lang=\"en\"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\" lang=\"en\"> <!--<![endif]-->\n\n<head>\n  <meta charset=\"utf-8\" />\n  <meta name=\"viewport\" content=\"width=device-width\" />\n  <title>{exp:stash:get name=\"chunk_metatitle\"}</title>\n  <meta name=\"description\" content=\"{exp:stash:get name=\"chunk_metadescription\"}\">\n  <!-- If you are using CSS version, only link these 2 files, you may add app.css to use for your overrides if you like. -->\n  <link rel=\"stylesheet\" href=\"/css/normalize.css\" />\n  <link rel=\"stylesheet\" href=\"/css/foundation.css\" />\n\n  <!-- If you are using the gem version, you need this only -->\n  <link rel=\"stylesheet\" href=\"/css/app.css\" />\n\n  <script src=\"/js/vendor/custom.modernizr.js\"></script>\n\n</head>\n<body>\n\n  <!-- body content here -->\n  <!-- Header and Nav -->  \n  <div class=\"row\">\n    <div class=\"large-3 columns\">\n      <h1><img src=\"{gv-logo}\"/></h1>\n    </div>\n    <div class=\"large-9 columns\">\n     {exp:stash:get name=\"chunk_global-nav\"}\n    </div>\n  </div>\n  \n  <!-- End Header and Nav -->\n  \n  \n  <div class=\"row\">    \n    \n    <!-- Main Content Section -->\n    <!-- This has been source ordered to come first in the markup (and on small devices) but to be to the right of the nav on larger screens -->\n    <div class=\"large-9 push-3 columns\">     \n      {exp:stash:get name=\"chunk_main-body\"}\n    </div>\n    \n    \n    <!-- Nav Sidebar -->\n    <!-- This is source ordered to be pulled to the left on larger screens -->\n    <div class=\"large-3 pull-9 columns\">\n      {!-- display local nav if set --}\n      {exp:stash:get name=\"chunk_local-nav\"}\n\n      {!-- output sidebar widgets --}\n      {exp:stash:get name=\"chunk_sidebar\"}\n      \n        \n    </div>\n    \n  </div>\n    \n  \n  <!-- Footer -->\n  \n  {snippet-footer}\n  \n\n  <script>\n  document.write(\'<script src=\' +\n  (\'__proto__\' in {} ? \'/js/vendor/zepto\' : \'/js/vendor/jquery\') +\n  \'.js><\\/script>\')\n  </script>\n  <script src=\"/js/foundation/foundation.js\"></script>\n  <script src=\"/js/foundation/foundation.alerts.js\"></script>\n  <script src=\"/js/foundation/foundation.clearing.js\"></script>\n  <script src=\"/js/foundation/foundation.cookie.js\"></script>\n  <script src=\"/js/foundation/foundation.dropdown.js\"></script>\n  <script src=\"/js/foundation/foundation.forms.js\"></script>\n  <script src=\"/js/foundation/foundation.joyride.js\"></script>\n  <script src=\"/js/foundation/foundation.magellan.js\"></script>\n  <script src=\"/js/foundation/foundation.orbit.js\"></script>\n  <script src=\"/js/foundation/foundation.placeholder.js\"></script>\n  <script src=\"/js/foundation/foundation.reveal.js\"></script>\n  <script src=\"/js/foundation/foundation.section.js\"></script>\n  <script src=\"/js/foundation/foundation.tooltips.js\"></script>\n  <script src=\"/js/foundation/foundation.topbar.js\"></script>\n  <script src=\"/js/foundation/foundation.interchange.js\"></script>\n  <script>\n    $(document).foundation();\n  </script>\n</body>\n</html>\n', null, '1382994810', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('37', '1', '14', 'topics', 'y', 'webpage', '<!-- // START videos/index -->\n\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:chunk_global-nav}\n		{snippet-global-nav-topbar}\n	{/stash:chunk_global-nav}\n\n	{stash:chunk_metadescription}{gv-video-metadescription}{/stash:chunk_metadescription}\n	{stash:chunk_metatitle}{gv-video-metatitle}{triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag} | {gv-global-metatitle}{/stash:chunk_metatitle}\n		\n	{stash:chunk_main-body}\n\n		<h1>{if segment_1 == \"\"}Latest {/if}{gv-videos-title}  {triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag}</h1> \n		<div class=\"row\">\n\n			{exp:channel:categories\n				channel=\"videos\"\n				status=\"open|public\"\n				show=\"not 1\"\n				style=\"linear\"\n				show_empty=\"no\"\n			}\n				<div class=\"large-4 small-12 columns {if count == total_results}end{/if}\">	\n		            <h4>{if category_image}{!--<img src=\"{gv-images-path}_category/{category-image-filename}\">--}<img src=\"{gv-images-path}_category/{category-image-filename}\"><br>{/if}<a href=\"/videos/listing/topic/{category_url_title}\">{category_name}</a></h4>\n		            {if category_description}<p>{category_description}</p>{/if}\n		            <ul class=\"no-bullet caption\">\n		            {exp:channel:entries\n		            	channel=\"videos\"\n		            	disable=\"custom_fields|categories|category_fields|member_data|pagination|trackbacks\"\n		            	dynamic=\"off\"\n		            	status=\"public\"\n		            	category=\"{category_id}\"\n		            	limit=\"5\"\n		            }\n		            	<li><a href=\"/videos/post/{url_title}\">{title}</a></li>\n		            	\n		            {/exp:channel:entries}\n		            	<li><strong><a href=\"/videos/listing/topics/{category_url_title}\">View all</a></strong></li>\n		            </ul>\n				</div>	\n			{/exp:channel:categories}\n\n		\n		</div>\n	{/stash:chunk_main-body}\n	\n	{stash:chunk_sidebar}\n		{snippet-sidebar}			\n	{/stash:chunk_sidebar}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/sub-left\"}\n<!-- // END videos/index -->\n', null, '1383089395', '1', 'n', '0', '', 'n', 'n', 'o', '70'), ('39', '1', '14', 'filtered', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:block_global-nav}\n		{snippet-header-topbar}\n	{/stash:block_global-nav}\n\n	{stash:block_metadescription}{gv-portfolio-metadescription}{/stash:block_metadescription}\n	{stash:block_metatitle}{gv-portfolio-metatitle}{triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag} | {gv-global-metatitle}{/stash:block_metatitle}\n		\n	{stash:block_main-body}\n\n		<h1>{gv-portfolio-title} {triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag}</h1>\n		<ul class=\"sub-nav\">\n			 {exp:zoo_triggers:categories \n			 	channel=\"portfolio\" \n			 	status=\"public\" \n			 	path=\"/portfolio/filtered/topic\" \n			 	all_text=\"Show all topics\" \n			 	show_counter=\"no\"\n			 	show_ul=\"no\"\n			 	css_active=\"active\"\n			 	cat_group_id=\"2\"\n\n			 	}\n		</ul>\n		<div class=\"row\">\n			{exp:channel:entries\n				channel=\"portfolio\"\n				status=\"public\"\n				disable=\"{var-disable}\"\n				limit=\"10\"\n				{var-sort-blog}\n				dynamic=\"off\"\n				paginate=\"bottom\"\n				paginate_base=\"/blog/\"\n				{triggers:entries}\n			}\n				\n				{if no_results}<li>Sorry, no posts match that filter.</li>{/if}\n\n\n				<div class=\"small-6 large-3 columns {if count == total_results}end{/if}\">\n					{if cf-post_featured-image}\n						<a class=\"th\" href=\"/portfolio/post/{url_title}/\">\n							<img src=\"{cf-post_featured-image:small}\">\n						</a>\n					{/if}\n					<h6><a href=\"/portfolio/post/{url_title}/\">{title}</a></h6>\n					<p class=\"item-meta\">\n						{entry_date format=\"{var-df-date}\"}<br>\n						Filed in: {categories}{category_name}{/categories}\n					</p>\n				</div> \n\n				 {snippet-paginate}\n\n			{/exp:channel:entries}\n		</div>\n\n\n	{/stash:block_main-body}\n	\n	{stash:block_sidebar}\n		{snippet-sidebar}			\n	{/stash:block_sidebar}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/sub-right\"}', null, '1383259480', '1', 'n', '0', '', 'n', 'n', 'o', '158'), ('40', '1', '13', 'filtered', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n	{stash:block_global-nav}\n		{snippet-header-topbar}\n	{/stash:block_global-nav}\n\n	{stash:block_metadescription}{gv-videos-metadescription}{/stash:block_metadescription}\n	{stash:block_metatitle}{gv-videos-metatitle}{triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag} | {gv-global-metatitle}{/stash:block_metatitle}\n		\n	{stash:block_main-body}\n\n		<h1>{gv-videos-title} {triggers:entries_title_archive}{triggers:entries_title_category}{triggers:entries_title_tag}</h1>\n		<ul class=\"sub-nav\">\n		<li>Filter:</li>\n		<li {if triggers:segment_3 == \"\"}class=\"active\"{/if}><a href=\"/videos/filtered/\">All</a></li>\n			 {exp:zoo_triggers:categories \n			 	channel=\"videos\" \n			 	status=\"public\" \n			 	path=\"/videos/filtered/topic\" \n			 	all_text=\"Show all topics\" \n			 	show_counter=\"no\"\n			 	show_ul=\"no\"\n			 	css_active=\"active\"\n			 	cat_group_id=\"2\"\n\n			 	}\n		</ul>\n		<div class=\"row\">\n			{exp:channel:entries\n				channel=\"videos\"\n				status=\"public\"\n				disable=\"{var-disable}\"\n				limit=\"10\"\n				{var-sort-blog}\n				dynamic=\"off\"\n				paginate=\"bottom\"\n				paginate_base=\"/blog/\"\n				{triggers:entries}\n			}\n				\n				{if no_results}<li>Sorry, no posts match that filter.</li>{/if}\n\n\n				<div class=\"small-6 large-4 columns {if count == total_results}end{/if}\">\n					{if cf-post_featured-image}\n						<a class=\"th\" href=\"/videos/post/{url_title}/\">\n							<img src=\"{cf-post_featured-image:medium}\">\n						</a>\n					{/if}\n					<h6><a href=\"/videos/post/{url_title}/\">{title}</a></h6>\n					<p class=\"item-meta\">\n						{entry_date format=\"{var-df-date}\"}<br>\n						Filed in: {categories}{category_name}{/categories}\n					</p>\n				</div> \n\n				 {snippet-paginate}\n\n			{/exp:channel:entries}\n		</div>\n\n\n	{/stash:block_main-body}\n	\n	{stash:block_sidebar}\n		{snippet-sidebar}			\n	{/stash:block_sidebar}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/index\"}', null, '1383262239', '1', 'n', '0', '', 'n', 'n', 'o', '31'), ('41', '1', '5', 'entry-list', 'y', 'webpage', '<!-- // START widgets/portfolio-recent -->\n{if embed:heading != \"\"}\n	<h4>{embed:heading}</h4>\n{/if}\n\n<div class=\"row\">\n	{exp:channel:entries\n		channel=\"{embed:channel}\"\n		disable=\"member_data|pagination|trackbacks\"\n		dynamic=\"off\"\n		sort=\"{var-sort-portfolio}\"\n		limit=\"{embed:limit}\"\n		status=\"public\"\n		category=\"{embed:filter}\"\n	}\n		\n			\n		<div class=\"small-6 large-6 columns {if count==total_results}end{/if}\">\n			<a class=\"th\" href=\"/{embed:channel}/post/{url_title}\">{cf-post_featured-image:small wrap=\"image\"}</a>						\n			<p class=\"item-meta\"><a href=\"/{embed:channel}/post/{url_title}\">{title}</a><br>{entry_date format=\"{var-df-date-short}\"}</p>\n		</div>\n										\n			\n	{/exp:channel:entries}\n</div>\n\n<div class=\"row\">\n	<div class=\"large-8 small-12 columns push-4\">\n	<p class=\"more\"><a href=\"/{embed:channel}/\">View all</a></p>\n	</div>\n</div>\n\n<!-- // END widgets/portfolio-recent -->', null, '1383276792', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('44', '1', '5', 'topic-list', 'y', 'webpage', '<!-- // START widgets/blog-topics -->\n <h5>Topics</h5>\n <ul class=\"side-nav\">\n {exp:zoo_triggers:categories \n 	channel=\"blog\" \n 	status=\"public\" \n 	path=\"/blog/topic\" \n 	all_text=\"Show all topics\" \n 	show_counter=\"no\"\n 	show_ul=\"no\"\n 	cat_group_id=\"2\"\n\n 	}\n\n</ul>\n<!-- // END widgets/blog-topics -->', null, '1383337345', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('45', '1', '5', 'archive-list', 'y', 'webpage', '<!-- // START widgets/blog-archives -->\n{if embed:heading != \"\"}\n	<h5>{embed:heading}</h5>\n{/if}\n <ul class=\"side-nav\">\n {exp:zoo_triggers:archive \n 	channel=\"{embed:channel}\" \n 	status=\"public\" \n 	path=\"/{embed:channel}/archive\"\n 	all_text=\"Show all topics\" \n 	show_counter=\"no\"\n 	show_ul=\"no\"\n 	show_counter=\"no\"\n 	show_future_entries=\"no\"\n 	css_active=\"active\"\n 	}\n\n</ul>\n\n<!-- // END widgets/blog-archives -->', null, '1383338079', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('42', '1', '9', 'contact', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n		{exp:channel:entries\n			channel=\"pages\"\n			status=\"public|open\"\n			\n		}\n		{if no_results}{redirect=\"404\"}{/if}		\n		\n		\n\n\n		{stash:block_global-nav}\n			{snippet-header-topbar}\n		{/stash:block_global-nav}\n\n		{stash:block_main-body}\n			\n			<h1>{title}</h1>\n			{cf-post_body}\n  			{exp:email:contact_form user_recipients=\"no\" recipients=\"bryan@openmotive.com\" charset=\"utf-8\"}\n  			        <h2>Contact Us</h2>\n  			        <p>\n  			                <label for=\"from\">Your Email:</label><br />\n  			                <input type=\"text\" id=\"from\" name=\"from\" size=\"40\" maxlength=\"35\" value=\"{member_email}\" />\n  			        </p>\n  			        <p>\n  			                <label for=\"subject\">Subject:</label><br />\n  			                <input type=\"text\" id=\"subject\" name=\"subject\" size=\"40\" value=\"Contact Form\" />\n  			        </p>\n  			        <p>\n  			                <label for=\"message\">Message:</label><br />\n  			                <textarea id=\"message\" name=\"message\" rows=\"18\" cols=\"40\">\n  			                        Support Email from: {member_name}\n  			                        Sent at:  {current_time format=\"%Y %m %d\"}\n  			                </textarea>\n  			        </p>\n  			        <p>\n  			                <input name=\"submit\" type=\'submit\' value=\'Submit\' />\n  			        </p>\n  			{/exp:email:contact_form}\n\n\n  		{/stash:block_main-body}\n\n		{stash:block_sidebar}\n			\n			{snippet-local-nav-nested}\n\n		{/stash:block_sidebar}\n	{/exp:channel:entries}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/sub-left\"}', null, '1383333256', '1', 'n', '0', '', 'n', 'n', 'o', '50'), ('43', '1', '9', 'contact-response', 'y', 'webpage', '\n{exp:stash:set parse_tags=\"yes\"}\n		{exp:channel:entries\n			channel=\"pages\"\n			status=\"public|open\"\n			\n		}\n		{if no_results}{redirect=\"404\"}{/if}		\n		\n		\n\n\n		{stash:block_global-nav}\n			{snippet-header-topbar}\n		{/stash:block_global-nav}\n\n		{stash:block_main-body}\n			\n		\n			 {gv-content-contact-form-response}\n\n\n  		{/stash:block_main-body}\n\n		{stash:block_sidebar}\n			\n			{snippet-local-nav-nested}\n\n		{/stash:block_sidebar}\n	{/exp:channel:entries}\n\n{/exp:stash:set}\n\n\n{embed=\"_wrappers/sub-left\"}', null, '1383334092', '1', 'n', '0', '', 'n', 'n', 'o', '8');

-- ----------------------------
--  Table structure for `exp_throttle`
-- ----------------------------
DROP TABLE IF EXISTS `exp_throttle`;
CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_upload_no_access`
-- ----------------------------
DROP TABLE IF EXISTS `exp_upload_no_access`;
CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_upload_prefs`
-- ----------------------------
DROP TABLE IF EXISTS `exp_upload_prefs`;
CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_upload_prefs`
-- ----------------------------
INSERT INTO `exp_upload_prefs` VALUES ('1', '1', 'Posts', '/Sites/idaho/httpdocs/images/posts/', 'http://idaho/images/posts/', 'img', '', '1024', '1920', '', '', '', '', '', '', '2', null);

-- ----------------------------
--  Table structure for `exp_wiki_categories`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wiki_categories`;
CREATE TABLE `exp_wiki_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wiki_id` int(8) unsigned NOT NULL,
  `cat_name` varchar(70) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `cat_namespace` varchar(125) NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `wiki_id` (`wiki_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_wiki_category_articles`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wiki_category_articles`;
CREATE TABLE `exp_wiki_category_articles` (
  `page_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`page_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_wiki_namespaces`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wiki_namespaces`;
CREATE TABLE `exp_wiki_namespaces` (
  `namespace_id` int(6) NOT NULL AUTO_INCREMENT,
  `wiki_id` int(10) unsigned NOT NULL,
  `namespace_name` varchar(100) NOT NULL,
  `namespace_label` varchar(150) NOT NULL,
  `namespace_users` text,
  `namespace_admins` text,
  PRIMARY KEY (`namespace_id`),
  KEY `wiki_id` (`wiki_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_wiki_page`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wiki_page`;
CREATE TABLE `exp_wiki_page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wiki_id` int(3) unsigned NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `page_namespace` varchar(125) NOT NULL DEFAULT '',
  `page_redirect` varchar(100) DEFAULT NULL,
  `page_locked` char(1) NOT NULL DEFAULT 'n',
  `page_moderated` char(1) NOT NULL DEFAULT 'n',
  `last_updated` int(10) unsigned NOT NULL DEFAULT '0',
  `last_revision_id` int(10) DEFAULT NULL,
  `has_categories` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`page_id`),
  KEY `wiki_id` (`wiki_id`),
  KEY `page_locked` (`page_locked`),
  KEY `page_moderated` (`page_moderated`),
  KEY `has_categories` (`has_categories`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_wiki_page`
-- ----------------------------
INSERT INTO `exp_wiki_page` VALUES ('1', '1', 'index', '', null, 'n', 'n', '1387413164', '1', 'n');

-- ----------------------------
--  Table structure for `exp_wiki_revisions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wiki_revisions`;
CREATE TABLE `exp_wiki_revisions` (
  `revision_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `wiki_id` int(3) unsigned NOT NULL,
  `revision_date` int(10) unsigned NOT NULL,
  `revision_author` int(8) NOT NULL,
  `revision_notes` text NOT NULL,
  `revision_status` varchar(10) NOT NULL DEFAULT 'open',
  `page_content` mediumtext NOT NULL,
  PRIMARY KEY (`revision_id`),
  KEY `page_id` (`page_id`),
  KEY `wiki_id` (`wiki_id`),
  KEY `revision_author` (`revision_author`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_wiki_revisions`
-- ----------------------------
INSERT INTO `exp_wiki_revisions` VALUES ('1', '1', '1', '1387413164', '1', 'Creating the first page', 'open', 'Welcome to the opening page of your EE Wiki!');

-- ----------------------------
--  Table structure for `exp_wiki_search`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wiki_search`;
CREATE TABLE `exp_wiki_search` (
  `wiki_search_id` varchar(32) NOT NULL,
  `search_date` int(10) NOT NULL,
  `wiki_search_query` text,
  `wiki_search_keywords` varchar(150) NOT NULL,
  PRIMARY KEY (`wiki_search_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_wiki_uploads`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wiki_uploads`;
CREATE TABLE `exp_wiki_uploads` (
  `wiki_upload_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wiki_id` int(3) unsigned NOT NULL,
  `file_name` varchar(60) NOT NULL,
  `file_hash` varchar(32) NOT NULL,
  `upload_summary` text,
  `upload_author` int(8) NOT NULL,
  `image_width` int(5) unsigned NOT NULL,
  `image_height` int(5) unsigned NOT NULL,
  `file_type` varchar(50) NOT NULL,
  `file_size` int(10) unsigned NOT NULL DEFAULT '0',
  `upload_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`wiki_upload_id`),
  KEY `wiki_id` (`wiki_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_wikis`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wikis`;
CREATE TABLE `exp_wikis` (
  `wiki_id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `wiki_label_name` varchar(100) NOT NULL,
  `wiki_short_name` varchar(50) NOT NULL,
  `wiki_text_format` varchar(50) NOT NULL,
  `wiki_html_format` varchar(10) NOT NULL,
  `wiki_upload_dir` int(3) unsigned NOT NULL DEFAULT '0',
  `wiki_admins` text,
  `wiki_users` text,
  `wiki_revision_limit` int(8) unsigned NOT NULL,
  `wiki_author_limit` int(5) unsigned NOT NULL,
  `wiki_moderation_emails` text,
  PRIMARY KEY (`wiki_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_wikis`
-- ----------------------------
INSERT INTO `exp_wikis` VALUES ('1', 'Mine', 'mine', 'markdown', 'all', '1', '1', '1', '200', '75', '');

