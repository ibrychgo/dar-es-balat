
Low Variables
=================
You can use the template tag ( {exp:low_variables:parse var="my_var"} ) to parse at stage five just like module tags.

Using Variables in place of embeds
**********************************

Using single variable syntax to parse and embed the information:


Tag Syntax
------------
{exp:low_variables:single
        var='my_spaghetti_code'
        preparse:child_entry_id='{entry_id}'
        preparse:child_url_title='{url_title}'
        preparse:segment_2='{segment_2}'
    }



Variable Code:
---------------
{exp:member_categories:check category="{child_url_title}"}
  {exp:playa:children entry_id='{child_entry_id}'}
    [some code about the grandchild entry]
  {/exp:playa:children}

  {if no_results}
    [some more code…]
  {/if}

{/exp:member_categories:check}

**********************************************
Datepicker date format: %Y-%m-%d %h:%m %A
**********************************************

Starter Entries block:
{exp:channel:entries
  channel="blog|videos"
  status="public"
  disable="{var-disable}"
  limit="2"
  {var-sort-blog}
  dynamic="off"
  paginate="bottom"
  paginate_base="/starters/"
  {triggers:entries}
}


LOCAL NAV WITH TAXONOMY
************************************
  {exp:taxonomy:nav 
                  tree_id="1" 
                  depth="2"
                  entry_id="{entry_id}"
                  display_root="no"
                  auto_expand="yes"
                  active_branch_start_level="1"
                  ul_css_class="side-nav"
               
            }
              <li class="{node_active}"><a href="{node_url}">{node_title}</a></li>
            {/exp:taxonomy:nav}    
     
Getting info about a node
----------------------------     
{exp:taxonomy:get_node 
         tree_id="1"
         key="entry_id" 
         val="{entry_id}"
         var_prefix="tx_"
         include_parent="yes"
       }

              <legend>Node information</legend>
                  
              <p>This node is {tx_node_level} levels deep.</p>
              <p>The parent node is {tx_parent_node_level} levels deep. </p>
              <p>The url to the parent is {exp:taxonomy:node_url entry_id="{tx_parent_node_entry_id}" tree_id="1"}</p>
 {/exp:taxonomy:get_node} 

          

************************************
Breadcrumbs

<p>You are here: 
                  {exp:taxonomy:breadcrumbs}
                      {if here}
                          {node_title}
                      {if:else}
                          <a href="{node_url}">{node_title}</a> &rarr; 
                      {/if}
                  {/exp:taxonomy:breadcrumbs}
              </p>
              *

****************************
Hacksaw

{exp:eehive_hacksaw
chars = "" // Limit by number of characters
chars_start = "" // Used with the 'chars' parameter, this starts the excerpt at X characters from the beginning of the content
words = "" // Limit by number of words
cutoff = "" // Limit by a specific cutoff string
append = "" // String to append to the end of the excerpt
allow = "" // HTML tags you want to allow. Ex allow="<b><a>"
}
  {your_content}

{/exp:eehive_hacksaw}